﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyItemsModule;
using OntologyItemsModule.Models;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using ReportModule.Factories;
using ReportModule.Models;
using ReportModule.Primitives;
using ReportModule.Services;
using ReportModule.Validation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule
{
    public class ReportController : AppController
    {
        private Services.ServiceAgentElastic serviceAgent;
        private Services.ServiceAgentElasticReport serviceAgentReport;
        private Services.ServiceAgentMSSQL serviceAgentMsSQL;
        private Factories.ReportViewFactory reportViewFactory;

        private Factories.ReportGridFactory reportGridFactory;

        public clsOntologyItem ClassReport
        {
            get
            {
                return Config.LocalData.Class_Reports;
            }
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetOItems(List<string> ids, string type)
        {
            return await serviceAgent.GetOItems(ids, type);
        }

        public async Task<ResultItem<ReportAndFields>> GetReport(clsOntologyItem oItem)
        {
            var result = new ResultItem<ReportAndFields>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new ReportAndFields()
            };

            var resultReportFields = await serviceAgentReport.GetReportFields(oItem);
            if (resultReportFields.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultReportFields.ResultState;
                return result;
            }

            var resultGetReport = await reportViewFactory.GetReport(resultReportFields.Result);
            if (resultGetReport.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultGetReport.ResultState;
                return result;
            }

            var getReportFilterItemsRequest = new GetFilterItemsRequest(FilterRequestType.GetFiltersByReport)
            {
                ReportOItem = new clsOntologyItem
                {
                    GUID = resultGetReport.Result.IdReport,
                    Name = resultGetReport.Result.NameReport,
                    GUID_Parent = Config.LocalData.Class_Reports.GUID,
                    Type = Globals.Type_Object
                }
            };
            var resultReportFilters = await serviceAgent.GetReportFilterItemModel(getReportFilterItemsRequest, false);

            var resultGetReportFields = await reportViewFactory.CreateReportFieldList(resultReportFields.Result);
            if (resultGetReport.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultGetReport.ResultState;
                return result;
            }

            var resultReportFiltersFactory = await ReportFilterItemsFactory.CreateReportFilterItems(resultReportFilters.Result, Globals);
            if (resultReportFiltersFactory.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultReportFiltersFactory.ResultState;
                return result;
            }

            result.Result.Report = resultGetReport.Result;
            result.Result.ReportFields = resultGetReportFields.Result;
            result.Result.ReportFilters = resultReportFiltersFactory.Result;

            return result;
        }

        public async Task<clsOntologyItem> SyncColumns(string idReport, IMessageOutput messageOutput)
        {
            var result = Globals.LState_Success.Clone();

            var serviceAgent = new Services.ServiceAgentElastic(Globals);
            var serviceAgentMSSQL = new ServiceAgentMSSQL(Globals);
            messageOutput?.OutputInfo(LogMessages.SyncColumns_GetReport);
            var getReportResult = await serviceAgent.GetOItem(idReport, Globals.Type_Object);
            result = getReportResult.ResultState;
            if (result.GUID == Globals.LState_Error.GUID)
            {
                result.Additional1 = "Error while getting the Report-Item!";
                messageOutput?.OutputError(result.Additional1);
                return result;
            }

            if (getReportResult.Result.GUID_Parent != Config.LocalData.Class_Reports.GUID)
            {
                result = Globals.LState_Error.Clone();
                result.Additional1 = "Requested Item is no Report!";
                messageOutput?.OutputError(result.Additional1);
                return result;
            }
            messageOutput?.OutputInfo(LogMessages.SyncColumns_HaveReport);

            var reportItem = getReportResult.Result;

            messageOutput?.OutputInfo(LogMessages.SyncColumns_GetReportAndFields);
            var report = await GetReport(reportItem);
            result = report.ResultState;

            if (result.GUID == Globals.LState_Error.GUID)
            {
                messageOutput?.OutputError(result.Additional1);
                return result;
            }

            messageOutput?.OutputInfo(LogMessages.SyncColumn_HaveReportAndFields);

            messageOutput?.OutputInfo(LogMessages.SyncColumns_GetMSSQLColumns);

            var columnsResult = await serviceAgentMSSQL.GetColumns(report.Result.Report);

            result = columnsResult.ResultState;

            if (result.GUID == Globals.LState_Error.GUID)
            {
                messageOutput?.OutputError(result.Additional1);
                return result;
            }

            messageOutput?.OutputInfo(LogMessages.SyncColumns_HaveMSSQLColumns);

            messageOutput?.OutputInfo(LogMessages.SyncColumns_GetMissingColumns);
            var missingColumns = (from col in columnsResult.Result
                                  join colExist in report.Result.ReportFields on col.ColumnName.ToLower() equals colExist.NameCol.ToLower() into colsExist
                                  from colExist in colsExist.DefaultIfEmpty()
                                  where colExist == null
                                  select col).ToList();

            messageOutput?.OutputInfo(string.Format(LogMessages.SyncColumns_HaveMissingColumns, missingColumns.Count, columnsResult.Result.Count));
            if (!missingColumns.Any()) return result;

            var elasticAgent = new ServiceAgentElastic(Globals);

            messageOutput?.OutputInfo(LogMessages.SyncColumns_GetDataMap);
            var getMapResult = await elasticAgent.GetMSSQLDataMaps();

            result = getMapResult.ResultState;

            if (result.GUID == Globals.LState_Error.GUID)
            {
                messageOutput?.OutputError(result.Additional1);
                return result;
            }
            messageOutput?.OutputInfo(string.Format(LogMessages.SyncColumns_HaveDataMap, getMapResult.Result.Count));

            messageOutput?.OutputInfo(LogMessages.SyncColumns_GetItemsToSave);
            missingColumns.Where(col => col.ColumnType.ToLower() == "varchar" && col.Length == 36).ToList().ForEach(col =>
            {
                col.ColumnType = "uniqueidentifier";
            });

            var relationConfig = new clsRelationConfig(Globals);

            var columnsToFields = (from col in missingColumns
                                   join fieldTypeMap in getMapResult.Result on col.ColumnType.ToLower() equals fieldTypeMap.Name_Other
                                   select new { col, fieldTypeMap, oColumn = new clsOntologyItem
                                       {
                                            GUID = Globals.NewGUID,
                                            Name = col.ColumnName,
                                            GUID_Parent = Config.LocalData.Class_DB_Columns.GUID,
                                            Type = Globals.Type_Object
                                       },
                                       oField = new clsOntologyItem
                                       {
                                           GUID = Globals.NewGUID,
                                           Name = col.ColumnName,
                                           GUID_Parent = Config.LocalData.Class_Report_Field.GUID,
                                           Type = Globals.Type_Object
                                       }
                                   }).ToList();

            var objectsToSave = new List<clsOntologyItem>();
            var attributesToSave = new List<clsObjectAtt>();
            var relationsToSave = new List<clsObjectRel>();

            var oReport = new clsOntologyItem
            {
                GUID = report.Result.Report.IdReport,
                Name = report.Result.Report.NameReport,
                GUID_Parent = Config.LocalData.Class_Reports.GUID,
                Type = Globals.Type_Object
            };

            var oView = new clsOntologyItem
            {
                GUID = report.Result.Report.IdDBViewOrEsType,
                Name = report.Result.Report.NameDBViewOrEsType,
                GUID_Parent = Config.LocalData.Class_DB_Views.GUID,
                Type = Globals.Type_Object
            };

            foreach (var colToField in columnsToFields)
            {
                objectsToSave.Add(colToField.oColumn);
                objectsToSave.Add(colToField.oField);
                attributesToSave.Add(relationConfig.Rel_ObjectAttribute(colToField.oField, Config.LocalData.AttributeType_invisible, colToField.col.ColumnType == "uniqueidentifier"));
                relationsToSave.Add(relationConfig.Rel_ObjectRelation(colToField.oField, oReport, Config.LocalData.RelationType_belongs_to));
                relationsToSave.Add(relationConfig.Rel_ObjectRelation(colToField.oField, new clsOntologyItem
                {
                    GUID = colToField.fieldTypeMap.ID_Object,
                    Name = colToField.fieldTypeMap.Name_Object,
                    GUID_Parent = colToField.fieldTypeMap.ID_Parent_Object,
                    Type = Globals.Type_Object
                }, Config.LocalData.RelationType_is_of_Type));
                relationsToSave.Add(relationConfig.Rel_ObjectRelation(colToField.oField, colToField.oColumn, Config.LocalData.RelationType_belongs_to));
                relationsToSave.Add(relationConfig.Rel_ObjectRelation(colToField.oColumn, oView, Config.LocalData.RelationType_belongs_to));
            }

            messageOutput?.OutputInfo(string.Format(LogMessages.SyncColumns_HaveItemsToSave, objectsToSave.Count, attributesToSave.Count, relationsToSave.Count));

            var dbWriter = new OntologyModDBConnector(Globals);

            messageOutput?.OutputInfo(LogMessages.SyncColumns_SaveItems);
            if (objectsToSave.Any())
            {
                result = dbWriter.SaveObjects(objectsToSave);
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving the objects!";
                    return result;
                }
            }

            if (attributesToSave.Any())
            {
                result = dbWriter.SaveObjAtt(attributesToSave);
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving the object-attributes!";
                    return result;
                }
            }
            
            if (relationsToSave.Any())
            {
                result = dbWriter.SaveObjRel(relationsToSave);
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving the object-relations!";
                    return result;
                }
            }
            messageOutput?.OutputInfo(LogMessages.SyncColumns_SavedItems);

            return result;
        }


        public async Task<ResultItem<List<ReportField>>> GetReportFields(clsOntologyItem reportItem)
        {
            var serviceAgent = new Services.ServiceAgentElastic(Globals);



            var result = new ResultItem<List<ReportField>>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new List<ReportField>()
            };

            var resultReportFields = await serviceAgentReport.GetReportFields(reportItem);
            if (resultReportFields.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultReportFields.ResultState;
                result.ResultState.Additional1 = "Error while reading Reportfields from Database!";
                return result;
            }

            var resultReportFieldList = await reportViewFactory.CreateReportFieldList(resultReportFields.Result);
            if (resultReportFieldList.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultReportFieldList.ResultState;
                result.ResultState.Additional1 = "Error while creating Reportfield List!";
                return result;
            }

            return resultReportFieldList;
        }

        public async Task<ResultItem<ReportConfig>> GetReportConfig(clsOntologyItem oItem, string getDataUrl, List<AdditionalReportField> additionalFields = null)
        {
            var result = new ResultItem<ReportConfig>
            {
                ResultState = Globals.LState_Success.Clone()
            };

            var resultReportFields = await serviceAgentReport.GetReportFields(oItem);
            if (resultReportFields.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultReportFields.ResultState;
                return result;
            }

            var resultReportFieldList = await reportViewFactory.CreateReportFieldList(resultReportFields.Result);
            if (resultReportFieldList.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultReportFieldList.ResultState;
                return result;
            }

            var resultGetReport = await reportViewFactory.GetReport(resultReportFields.Result);
            if (resultGetReport.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultGetReport.ResultState;
                return result;
            }

            var getRowDiffRequest = new GetRowDiffRequest(resultGetReport.Result, resultReportFieldList.Result);
            var resultGetRowDiffs = await GetRowDiff(getRowDiffRequest);

            if (resultGetRowDiffs.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultGetRowDiffs.ResultState;
                return result;
            }

            var getReportFilterItemsRequest = new GetFilterItemsRequest(FilterRequestType.GetFiltersByReport)
            {
                ReportOItem = new clsOntologyItem
                {
                    GUID = resultGetReport.Result.IdReport,
                    Name = resultGetReport.Result.NameReport,
                    GUID_Parent = Config.LocalData.Class_Reports.GUID,
                    Type = Globals.Type_Object
                }
            };
            var resultReportFilters = await serviceAgent.GetReportFilterItemModel(getReportFilterItemsRequest, false);

            var resultReportFiltersFactory = await ReportFilterItemsFactory.CreateReportFilterItems(resultReportFilters.Result, Globals);
            if (resultReportFiltersFactory.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultReportFiltersFactory.ResultState;
                return result;
            }

            var resultGridconfig = await reportGridFactory.CreateGridConfig(resultReportFieldList.Result, getDataUrl, additionalFields);
            if (resultGridconfig.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultGridconfig.ResultState;
                return result;
            }

            result.Result = new ReportConfig
            {
                ReportItem = resultGetReport.Result,
                ReportFields = resultReportFieldList.Result,
                GridConfig = resultGridconfig.Result,
                RowDiff = resultGetRowDiffs.Result,
                ReportFilters = resultReportFiltersFactory.Result
            };

            return result;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetObjectsForSend(List<ObjectForSendRaw> objectsForSend, List<ReportField> reportFields)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(async () =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };


                var fields = (from objectForSend in objectsForSend
                              join reportField in reportFields on objectForSend.columnNameSelected equals reportField.NameCol
                              join leadField in reportFields on reportField.IdLeadField equals leadField.IdReportField
                              select new { leadField, objectForSend });

                if (!fields.Any())
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "ReportFields not found!";
                    return result;
                }

                var guids = new List<string>();
                foreach (var field in fields)
                {
                    //var ix = reportFields.IndexOf(field.leadField);

                    var valueItem = field.objectForSend.row.FirstOrDefault(cell => cell.columnName == field.leadField.NameCol);
                    if (valueItem != null)
                    {
                        var value = ((string[])valueItem.cellValue)[0];
                        value = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(value ?? ""));

                        if (value == null)
                        {
                            result.ResultState = Globals.LState_Nothing.Clone();
                            return result;
                        }

                        guids.Add(value);
                    }
                }


                if (guids.Any())
                {
                    var oItem = await GetOItems(guids, Globals.Type_Object);
                    if (oItem.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        return result;
                    }
                    result.Result = oItem.Result;
                }

                return result;
            });

            return taskResult;
        }



        public async Task<ResultItem<List<Dictionary<string, object>>>> LoadData(ReportItem reportItem, List<ReportField> reportFields, string whereClaus = null, string orderClause = null, List<AdditionalReportField> additionalFields = null)
        {
            var result = new ResultItem<List<Dictionary<string, object>>>
            {
                ResultState = Globals.LState_Success.Clone()
            };

            var resultGetData = await serviceAgentMsSQL.GetData(reportItem, reportFields, whereClause: whereClaus, orderClause: orderClause, additionalFields: additionalFields);
            if (resultGetData.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = resultGetData.ResultState;
                return result;
            }

            result.Result = resultGetData.Result;
            return result;
        }

        public async Task<ResultItem<GetRowDiffResult>> GetRowDiff(GetRowDiffRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<GetRowDiffResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new GetRowDiffResult()
                };

                request.MessageOutput?.OutputInfo(Primitives.LogMessages.GetRowDiff_ValidateRequest);
                var validationResult = ValidationController.ValidateGetRowDiffRequest(request, Globals);

                result.ResultState = validationResult;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo(Primitives.LogMessages.GetRowDiff_ValidatedRequest);

                var elasticAgent = new ServiceAgentElastic(Globals);

                var searchReplaceContent = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = request.ReportItem.IdReport,
                        ID_RelationType = Config.LocalData.ClassRel_Row_Diff_belongs_to_Reports.ID_RelationType,
                        ID_Parent_Object = Config.LocalData.ClassRel_Row_Diff_belongs_to_Reports.ID_Class_Left
                    }
                };

                request.MessageOutput?.OutputInfo(Primitives.LogMessages.GetRowDiff_GetRowDiffs);
                var getReport = await elasticAgent.GetRelationItems(searchReplaceContent);

                result.ResultState = getReport.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = Primitives.LogMessages.GetRowDiff_ErrorGetRowDiffs;
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo(Primitives.LogMessages.GetRowDiff_HaveRowDiffs);

                if (!getReport.Result.Any()) return result;

                //result.Result.Report = request.ReportItem;

                request.MessageOutput?.OutputInfo(Primitives.LogMessages.GetRowDiff_GetRowDiffFields);

                var searchFields = getReport.Result.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_RelationType = Config.LocalData.ClassRel_Row_Diff_compare_Report_Field.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Row_Diff_compare_Report_Field.ID_Class_Right
                }).ToList();

                var getFieldsResult = await elasticAgent.GetRelationItems(searchFields);
                result.ResultState = getFieldsResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = Primitives.LogMessages.GetRowDiff_ErrorGetRowDiffFields;
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo(Primitives.LogMessages.GetRowDiff_HaveRowDiffFields);

                var fields = getFieldsResult.Result.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology,
                    Val_Long = rel.OrderID
                }).ToList();

                result.Result.Field1 = (from diffField in fields.Where(fld => fld.Val_Long == 1)
                                        join field in request.ReportFields on diffField.GUID equals field.IdReportField
                                        select field).FirstOrDefault();

                result.Result.Field2 = (from diffField in fields.Where(fld => fld.Val_Long == 2)
                                        join field in request.ReportFields on diffField.GUID equals field.IdReportField
                                        select field).FirstOrDefault();

                result.ResultState = ValidationController.ValidateGetRowDiffResult(result.Result, Globals);

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<SyncResult>> SyncReport(SyncReportRequest request)
        {
            var ontologyController = new OntologyConnector(Globals);

            var reportOItem = new clsOntologyItem
            {
                GUID = request.ReportItem.IdReport,
                Name = request.ReportItem.NameReport,
                GUID_Parent = Config.LocalData.Class_Reports.GUID,
                Type = Globals.Type_Object
            };


            var result = new ResultItem<SyncResult>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new SyncResult
                {
                    Report = request.ReportItem,
                    SyncStart = DateTime.Now
                }
            };

            request.MessageOutput?.OutputInfo($"Start Sync: {result.Result.SyncStart}");

            request.MessageOutput?.OutputInfo($"Get Ontologies...");
            var resultOntology = await ontologyController.GetOntologies(new OntologyItemsModule.Services.GetOntologyRequest(reportOItem)
            {
                GetClassOntology = false,
                DirectionRefOntology = Globals.Direction_LeftRight,
                RelationTypeRefOntology = Config.LocalData.RelationType_contains
            });

            result.ResultState = resultOntology.ResultState;

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState.Additional1 = "Error whihle getting Ontology";
                request.MessageOutput?.OutputError(result.ResultState.Additional1);
                return result;
            }

            request.MessageOutput?.OutputInfo($"Have Ontologies.");

            var server = request.ReportItem.NameServer ?? Globals.Server;
            var dbOrIndex = request.ReportItem.NameDatabaseOrIndex ?? Globals.Rep_Database;
            var instance = request.ReportItem.NameInstance;
            var dataSource = server + (string.IsNullOrEmpty(instance) ? "" : @"\" + instance);
            var connectionStringSQLConnection = "Data Source=" + dataSource + ";Initial Catalog=" + dbOrIndex +
                ";Integrated Security=True;Connection Timeout=900";

            var deleteAdapter = new OntoDbReportsTableAdapters.delete_ImportTablesTableAdapter();
            deleteAdapter.Connection = new System.Data.SqlClient.SqlConnection(connectionStringSQLConnection);
            deleteAdapter.SelectCommandTimeout = 900;
            request.MessageOutput?.OutputInfo($"Prepare deletion...");
            deleteAdapter.GetData();
            request.MessageOutput?.OutputInfo($"Prepared deletion.");

            var classes = resultOntology.Result.SelectMany(onto => onto.RefItems).Where(refItem => refItem.Type == Globals.Type_Class).GroupBy(refItem => new { refItem.GUID }).Select(cls => new clsOntologyItem { GUID = cls.Key.GUID }).ToList();

            if (classes.Any())
            {
                request.MessageOutput?.OutputInfo($"Sync classes... {classes.Count} (Count)");
                try
                {
                    result.ResultState = await serviceAgentMsSQL.SyncSQLDBClasses(classes, request.ReportItem);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while syncing Classes";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    request.MessageOutput?.OutputInfo($"Synced classes.");
                }
                catch (Exception ex)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = ex.Message;
                    return result;
                }
            }

            var attributeTypes = resultOntology.Result.SelectMany(onto => onto.RefItems).Where(refItem => refItem.Type == Globals.Type_AttributeType).GroupBy(refItem => new { refItem.GUID }).Select(attT => new clsOntologyItem { GUID = attT.Key.GUID }).ToList();

            var classAtts = (from cls in classes
                             from attType in attributeTypes
                             select new clsClassAtt { ID_Class = cls.GUID, ID_AttributeType = attType.GUID }).ToList();

            if (classAtts.Any())
            {
                request.MessageOutput?.OutputInfo($"Sync ClassAtts... {classAtts.Count} (Count)");
                try
                {
                    result.ResultState = await serviceAgentMsSQL.SyncSQLDbAttributes(classAtts, request.ReportItem);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while syncing Attributes";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    request.MessageOutput?.OutputInfo($"Synced ClassAtts.");
                }
                catch (Exception ex)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = ex.Message;
                    return result;
                }
            }

            var relationTypes = resultOntology.Result.SelectMany(onto => onto.RefItems).Where(refItem => refItem.Type == Globals.Type_RelationType).GroupBy(refitem => new { refitem.GUID }).Select(relT => new clsOntologyItem { GUID = relT.Key.GUID }).ToList();
            var classRel = (from cls in classes
                            from relationType in relationTypes
                            select new clsClassRel { ID_Class_Left = cls.GUID, ID_RelationType = relationType.GUID }).ToList();

            classRel.AddRange(from cls in classes
                              from relationType in relationTypes
                              select new clsClassRel { ID_Class_Right = cls.GUID, ID_RelationType = relationType.GUID });

            if (classRel.Any())
            {
                request.MessageOutput?.OutputInfo($"Sync ClassRels... {classRel.Count} (Count)");
                request.MessageOutput?.OutputInfo($"Sync ClassAtts... {classAtts.Count} (Count)");
                try
                {
                    result.ResultState = await serviceAgentMsSQL.SyncSQLDbRelationsOr(classRel, request.ReportItem);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while syncing Relations";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    request.MessageOutput?.OutputInfo($"Synced ClassRels.");
                }
                catch (Exception ex)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = ex.Message;
                    return result;
                }
            }

            request.MessageOutput?.OutputInfo($"Update Tables: {request.ReportItem.NameReport}");
            result.ResultState = await serviceAgentMsSQL.UpdateTables(request.ReportItem);

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState.Additional1 = "Error while Updating Tables in the DB";
                request.MessageOutput?.OutputError(result.ResultState.Additional1);
            }
            else
            {
                result.Result.SyncEnd = DateTime.Now;
                request.MessageOutput?.OutputInfo($"Updated Tables.");
                request.MessageOutput?.OutputInfo($"Finished Sync: {result.Result.SyncEnd}, duration: {result.Result.SyncEnd.Subtract(result.Result.SyncStart).TotalSeconds} s");
            }

            return result;
        }

        public async Task<clsOntologyItem> CreatePDFReport(CreatePDFReportRequest request)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
           {
               var result = Globals.LState_Success.Clone();

               if (string.IsNullOrEmpty(request.IdConfig))
               {
                   result = Globals.LState_Error.Clone();
                   result.Additional1 = "Config is not set!";
                   request.MessageOutput?.OutputError(result.Additional1);
                   return result;
               }

               var elasticAgent = new ServiceAgentElastic(Globals);

               request.MessageOutput?.OutputInfo("Get Model....");
               var resultModel = await serviceAgent.GetModelPDFReport(request);
               result = resultModel.ResultState;
               if (result.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.Additional1);
                   return result;
               }
               request.MessageOutput?.OutputInfo("Have Model.");

               foreach (var config in resultModel.Result.Configs)
               {
                   var path = resultModel.Result.ConfigsToPaths.FirstOrDefault(pathRel => pathRel.ID_Object == config.GUID);
                   var report = resultModel.Result.ConfigsToReports.Where(pathRel => pathRel.ID_Object == config.GUID).Select(rel => new clsOntologyItem
                   {
                       GUID = rel.ID_Other,
                       Name = rel.Name_Other,
                       GUID_Parent = rel.ID_Parent_Other,
                       Type = rel.Ontology
                   }).FirstOrDefault();

                   var whereClause = (from filter in resultModel.Result.ConfigsToFilter.Where(filt => filt.ID_Object == config.GUID)
                                      join filterValue in resultModel.Result.FilterValues on filter.ID_Other equals filterValue.ID_Object
                                      select filterValue).FirstOrDefault();

                   var sortClause = resultModel.Result.ConfigsToSort.Where(srt => srt.ID_Object == config.GUID).Select(rel => new clsOntologyItem
                   {
                       GUID = rel.ID_Other,
                       Name = rel.Name_Other,
                       GUID_Parent = rel.ID_Parent_Other,
                       Type = rel.Ontology
                   }).FirstOrDefault();

                   if (path == null)
                   {
                       result = Globals.LState_Error.Clone();
                       result.Additional1 = $"Config {config.Name}: No path defined!";
                       request.MessageOutput?.OutputError(result.Additional1);
                       return result;
                   }

                   var fieldFormats = (from fieldFormat in resultModel.Result.ConfigsToFieldFormat.Where(rel => rel.ID_Object == config.GUID)
                                       join isHtml in resultModel.Result.FieldFormatIsHtml on fieldFormat.ID_Other equals isHtml.ID_Object into isHtmls
                                       from isHtml in isHtmls.DefaultIfEmpty()
                                       join reportField in resultModel.Result.FieldFormatToReportFields on fieldFormat.ID_Other equals reportField.ID_Object
                                       select new { reportField, isHtml });

                   var outputPath = path.Name_Other;

                   if (string.IsNullOrEmpty(outputPath))
                   {
                       result = Globals.LState_Error.Clone();
                       result.Additional1 = "The PDFFolder is not set!";
                       request.MessageOutput?.OutputError(result.Additional1);
                       return result;
                   }

                   if (!Directory.Exists(outputPath))
                   {
                       result = Globals.LState_Error.Clone();
                       result.Additional1 = $"PDFFolder {outputPath} does not exist!";
                       request.MessageOutput?.OutputError(result.Additional1);
                       return result;
                   }


                   try
                   {
                       var testFile = Path.Combine(outputPath, $"{Guid.NewGuid().ToString()}.txt");
                       File.WriteAllText(testFile, "test");
                       File.Delete(testFile);
                   }
                   catch (Exception ex)
                   {
                       result = Globals.LState_Error.Clone();
                       result.Additional1 = $"Error while writing test-File in path {outputPath}";
                       request.MessageOutput?.OutputError(result.Additional1);
                       return result;

                   }



                   var refItemTask = await GetOItem(report.GUID, Globals.Type_Object);

                   if (refItemTask.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result = Globals.LState_Error.Clone();
                       result.Additional1 = "Report cannot be found!";
                       request.MessageOutput?.OutputError(result.Additional1);
                       return result;
                   }

                   var resultGridConfig = await GetReportConfig(refItemTask.Result, "");

                   if (resultGridConfig.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result = Globals.LState_Error.Clone();
                       result.Additional1 = "Report cannot be found!";
                       request.MessageOutput?.OutputError(result.Additional1);
                       return result;
                   }

                   if (resultGridConfig.Result.ReportItem == null)
                   {
                       if (resultGridConfig.ResultState.GUID == Globals.LState_Error.GUID)
                       {
                           result = Globals.LState_Error.Clone();
                           result.Additional1 = "Report cannot be found!";
                           request.MessageOutput?.OutputError(result.Additional1);
                           return result;
                       }
                   }

                   var resultLoadData = await LoadData(resultGridConfig.Result.ReportItem, resultGridConfig.Result.ReportFields, whereClause?.Val_String, sortClause?.Name);

                   if (resultLoadData.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result = Globals.LState_Error.Clone();
                       result.Additional1 = "Report cannot be loaded!";
                       request.MessageOutput?.OutputError(result.Additional1);
                       return result;
                   }

                   var filePath = Path.Combine(outputPath, $"{Guid.NewGuid().ToString()}.pdf");
                   using (var fs = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                   {
                       var doc = new Document();
                       var writer = PdfWriter.GetInstance(doc, fs);
                       PageEventHelper pageEventHelper = new PageEventHelper();
                       writer.PageEvent = pageEventHelper;
                       var startTable = true;
                       var endTable = false;
                       PdfPTable table = null;
                       doc.Open();

                       var visibleFieldsWithFormats = (from reportField in resultGridConfig.Result.ReportFields.Where(field => field.IsVisible).OrderBy(field => field.OrderId)
                                                       join fieldFormat in fieldFormats on reportField.IdReportField equals fieldFormat.reportField.ID_Other into fieldFormats1
                                                       from fieldFormat in fieldFormats1.DefaultIfEmpty()
                                                       select new { reportField, fieldFormat }).ToList();
                       foreach (var row in resultLoadData.Result)
                       {

                           if (startTable)
                           {
                               table = new PdfPTable(new float[] { 1f, 3f });
                               table.HorizontalAlignment = 0;
                               table.SpacingBefore = 20f;
                               table.SpacingAfter = 30f;
                               startTable = false;
                               endTable = true;
                           }


                           foreach (var field in visibleFieldsWithFormats)
                           {
                               if (field.fieldFormat != null && field.fieldFormat.isHtml != null && field.fieldFormat.isHtml.Val_Bit.Value)
                               {
                                   if (endTable)
                                   {
                                       startTable = true;
                                       endTable = false;
                                       doc.Add(table);
                                   }
                                   Paragraph title = new Paragraph();
                                   title.Alignment = Element.ALIGN_LEFT;
                                   title.Font = FontFactory.GetFont("Arial", 24);
                                   title.Add($"\n{field.reportField.NameReportField}\n\n");
                                   doc.Add(title);

                                   using (StringReader sr = new StringReader(row[field.reportField.NameCol]?.ToString() ?? string.Empty))
                                   {
                                       XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, sr);
                                   }

                               }
                               else
                               {
                                   if (startTable)
                                   {
                                       table = new PdfPTable(new float[] { 1f, 3f });
                                       table.HorizontalAlignment = 0;
                                       table.SpacingBefore = 20f;
                                       table.SpacingAfter = 30f;
                                       startTable = false;
                                       endTable = true;
                                   }
                                   table.AddCell(field.reportField.NameReportField);
                                   if (row.ContainsKey(field.reportField.NameCol))
                                   {
                                       table.AddCell(row.ContainsKey(field.reportField.NameCol) ? row[field.reportField.NameCol]?.ToString() ?? string.Empty : string.Empty);
                                   }
                                   else
                                   {
                                       table.AddCell(string.Empty);
                                   }
                               }



                           }

                           if (endTable)
                           {
                               doc.Add(table);
                               startTable = true;
                           }
                           doc.NewPage();
                       }

                       doc.Close();
                   }
               }




               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<AutoCompleteItem>> GetAutoCompletes(clsOntologyItem reportOItem, List<ReportField> reportFields, string query, IMessageOutput messageOutput = null)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<AutoCompleteItem>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new AutoCompleteItem()
                };

                var getFilterItemsReqest = new GetFilterItemsRequest(FilterRequestType.GetFiltersByReport)
                {
                    ReportOItem = reportOItem,
                    MessageOutput = messageOutput
                };

                var getFilterItemsResult = await GetFilterItems(getFilterItemsReqest);

                result.ResultState = getFilterItemsResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var factory = new AutoCompleteFactory(Globals);

                messageOutput?.OutputInfo("Getting the autocomplete-model...");
                var factoryResult = await factory.GetAutoCompleteFilter(getFilterItemsResult.Result, reportFields, query);

                result.ResultState = factoryResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                result.Result = factoryResult.Result;

                messageOutput?.OutputInfo("Have the autocomplete-model.");

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SavePattern(clsOntologyItem reportItem, List<ReportField> reportFields, string text, IMessageOutput messageOut = null)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = Globals.LState_Success.Clone();

                var elasticAgent = new ServiceAgentElastic(Globals);

                if (string.IsNullOrEmpty(text))
                {
                    result = Globals.LState_Nothing.Clone();
                    result.Additional1 = "The query is empty! It will not be saved!";
                    messageOut?.OutputError(result.Additional1);
                    return result;
                }

                var getReportFilterItem = new GetFilterItemsRequest(FilterRequestType.GetFiltersByReport)
                {
                    ReportOItem = reportItem,
                    MessageOutput = messageOut
                };

                messageOut?.OutputInfo("Get ReportFilterItemModel...");
                var getAutoCompleteModelResult = await elasticAgent.GetReportFilterItemModel(getReportFilterItem, false);

                result = getAutoCompleteModelResult.ResultState;

                if (result.GUID == Globals.LState_Error.GUID)
                {
                    messageOut?.OutputError(result.Additional1);
                    return result;
                }

                messageOut?.OutputInfo("Have ReportFilterItemModel.");

                if (getAutoCompleteModelResult.Result.FilterItemValues.Any(pat => pat.Val_String == text))
                {
                    result = Globals.LState_Nothing;
                    result.Additional1 = "The Pattern is existing and will not be saved!";
                    messageOut?.OutputInfo(result.Additional1);
                    return result;
                }

                messageOut?.OutputInfo("Create Filter-Object and relation to Report...");
                var relationConfig = new clsRelationConfig(Globals);
                var patternObject = new clsOntologyItem
                {
                    GUID = Globals.NewGUID,
                    Name = text.Length > 255 ? text.Substring(0, 254) : text,
                    GUID_Parent = Config.LocalData.Class_Report_Filter.GUID,
                    Type = Globals.Type_Object
                };
                var saveObjects = new List<clsOntologyItem>
            {
                patternObject
            };

                var saveRelations = new List<clsObjectRel>
            {
                relationConfig.Rel_ObjectRelation(patternObject,
                reportItem,
                Config.LocalData.RelationType_belongs_to
                )
            };

                messageOut?.OutputInfo("Created Filter-Object and relation to Report.");
                messageOut?.OutputInfo("Get Report-Field...");
                var reportField = reportFields.FirstOrDefault(repField => text.ToLower().Contains(repField.NameCol));
                if (reportField == null)
                {
                    reportField = reportFields.FirstOrDefault();
                }

                if (reportField == null)
                {
                    result = Globals.LState_Error.Clone();
                    result.Additional1 = "No field found!";
                    messageOut?.OutputError(result.Additional1);
                }

                messageOut?.OutputInfo("Have Report-Field.");

                saveRelations.Add(relationConfig.Rel_ObjectRelation(patternObject,
                    new clsOntologyItem
                    {
                        GUID = reportField.IdReportField,
                        Name = reportField.NameReportField,
                        GUID_Parent = Config.LocalData.Class_Report_Field.GUID,
                        Type = Globals.Type_Object
                    },
                    Config.LocalData.RelationType_contains
                    ));

                var saveAttributes = new List<clsObjectAtt>
                {
                    relationConfig.Rel_ObjectAttribute(patternObject, Config.LocalData.AttributeType_Value, text)
                };

                var objectSaveResult = await elasticAgent.SaveObjects(saveObjects);
                result = objectSaveResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving the Pattern-Object!";
                    return result;
                }

                var attributeSaveResult = await elasticAgent.SaveAttributes(saveAttributes);
                result = attributeSaveResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while saving the Pattern-Attribute!";
                    return result;
                }

                var relationSaveResult = await elasticAgent.SaveRelations(saveRelations);
                result = relationSaveResult;
                if (result.GUID == Globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while relation between Textparser and Pattern!";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<ReportFilterItem>>> GetFilterItems(GetFilterItemsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<ReportFilterItem>>>(async () =>
           {
               var result = new ResultItem<List<ReportFilterItem>>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new List<ReportFilterItem>()
               };

               request.MessageOutput?.OutputInfo("Validate request...");

               result.ResultState = ValidationController.ValidateGetFilterItemsRequest(request, Globals);

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               request.MessageOutput?.OutputInfo("Validated request.");

               var elasticAgent = new ServiceAgentElastic(Globals);


               request.MessageOutput?.OutputInfo("Get model...");
               var modelResult = await elasticAgent.GetReportFilterItemModel(request, result.ResultState.Mark.Value);
               result.ResultState = modelResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Have model.");

               request.MessageOutput?.OutputInfo("Create List of Filter-Items...");
               result = await ReportFilterItemsFactory.CreateReportFilterItems(modelResult.Result, Globals);
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               request.MessageOutput?.OutputInfo("Created List of Filter-Items.");

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<ReportSortItem>>> GetSortItems(GetSortItemsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<ReportSortItem>>>(async () =>
            {
                var result = new ResultItem<List<ReportSortItem>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<ReportSortItem>()
                };

                request.MessageOutput?.OutputInfo("Validate request...");

                result.ResultState = ValidationController.ValidateGetSortItemsRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Validated request.");

                var elasticAgent = new ServiceAgentElastic(Globals);


                request.MessageOutput?.OutputInfo("Get model...");
                var modelResult = await elasticAgent.GetReportSortItemModel(request);
                result.ResultState = modelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have model.");

                request.MessageOutput?.OutputInfo("Create List of Sort-Items...");
                result = await ReportSortItemsFactory.CreateReportSortItems(modelResult.Result, Globals);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Created List of Sort-Items.");

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<VariableToField>>> GetVariablesToField(GetVariablesToFieldsRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<List<VariableToField>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<VariableToField>()
                };

                if (!request.VariablesToFieldsOItems.Any())
                {
                    request.MessageOutput?.OutputInfo("No Variable-Field-Mapping provided!");
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validate Request...");
                result.ResultState = ValidationController.ValidateGetVariablesToFieldsRequest(request, Globals);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Validated Request.");

                var elasticAgent = new ServiceAgentElastic(Globals);

                var serviceResult = await elasticAgent.GetVariableToFieldModel(request);

                result.ResultState = serviceResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var factory = new VariableMapFactory(Globals);

                var factoryResult = await factory.CreateVariableToFieldMaps(serviceResult.Result);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                result.Result = factoryResult.Result;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<MoveObjectsByReportResult>> MoveObjectsByReport(MoveObjectsByReportRequest request)
        {
            var taskResult = await Task.Run(async () =>
            {
                var result = new ResultItem<MoveObjectsByReportResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new MoveObjectsByReportResult()
                };

                request.MessageOutput?.OutputInfo("Validate request...");

                result.ResultState = ValidationController.ValidateMoveObjectsByReportRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated request.");

                if (request.CancellationToken.IsCancellationRequested)
                {
                    request.MessageOutput?.OutputInfo("User cancelled!");
                    return result;
                }

                var elasticAgent = new ServiceAgentElastic(Globals);

                request.MessageOutput?.OutputInfo("Get model...");

                var modelResult = await elasticAgent.GetMoveObjectsByReportModel(request);

                result.ResultState = modelResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have model.");

                if (request.CancellationToken.IsCancellationRequested)
                {
                    request.MessageOutput?.OutputInfo("User cancelled!");
                    return result;
                }

                request.MessageOutput?.OutputInfo($"Get report {modelResult.Result.Report.Name}...");
                var getReportResult = await GetReport(modelResult.Result.Report);
                result.ResultState = getReportResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                if (request.CancellationToken.IsCancellationRequested)
                {
                    request.MessageOutput?.OutputInfo("User cancelled!");
                    return result;
                }

                var reportField = getReportResult.Result.ReportFields.FirstOrDefault(field => field.IdReportField == modelResult.Result.ReportFieldId.GUID);

                if (reportField == null)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Related Report-Field cannot be found in Report!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var reportFilter = getReportResult.Result.ReportFilters.FirstOrDefault(filt => filt.FilterItem.GUID == modelResult.Result.ReportFilter.GUID);

                var sbMessage = new StringBuilder();

                sbMessage.AppendLine($"Have Report with Report-Field {reportField.NameReportField}");
                if (reportFilter != null)
                {
                    sbMessage.AppendLine($"Filter: {reportFilter.Value.Val_String}");
                }

                request.MessageOutput?.OutputInfo(sbMessage.ToString());

                var idDestClass = modelResult.Result.DestinationClass.GUID;

                try
                {
                    request.MessageOutput?.OutputInfo("Get Raw data from report...");
                    var dataResult = await LoadData(getReportResult.Result.Report, getReportResult.Result.ReportFields, reportFilter?.Value.Val_String);
                    request.MessageOutput?.OutputInfo($"Have {dataResult.Result.Count} rows");
                    var objectIds = dataResult.Result.Where(res => res.ContainsKey(reportField.NameCol)).Select(res => res[reportField.NameCol].ToString()).GroupBy(id => id).Select(idGrp => idGrp.Key).ToList();
                    request.MessageOutput?.OutputInfo($"Have {objectIds} object-Ids");

                    request.MessageOutput?.OutputInfo($"Save session-log...");
                    var sessionStamp = DateTime.Now;
                    var sessionObject = new clsOntologyItem
                    {
                        GUID = Globals.NewGUID,
                        Name = sessionStamp.ToString("yyyyMMddHHmmss"),
                        GUID_Parent = MoveObjects.Config.LocalData.Class_Movelog__Objects_by_Report_.GUID,
                        Type = Globals.Type_Object
                    };

                    var relationConfig = new clsRelationConfig(Globals);
                    var sessionSaveResult = await elasticAgent.SaveObjects(new List<clsOntologyItem> { sessionObject });
                    result.ResultState = sessionSaveResult;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the Session-Object!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    var sessionStampAttribute = relationConfig.Rel_ObjectAttribute(sessionObject, MoveObjects.Config.LocalData.AttributeType_Session_Stamp, sessionStamp);
                    result.ResultState = await elasticAgent.SaveAttributes(new List<clsObjectAtt>
                    {
                        sessionStampAttribute
                    });

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the Session-Stamp!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    var configToSessionLog = relationConfig.Rel_ObjectRelation(sessionObject, modelResult.Result.Config, MoveObjects.Config.LocalData.RelationType_belongs_to);
                    var relationsToSave = new List<clsObjectRel>
                    {
                        configToSessionLog
                    };


                    result.ResultState = await elasticAgent.SaveRelations(relationsToSave);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the relation between log and config!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    request.MessageOutput?.OutputInfo("Saved Session-log.");

                    var count = objectIds.Count;
                    var pageSize = 500;
                    var skip = 0;

                    var objectEditController = new ObjectEditController(Globals);
                    while (count > 0)
                    {
                        if (count < 500)
                        {
                            pageSize = count;
                        }

                        var objectIdPage = objectIds.Skip(skip).Take(pageSize).ToList();

                        var moveObjectsRequest = new MoveObjectListRequest
                        {
                            MessageOutput = request.MessageOutput,
                            ObjectIds = objectIdPage,
                            IdDestClass = idDestClass
                        };

                        var moveObjectsResult = await objectEditController.MoveObjectList(moveObjectsRequest);

                        count -= pageSize;

                        request.MessageOutput?.OutputInfo($"Moved {moveObjectsResult.Result.MovedItems.OItems.Count} objects, {count} objects left.");

                        skip += pageSize;
                    }
                }
                catch (Exception ex)
                {

                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = ex.Message;
                }


                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<GetObjectsFromReportResult>> GetObjectsFromReport(GetObjectsFormReportRequest request)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = new ResultItem<GetObjectsFromReportResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new GetObjectsFromReportResult()
                };

                request.MessageOutput?.OutputInfo("Validate request...");
                result.ResultState = ValidationController.ValidateGetObjectsFromReportRequest(request, Globals);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Validated request.");

                request.MessageOutput?.OutputInfo("Get Model...");

                var elasticAgent = new ServiceAgentElastic(Globals);

                var modelResult = await elasticAgent.GetObjectsFromReportModel(request);

                result.ResultState = modelResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }
                request.MessageOutput?.OutputInfo("Have Model.");


                request.MessageOutput?.OutputInfo("Get Report...");

                var reportResult = await GetReport(modelResult.Result.Report);
                result.ResultState = reportResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Have Report.");

                var reportFields = (from reportFieldReport in reportResult.Result.ReportFields
                                    join reportFieldObjects in modelResult.Result.ReportFields on reportFieldReport.IdReportField equals reportFieldObjects.GUID
                                    select reportFieldObjects).ToList();

                if (reportFields.Count != modelResult.Result.ReportFields.Count)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = $"Not all Report-Fields are attached to the Report!";
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                ReportFilterItem filterItem = null;

                if (modelResult.Result.ReportFilter != null)
                {
                    filterItem = reportResult.Result.ReportFilters.FirstOrDefault(filter => filter.FilterItem.GUID == modelResult.Result.ReportFilter.GUID);

                    if (filterItem == null)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "The provided Filter is no Report-Field!";
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                }

                var dataResult = await LoadData(reportResult.Result.Report, reportResult.Result.ReportFields, filterItem?.Value?.Val_String);
                result.ResultState = dataResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                var objectIds = new List<string>();
                foreach (var row in dataResult.Result)
                {
                    foreach (var field in reportFields)
                    {
                        if (row.ContainsKey(field.Name))
                        {
                            var objectId = row[field.Name].ToString();
                            if (!objectIds.Contains(objectId))
                            {
                                objectIds.Add(row[field.Name].ToString());
                            }
                        }
                    }
                }

                if (objectIds.Any())
                {
                    var objectsResult = await elasticAgent.GetOItems(objectIds, Globals.Type_Object);
                    result.ResultState = objectsResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }
                    (from objectId in objectIds
                     join objectItem in objectsResult.Result on objectId equals objectItem.GUID
                     select new { objectId, objectItem }).ToList().ForEach(objMap =>
                     {
                         
                         if (objMap.objectItem == null)
                         {
                             var objectItem = new clsOntologyItem
                             {
                                 GUID = objMap.objectId,
                                 Type = Globals.Type_Object
                             };
                             result.Result.InvlidObjects.Add(objectItem);
                         }
                         else
                         {
                             var objectItem = objMap.objectItem;
                             result.Result.ValidObjects.Add(objectItem);
                         }
                     });
                }

                return result;
            });

            return taskResult;
        }

        public ReportController(Globals globals, bool isAspNetProject) : base(globals)
        {
            serviceAgent = new Services.ServiceAgentElastic(Globals);
            serviceAgentReport = new Services.ServiceAgentElasticReport(Globals);
            serviceAgentMsSQL = new Services.ServiceAgentMSSQL(Globals);
            reportViewFactory = new Factories.ReportViewFactory(Globals);
            reportGridFactory = new Factories.ReportGridFactory(Globals);
        }
    }

    public class ReportConfig
    {
        public ReportItem ReportItem { get; set; }
        public List<ReportField> ReportFields { get; set; } = new List<ReportField>();

        public List<ReportFilterItem> ReportFilters { get; set; } = new List<ReportFilterItem>();

        public GetRowDiffResult RowDiff { get; set; }
        public Dictionary<string, object> GridConfig { get; set; }
    }
}
