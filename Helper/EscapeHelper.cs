﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Helper
{
    public static class EscapeHelper
    {
        public static string EscapeStringForMsSQLName(string source)
        {
			source = source.Replace(" ", "_");
			source = source.Replace(":", "_");
			source = source.Replace("-", "_");
			source = source.Replace("(", "_");
			source = source.Replace(")", "_");
			source = source.Replace("/", "_");
			source = source.Replace("\\", "_");
			source = source.Replace(">", "_");
			source = source.Replace("*", "_");
			source = source.Replace(".", "_");
			source = source.Replace("_", "_");
			source = source.Replace(",", "_");
			source = source.Replace("\"", "_");
			source = source.Replace("&", "_");
			source = source.Replace("+", "_");
			source = source.Replace("%", "_");

			return source;
		}
    }
}
