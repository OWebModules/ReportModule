﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Primitives
{
    public static class LogMessages
    {
        public static string GetRowDiff_ValidateRequest => "Validate request...";
        public static string GetRowDiff_ValidatedRequest => "Validated request.";

        public static string GetRowDiff_GetRowDiffs => "Get Row Diffs...";

        public static string GetRowDiff_ErrorGetRowDiffs => "Error while getting the Row Diffs!";

        public static string GetRowDiff_HaveRowDiffs => "Have Row Diffs.";

        public static string GetRowDiff_GetRowDiffFields => "Get Row Diff fields...";

        public static string GetRowDiff_ErrorGetRowDiffFields => "Error while getting the Row Diffs!";

        public static string GetRowDiff_HaveRowDiffFields => "Have Row Diff fields.";

        public static string ValidateGetRowDiffRequest_NoReport => "No Report provided!";

        public static string ValidateGetRowDiffResult_EmptyField => "Not all Fields are provided. You must assign field 1 with OrderId 1 and field 2 with OrderId 2!";

        public static string ValidateGetRowDiffRequest_NoReportFields => "No Report-Fields provided!";
        public static string SyncColumns_GetReport => "Get Report...";
        public static string SyncColumns_HaveReport => "Have Report.";
        public static string SyncColumns_GetReportAndFields => "Get Report and Fields...";
        public static string SyncColumn_HaveReportAndFields => "Have Report and Fields.";
        public static string SyncColumns_GetMSSQLColumns => "Get MS-SQL Columns...";

        public static string SyncColumns_HaveMSSQLColumns => "Have MS-SQL Columns.";

        public static string SyncColumns_GetMissingColumns => "Get missing Columns...";

        public static string SyncColumns_HaveMissingColumns => "Have missing Columns {0} of {1}";

        public static string SyncColumns_GetDataMap => "Get the Datamaps...";

        public static string SyncColumns_HaveDataMap => "Have {0} Datamaps.";

        public static string SyncColumns_GetItemsToSave => "Get the items to save...";

        public static string SyncColumns_HaveItemsToSave => "Have {0} Objects, {1} Attributes, {2} Relations.";

        public static string SyncColumns_SaveItems => "Save Items...";

        public static string SyncColumns_SavedItems => "Saved Items.";
    }
}
