﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Services
{
    public class ServiceAgentElasticReport
    {

        private Globals globals;

        private async Task<ResultItem<clsOntologyItem>> GetReport(clsOntologyItem oItemRef)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchReportLeftRight = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = oItemRef.GUID,
                        ID_Parent_Other = Config.LocalData.Class_Reports.GUID
                    }
                };

                var dbReaderLeftRight = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderLeftRight.GetDataObjectRel(searchReportLeftRight);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                if (dbReaderLeftRight.ObjectRels.Any())
                {
                    result.Result = dbReaderLeftRight.ObjectRels.Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = globals.Type_Object
                    }).First();

                    return result;
                }


                var searchReportRightLeft = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = oItemRef.GUID,
                        ID_Parent_Object = Config.LocalData.Class_Reports.GUID
                    }
                };

                var dbReaderRightLeft = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderRightLeft.GetDataObjectRel(searchReportRightLeft);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                if (!dbReaderRightLeft.ObjectRels.Any())
                {
                    result.ResultState = globals.LState_Nothing.Clone();
                    return result;
                };

                result.Result = dbReaderRightLeft.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Object,
                    Name = rel.Name_Object,
                    GUID_Parent = rel.ID_Parent_Object,
                    Type = globals.Type_Object
                }).First();

                return result;
            });

            return taskResult;

        }


        private async Task<ResultItem<List<clsObjectRel>>> GetReportType(clsOntologyItem oItemReport)
        {
            var taskResult = await Task.Run<ResultItem<List<clsObjectRel>>>(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone()
                };


                var dbReader = new OntologyModDBConnector(globals);

                var searchReportType = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = oItemReport.GUID,
                        ID_Parent_Other = Config.LocalData.ClassRel_Reports_is_of_Type_Report_Type.ID_Class_Right,
                        ID_RelationType = Config.LocalData.ClassRel_Reports_is_of_Type_Report_Type.ID_RelationType
                    }
                };

                result.ResultState = dbReader.GetDataObjectRel(searchReportType);
                result.Result = dbReader.ObjectRels;
                
                return result;
            });
            

            return taskResult;
        }

        public async Task<ResultItem<ReportFieldResult>> GetReportFields(clsOntologyItem reportItem)
        {
            var taskResult = await Task.Run<ResultItem<ReportFieldResult>>(async () =>
            {
                var result = new ResultItem<ReportFieldResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ReportFieldResult()
                };

                result.Result.Reports = new List<clsOntologyItem> { reportItem };
                if (reportItem.GUID_Parent != Config.LocalData.Class_Reports.GUID)
                {
                    var resultGetReport = await GetReport(reportItem);
                    result.ResultState = resultGetReport.ResultState;
                    result.Result.Reports = new List<clsOntologyItem> { resultGetReport.Result };
                    if (result.ResultState.GUID == globals.LState_Error.GUID || result.ResultState.GUID == globals.LState_Nothing.GUID)
                    {
                        return result;
                    }
                    result.Result.Reports = result.Result.Reports;
                }

                
                var resultGetReportType = await GetReportType(reportItem);
                result.ResultState = resultGetReportType.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.ReportToReportTypes = resultGetReportType.Result;

                var result001 = Get_001_ReportFieldsOfReport(reportItem);
                result.ResultState = result001.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.ReportFieldsOfReport = result001.Result;

                var result002 = Get_002_MSSQL_ReportToView(reportItem);
                result.ResultState = result002.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.ReportToViews = result002.Result;

                var result003 = Get_003_MSSQL_ReportFieldsToInvisible(result.Result.ReportFieldsOfReport);
                result.ResultState = result003.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.ReportFieldsToInvisible = result003.Result;

                var result004 = Get_004_MSSQL_ReportFieldsToColumn(result.Result.ReportFieldsOfReport);
                result.ResultState = result004.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.ReportFieldsToColumns = result004.Result;

                var result005 = Get_005_MSSQL_ColumnToView(result.Result.ReportFieldsToColumns);
                result.ResultState = result005.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.ColumnToViews = result005.Result;

                var result006 = Get_006_MSSQL_DatabaseOnServerToViews(result.Result.ReportToViews);
                result.ResultState = result006.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.DatabaseOnServerToViews = result006.Result;

                var result007 = Get_007_MSSQL_DBOnServerToDatabase(result.Result.DatabaseOnServerToViews);
                result.ResultState = result007.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.DBOnServerToDatabases = result007.Result;

                var result008 = Get_008_MSSQL_DBOnServerToServer(result.Result.DatabaseOnServerToViews);
                result.ResultState = result008.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.DBOnServerToServers = result008.Result;

                var result008a = Get_008a_MSSQL_DBOnServerToInstance(result.Result.DatabaseOnServerToViews);
                result.ResultState = result008.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.DBOnServerToInstance = result008a.Result;

                var result009 = Get_009_MSSQL_LeadFieldsToFields(result.Result.ReportFieldsOfReport);
                result.ResultState = result009.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.LeadFieldsToFields = result009.Result;

                var result010 = Get_010_MSSQL_TypeFieldToFields(result.Result.ReportFieldsOfReport);
                result.ResultState = result010.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.TypeFieldToFields = result010.Result;

                var result011 = Get_011_MSSQL_FieldsToFieldTypes(result.Result.ReportFieldsOfReport);
                result.ResultState = result011.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.FieldsToFieldTypes = result011.Result;

                var result012 = Get_012_MSSQL_FieldsToFieldFormats(result.Result.ReportFieldsOfReport);
                result.ResultState = result012.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.FieldsToFieldFormats = result012.Result;

                var result013a = Get_013_MSSQL_FieldsToWidths(result.Result.ReportFieldsOfReport);
                result.ResultState = result013a.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.ReportFieldsToWidths = result013a.Result;

                var result013 = Get_013_ReportToAggregate(reportItem);
                result.ResultState = result013.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.ReportToAggregates = result013.Result;

                var result014 = Get_014_AggregateTypes(result.Result.ReportToAggregates);
                result.ResultState = result014.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.AggregateTypes = result014.Result;

                var result015 = Get_015_AggregateFields(result.Result.ReportToAggregates);
                result.ResultState = result015.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.AggregateFields = result015.Result;

                var result016 = Get_016_Templates(result.Result.ReportFieldsOfReport);
                result.ResultState = result016.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.Templates = result016.Result;

                var result017 = Get_017_TemplateHtml(result.Result.Templates);
                result.ResultState = result017.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.TemplateAttributes = result017.Result;

                var result018 = Get_018_MSSQL_ReportFieldsToNoEncode(result.Result.ReportFieldsOfReport);

                result.ResultState = result018.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.ReportFieldsToNoEncode = result018.Result;

                return result;
                
            });

            return taskResult;
            
        }

        private ResultItem<List<clsObjectRel>> Get_001_ReportFieldsOfReport(clsOntologyItem oItemReport)
        {

            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var dbReaderFields = new OntologyModDBConnector(globals);

            var searchReportFieldsOfReport = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = oItemReport.GUID,
                    ID_Parent_Object = Config.LocalData.ClassRel_Report_Field_belongs_to_Reports.ID_Class_Left,
                    ID_RelationType = Config.LocalData.ClassRel_Report_Field_belongs_to_Reports.ID_RelationType
                }
            };

            result.ResultState = dbReaderFields.GetDataObjectRel(searchReportFieldsOfReport);

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            result.Result = dbReaderFields.ObjectRels.OrderBy(field => field.OrderID).ToList();

            return result;

        }

        private ResultItem<List<clsObjectRel>> Get_002_MSSQL_ReportToView(clsOntologyItem oItemReport)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var dbReader = new OntologyModDBConnector(globals);

            var searchView = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = oItemReport.GUID,
                    ID_Parent_Other = Config.LocalData.ClassRel_Reports_is_DB_Views.ID_Class_Right,
                    ID_RelationType = Config.LocalData.ClassRel_Reports_is_DB_Views.ID_RelationType
                }
            };

            result.ResultState = dbReader.GetDataObjectRel(searchView);
            result.Result = dbReader.ObjectRels;

            return result;
        }

        private ResultItem<List<clsObjectAtt>> Get_003_MSSQL_ReportFieldsToInvisible(List<clsObjectRel> reportFieldsOfReport)
        {
            var result = new ResultItem<List<clsObjectAtt>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectAtt>()
            };

            var dbReaderInvisible = new OntologyModDBConnector(globals);
            var searchInvisibleAttributes = reportFieldsOfReport.Select(field => new clsObjectAtt
            {
                ID_Object = field.ID_Object,
                ID_AttributeType = Config.LocalData.ClassAtt_Report_Field_invisible.ID_AttributeType
            }).ToList();

            if (searchInvisibleAttributes.Any())
            {
                result.ResultState = dbReaderInvisible.GetDataObjectAtt(searchInvisibleAttributes);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result = dbReaderInvisible.ObjAtts;
            }
            else
            {
                result.Result = new List<clsObjectAtt>();
            }

            return result;
        }

        private ResultItem<List<clsObjectRel>> Get_004_MSSQL_ReportFieldsToColumn(List<clsObjectRel> reportFieldsOfReport)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var dbReader = new OntologyModDBConnector(globals);
            var searchColumns = reportFieldsOfReport.Select(field => new clsObjectRel
            {
                ID_Object = field.ID_Object,
                ID_RelationType = Config.LocalData.ClassRel_Report_Field_belongs_to_DB_Columns.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_Report_Field_belongs_to_DB_Columns.ID_Class_Right
            }).ToList();

            if (searchColumns.Any())
            {
                result.ResultState = dbReader.GetDataObjectRel(searchColumns);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                
                result.Result = dbReader.ObjectRels;
                

            }
            else
            {
                result.Result = new List<clsObjectRel>();
            }

            return result;

        }

        private ResultItem<List<clsObjectRel>> Get_005_MSSQL_ColumnToView(List<clsObjectRel> reportFieldsToColumns)
        {

            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };
                
            var dbReader = new OntologyModDBConnector(globals);

            var searchView = reportFieldsToColumns.Select(column => new clsObjectRel
            {
                ID_Object = column.ID_Other,
                ID_RelationType = Config.LocalData.ClassRel_DB_Columns_belongs_to_DB_Views.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_DB_Columns_belongs_to_DB_Views.ID_Class_Right
            }).ToList();

            if (searchView.Any())
            {
                result.ResultState = dbReader.GetDataObjectRel(searchView);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                
                result.Result = dbReader.ObjectRels;
                
            }
            else
            {
                result.Result = new List<clsObjectRel>();
            }

            return result;
        }

        private ResultItem<List<clsObjectRel>> Get_006_MSSQL_DatabaseOnServerToViews(List<clsObjectRel> reportToView)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var dbReader = new OntologyModDBConnector(globals);

            var searchDatabaseOnServer = reportToView.Select(view => new clsObjectRel
            {
                ID_Other = view.ID_Other,
                ID_RelationType = Config.LocalData.ClassRel_Database_on_Server_contains_DB_Views.ID_RelationType,
                ID_Parent_Object = Config.LocalData.ClassRel_Database_on_Server_contains_DB_Views.ID_Class_Left
            }).ToList();

            if (searchDatabaseOnServer.Any())
            {
                result.ResultState = dbReader.GetDataObjectRel(searchDatabaseOnServer);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                
                result.Result = dbReader.ObjectRels;
                
            }
            else
            {
                result.Result = new List<clsObjectRel>();
            }

            return result;
        }

        private ResultItem<List<clsObjectRel>> Get_007_MSSQL_DBOnServerToDatabase(List<clsObjectRel> dbOnServerToColumns)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var dbReader = new OntologyModDBConnector(globals);

            var searchDBOnServerToDatabase = dbOnServerToColumns.Select(dbOnServer => new clsObjectRel
            {
                ID_Object = dbOnServer.ID_Object,
                ID_RelationType = Config.LocalData.ClassRel_Database_on_Server_belongs_to_Database.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_Database_on_Server_belongs_to_Database.ID_Class_Right
            }).ToList();

            if (searchDBOnServerToDatabase.Any())
            {
                result.ResultState = dbReader.GetDataObjectRel(searchDBOnServerToDatabase);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                
                result.Result = dbReader.ObjectRels;
                
            }
            else
            {
                result.Result = new List<clsObjectRel>();
            }

            return result;
        }

        private ResultItem<List<clsObjectRel>> Get_008_MSSQL_DBOnServerToServer(List<clsObjectRel> dbOnServerToColumns)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var dbReader = new OntologyModDBConnector(globals);

            var searchDBOnServerToServer = dbOnServerToColumns.Select(dbOnServer => new clsObjectRel
            {
                ID_Object = dbOnServer.ID_Object,
                ID_RelationType = Config.LocalData.ClassRel_Database_on_Server_located_in_Server.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_Database_on_Server_located_in_Server.ID_Class_Right
            }).ToList();

            if (searchDBOnServerToServer.Any())
            {
                result.ResultState = dbReader.GetDataObjectRel(searchDBOnServerToServer);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                
                result.Result = dbReader.ObjectRels;
                
            }
            else
            {
                result.Result = new List<clsObjectRel>();
            }

            return result;
        }

        private ResultItem<List<clsObjectRel>> Get_008a_MSSQL_DBOnServerToInstance(List<clsObjectRel> dbOnServerToColumns)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var dbReader = new OntologyModDBConnector(globals);

            var searchDBOnServerToInstance = dbOnServerToColumns.Select(dbOnServer => new clsObjectRel
            {
                ID_Object = dbOnServer.ID_Object,
                ID_RelationType = Config.LocalData.ClassRel_Database_on_Server_needs_Database_Instance.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_Database_on_Server_needs_Database_Instance.ID_Class_Right
            }).ToList();

            if (searchDBOnServerToInstance.Any())
            {
                result.ResultState = dbReader.GetDataObjectRel(searchDBOnServerToInstance);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result = dbReader.ObjectRels;

            }
            else
            {
                result.Result = new List<clsObjectRel>();
            }

            return result;
        }

        private ResultItem<List<clsObjectRel>> Get_009_MSSQL_LeadFieldsToFields(List<clsObjectRel> reportFieldsOfReport)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var dbReader = new OntologyModDBConnector(globals);

            var searchFieldsToLeadFields = reportFieldsOfReport.Select(reportFieldOfReport => new clsObjectRel
            {
                ID_Other = reportFieldOfReport.ID_Object,
                ID_RelationType = Config.LocalData.ClassRel_Report_Field_leads_Report_Field.ID_RelationType,
                ID_Parent_Object = Config.LocalData.ClassRel_Report_Field_leads_Report_Field.ID_Class_Left
            }).ToList();

            if (searchFieldsToLeadFields.Any())
            {
                result.ResultState = dbReader.GetDataObjectRel(searchFieldsToLeadFields);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                
                result.Result = dbReader.ObjectRels;
                
            }
            else
            {
                result.Result = new List<clsObjectRel>();
            }

            return result;
        }

        private ResultItem<List<clsObjectRel>> Get_010_MSSQL_TypeFieldToFields(List<clsObjectRel> reportFieldsOfReport)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var dbReader = new OntologyModDBConnector(globals);

            var searchTypeFieldsToFields = reportFieldsOfReport.Select(reportFieldOfReport => new clsObjectRel
            {
                ID_Other = reportFieldOfReport.ID_Object,
                ID_RelationType = Config.LocalData.ClassRel_Report_Field_Type_Field_Report_Field.ID_RelationType,
                ID_Parent_Object = Config.LocalData.ClassRel_Report_Field_Type_Field_Report_Field.ID_Class_Left
            }).ToList();

            if (searchTypeFieldsToFields.Any())
            {
                result.ResultState = dbReader.GetDataObjectRel(searchTypeFieldsToFields);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                
                result.Result = dbReader.ObjectRels;
                
            }
            else
            {
                result.Result = new List<clsObjectRel>();
            }

            return result;
        }

        private ResultItem<List<clsObjectRel>> Get_011_MSSQL_FieldsToFieldTypes(List<clsObjectRel> reportFieldsOfReport)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var dbReader = new OntologyModDBConnector(globals);

            var searchFieldsToFieldTypes = reportFieldsOfReport.Select(reportFieldOfReport => new clsObjectRel
            {
                ID_Object = reportFieldOfReport.ID_Object,
                ID_RelationType = Config.LocalData.ClassRel_Report_Field_is_of_Type_Field_Type.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_Report_Field_is_of_Type_Field_Type.ID_Class_Right
            }).ToList();

            if (searchFieldsToFieldTypes.Any())
            {
                result.ResultState = dbReader.GetDataObjectRel(searchFieldsToFieldTypes);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                
                result.Result = dbReader.ObjectRels;
                
            }
            else
            {
                result.Result = new List<clsObjectRel>();
            }

            return result;
        }

        private ResultItem<List<clsObjectRel>> Get_012_MSSQL_FieldsToFieldFormats(List<clsObjectRel> reportFieldsOfReport)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var dbReader = new OntologyModDBConnector(globals);

            var searchFieldsToFieldFormats = reportFieldsOfReport.Select(reportFieldOfReport => new clsObjectRel
            {
                ID_Object = reportFieldOfReport.ID_Object,
                ID_RelationType = Config.LocalData.ClassRel_Report_Field_Formatted_by_Field_Format.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_Report_Field_Formatted_by_Field_Format.ID_Class_Right
            }).ToList();

            if (searchFieldsToFieldFormats.Any())
            {
                result.ResultState = dbReader.GetDataObjectRel(searchFieldsToFieldFormats);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                
                result.Result = dbReader.ObjectRels;
                
            }
            else
            {
                result.Result = new List<clsObjectRel>();
            }

            return result;
        }

        private ResultItem<List<clsObjectRel>> Get_013_MSSQL_FieldsToWidths(List<clsObjectRel> reportFieldsOfReport)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var dbReader = new OntologyModDBConnector(globals);

            var searchFieldsToWidths = reportFieldsOfReport.Select(reportFieldOfReport => new clsObjectRel
            {
                ID_Object = reportFieldOfReport.ID_Object,
                ID_RelationType = Config.LocalData.ClassRel_Report_Field_belonging_width.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_Report_Field_belonging_width.ID_Class_Right
            }).ToList();

            if (searchFieldsToWidths.Any())
            {
                result.ResultState = dbReader.GetDataObjectRel(searchFieldsToWidths);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result = dbReader.ObjectRels;

            }
            else
            {
                result.Result = new List<clsObjectRel>();
            }

            return result;
        }

        private ResultItem<List<clsObjectRel>> Get_013_ReportToAggregate(clsOntologyItem oItemReport)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var dbReaderAggregates = new OntologyModDBConnector(globals);

            var aggregatesOfReports = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = oItemReport.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Aggregate_belongs_to_Reports.ID_RelationType,
                    ID_Parent_Object = Config.LocalData.ClassRel_Aggregate_belongs_to_Reports.ID_Class_Left
                }
            };

            result.ResultState = dbReaderAggregates.GetDataObjectRel(aggregatesOfReports);

            if (result.ResultState.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            result.Result = dbReaderAggregates.ObjectRels.OrderBy(field => field.OrderID).ToList();

            return result;



        }

        private ResultItem<List<clsObjectRel>> Get_014_AggregateTypes(List<clsObjectRel> aggregatesOfReport)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var dbReader = new OntologyModDBConnector(globals);

            var searchAggregateTypes = aggregatesOfReport.Select(aggregate => new clsObjectRel
            {
                ID_Object = aggregate.ID_Object,
                ID_RelationType = Config.LocalData.ClassRel_Aggregate_is_of_Type_Aggregate_Type.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_Aggregate_is_of_Type_Aggregate_Type.ID_Class_Right
            }).ToList();

            if (searchAggregateTypes.Any())
            {
                result.ResultState = dbReader.GetDataObjectRel(searchAggregateTypes);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                
                result.Result = dbReader.ObjectRels;
                
            }
            else
            {
                result.Result = new List<clsObjectRel>();
            }

            return result;
        }

        private ResultItem<List<clsObjectRel>> Get_015_AggregateFields(List<clsObjectRel> aggregatesOfReport)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var dbReader = new OntologyModDBConnector(globals);

            var searchAggregateFields = aggregatesOfReport.Select(aggregate => new clsObjectRel
            {
                ID_Object = aggregate.ID_Object,
                ID_RelationType = Config.LocalData.ClassRel_Aggregate_belongs_to_Report_Field.ID_RelationType,
                ID_Parent_Other = Config.LocalData.ClassRel_Aggregate_belongs_to_Report_Field.ID_Class_Right
            }).ToList();

            if (searchAggregateFields.Any())
            {
                result.ResultState = dbReader.GetDataObjectRel(searchAggregateFields);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                
                result.Result = dbReader.ObjectRels;
                
            }
            else
            {
                result.Result = new List<clsObjectRel>();
            }

            return result;
        }

        private ResultItem<List<clsObjectRel>> Get_016_Templates(List<clsObjectRel> reportFieldsOfReport)
        {
            var result = new ResultItem<List<clsObjectRel>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectRel>()
            };

            var dbReader = new OntologyModDBConnector(globals);

            var searchTemplates = reportFieldsOfReport.Select(fieldToReport => new clsObjectRel
            {
                ID_Other = fieldToReport.ID_Object,
                ID_RelationType = Config.LocalData.ClassRel_Template__Telerik__belongs_to_Report_Field.ID_RelationType,
                ID_Parent_Object = Config.LocalData.ClassRel_Template__Telerik__belongs_to_Report_Field.ID_Class_Left
            }).ToList();

            if (searchTemplates.Any())
            {
                result.ResultState = dbReader.GetDataObjectRel(searchTemplates);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result = dbReader.ObjectRels;

            }
            else
            {
                result.Result = new List<clsObjectRel>();
            }

            return result;
        }

        private ResultItem<List<clsObjectAtt>> Get_017_TemplateHtml(List<clsObjectRel> templatesToReportFields)
        {
            var result = new ResultItem<List<clsObjectAtt>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectAtt>()
            };

            var dbReader = new OntologyModDBConnector(globals);

            var searchTemplates = templatesToReportFields.Select(templateToField => new clsObjectAtt
            {
                ID_Object = templateToField.ID_Object,
                ID_AttributeType = Config.LocalData.ClassAtt_Template__Telerik__HTML.ID_AttributeType
            }).ToList();

            if (searchTemplates.Any())
            {
                result.ResultState = dbReader.GetDataObjectAtt(searchTemplates);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result = dbReader.ObjAtts;

            }
            else
            {
                result.Result = new List<clsObjectAtt>();
            }

            return result;
        }

        private ResultItem<List<clsObjectAtt>> Get_018_MSSQL_ReportFieldsToNoEncode(List<clsObjectRel> reportFieldsOfReport)
        {
            var result = new ResultItem<List<clsObjectAtt>>
            {
                ResultState = globals.LState_Success.Clone(),
                Result = new List<clsObjectAtt>()
            };

            var dbReaderNoEncode = new OntologyModDBConnector(globals);
            var searchNoEncode = reportFieldsOfReport.Select(field => new clsObjectAtt
            {
                ID_Object = field.ID_Object,
                ID_AttributeType = Config.LocalData.AttributeType_Do_not_encode.GUID
            }).ToList();

            if (searchNoEncode.Any())
            {
                result.ResultState = dbReaderNoEncode.GetDataObjectAtt(searchNoEncode);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result = dbReaderNoEncode.ObjAtts;
            }
            else
            {
                result.Result = new List<clsObjectAtt>();
            }

            return result;
        }

        public ServiceAgentElasticReport(Globals globals)
        {
            this.globals = globals;
        }
    }

    public class ReportFieldResult
    {
        public List<clsOntologyItem> Reports { get; set; }
        public List<clsObjectRel> ReportToReportTypes { get; set; }
        public List<clsObjectRel> ReportFieldsOfReport { get; set; }
        public List<clsObjectRel> ReportToViews { get; set; }
        public List<clsObjectAtt> ReportFieldsToInvisible { get; set; }
        public List<clsObjectRel> ReportFieldsToColumns { get; set; }
        public List<clsObjectRel> ColumnToViews { get; set; }
        public List<clsObjectRel> DatabaseOnServerToViews { get; set; }
        public List<clsObjectRel> DBOnServerToDatabases { get; set; }
        public List<clsObjectRel> DBOnServerToServers { get; set; }
        public List<clsObjectRel> DBOnServerToInstance { get; set; }
        public List<clsObjectRel> LeadFieldsToFields { get; set; }
        public List<clsObjectRel> TypeFieldToFields { get; set; }
        public List<clsObjectRel> FieldsToFieldTypes { get; set; }
        public List<clsObjectRel> FieldsToFieldFormats { get; set; }
        public List<clsObjectRel> ReportToAggregates { get; set; }
        public List<clsObjectRel> AggregateTypes { get; set; }
        public List<clsObjectRel> AggregateFields { get; set; }

        public List<clsObjectRel> Templates { get; set; } = new List<clsObjectRel>();

        public List<clsObjectAtt> TemplateAttributes { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectRel> ReportFieldsToWidths { get; set; } = new List<clsObjectRel>();

        public List<clsObjectAtt> ReportFieldsToNoEncode { get; set; } = new List<clsObjectAtt>();

    }
}
