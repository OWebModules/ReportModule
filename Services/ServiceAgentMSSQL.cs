﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using ReportModule.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace ReportModule.Services
{
    public class ServiceAgentMSSQL
    {
        private Globals globals;

        public List<TableUpdateItem> TableUpdateItems { get; set; } = new List<TableUpdateItem>();

        public async Task<ResultItem<List<MSSQLColumn>>> GetColumns(ReportItem report)
        {
            var taskResult = await Task.Run<ResultItem<List<MSSQLColumn>>>(() =>
            {
                var result = new ResultItem<List<MSSQLColumn>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<MSSQLColumn>()
                };
                var dataSource = report.NameServer + (string.IsNullOrEmpty(report.NameInstance) ? "" : @"\" + report.NameInstance);
                var connectionStringSQLConnection = "Data Source=" + dataSource + ";Initial Catalog=" + report.NameDatabaseOrIndex +
                    ";Integrated Security=True";

                var dataTable = new OntoDbReports.dtbl_ColumnsDataTable();
                try
                {
                    var dataSet = new OntoDbReportsTableAdapters.dtbl_ColumnsTableAdapter();
                    dataSet.Connection = new SqlConnection(connectionStringSQLConnection);
                    dataSet.Fill(dataTable, report.NameDBViewOrEsType);
                }
                catch (Exception ex)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = ex.Message;
                    return result;
                }

                result.Result = dataTable.Rows.Cast<DataRow>().Select(row => new MSSQLColumn 
                { 
                    ColumnName = row["name"].ToString(), 
                    ColumnType = row["name_type"].ToString() ,
                    Length = Convert.ToInt32(row["max_length"])
                }).ToList();

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<Dictionary<string, object>>>> GetData(ReportItem report, List<ReportField> reportFields, string whereClause = null, string orderClause = null, List<AdditionalReportField> additionalFields = null)
        {
            
            var taskResult = await Task.Run<ResultItem<List<Dictionary<string, object>>>>(() =>
            {
                var dictList = new List<Dictionary<string, object>>();
                if (string.IsNullOrEmpty(report.NameDatabaseOrIndex) ||
                    string.IsNullOrEmpty(report.NameServer) ||
                    string.IsNullOrEmpty(report.NameDBViewOrEsType))
                {
                    return new ResultItem<List<Dictionary<string, object>>> { ResultState = globals.LState_Nothing.Clone(), Result = dictList };
                }
                try
                {
                    var dataSource = report.NameServer + (string.IsNullOrEmpty(report.NameInstance) ? "" : @"\" + report.NameInstance);
                    var connectionString = "Provider=SQLNCLI11;Data Source=" + dataSource + ";Initial Catalog=" + report.NameDatabaseOrIndex +
                        ";Integrated Security=SSPI;Connect Timeout=360";
                    var query = "SELECT * FROM [" + report.NameDatabaseOrIndex + "].[dbo].[" + report.NameDBViewOrEsType + "]";

                    if (!string.IsNullOrEmpty(whereClause))
                    {
                        query = $"{query}\n WHERE {whereClause}";
                    }

                    if (!string.IsNullOrEmpty(orderClause))
                    {
                        query = $"{query}\n ORDER BY {orderClause}";
                    }

                    DataSet dataSet = new DataSet();
                    var dataAdapter = new OleDbDataAdapter(query, connectionString);
                    dataAdapter.SelectCommand.CommandTimeout = 360;
                
                    dataAdapter.Fill(dataSet);

                    var dataTable = dataSet.Tables[0];

                    var count = 0;

                    var columns = dataTable.Columns.Cast<DataColumn>().ToList();

                    if (additionalFields != null && additionalFields.Any())
                    {
                        foreach (var field in additionalFields)
                        {
                            var dataType = typeof(string);
                            if (field.DataType == "date")
                            {
                                dataType = typeof(DateTime);
                            }
                            else if (field.DataType == "number")
                            {
                                dataType = typeof(double);
                            }
                            else if (field.DataType == "boolean")
                            {
                                dataType = typeof(Boolean);
                            }

                            var column = new DataColumn(field.Name, dataType);
                            columns.Add(column);
                        }
                    }

                    var fields = (from col in columns
                                  join repField in reportFields on col.ColumnName.ToLower() equals repField.NameCol.ToLower()
                                  select repField).ToList();

                    

                    dataTable.Rows.Cast<DataRow>().ToList().ForEach(row =>
                    {
                        var dict = new Dictionary<string, object>();
                        dict.Add("IdRow", Guid.NewGuid().ToString());

                        fields.ForEach(repField =>
                        {
                            if (repField.NameFieldType == "DateTime" ||
                                repField.NameFieldType == "Bit" ||
                                repField.NameFieldType == "Long" ||
                                repField.NameFieldType == "Zahl" ||
                                repField.DoNotEncode)
                            {
                                dict.Add(repField.NameCol, row[repField.NameCol]);
                            }
                            else
                            {
                                dict.Add(repField.NameCol, HttpUtility.HtmlEncode(row[repField.NameCol]));
                            }
                            
                        });

                        dictList.Add(dict);
                        count++;
                    });
                }
                catch (Exception ex)
                {

                    
                }
                

                return new ResultItem<List<Dictionary<string, object>>> { ResultState = globals.LState_Success.Clone(), Result = dictList };
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SyncSQLDbAttributes(List<clsClassAtt> classAttributes, ReportItem report)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var server = report.NameServer ?? globals.Server;
                var dbOrIndex = report.NameDatabaseOrIndex ?? globals.Rep_Database;
                var dataSource = server + (string.IsNullOrEmpty(report.NameInstance) ? "" : @"\" + report.NameInstance);
                var connectionStringSQLConnection = "Data Source=" + dataSource + ";Initial Catalog=" + dbOrIndex +
                    ";Integrated Security=True";
                var connectionStringOleDb = "Provider=SQLNCLI11;Data Source=" + dataSource + ";Initial Catalog=" + dbOrIndex +
                    ";Integrated Security=SSPI";

                var createATableAttT = new OntoDbReportsTableAdapters.create_BulkTable_attTTableAdapter();
                

                createATableAttT.Connection = new System.Data.SqlClient.SqlConnection(connectionStringSQLConnection);

                var connectionBulkImport = new System.Data.SqlClient.SqlConnection(connectionStringSQLConnection);

                SqlBulkCopy bulkCopy =
                    new SqlBulkCopy
                    (
                        connectionBulkImport,
                        SqlBulkCopyOptions.TableLock |
                        SqlBulkCopyOptions.FireTriggers |
                        SqlBulkCopyOptions.UseInternalTransaction,
                        null
                    );

                var dbReaderObjectClasses = new OntologyModDBConnector(globals);
                var dbReaderClassAttributes = new OntologyModDBConnector(globals);

                result = dbReaderClassAttributes.GetDataClassAtts(classAttributes);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                connectionBulkImport.Open();

                var dbReaderAttributeTypes = new OntologyModDBConnector(globals);
                result = dbReaderAttributeTypes.GetDataAttributeType();

                if (result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                foreach (var classAttribute in (from classAtt in dbReaderClassAttributes.ClassAtts
                                                join attType in dbReaderAttributeTypes.AttributeTypes on classAtt.ID_AttributeType equals attType.GUID
                                                select new { classAtt, attType }))
                {
                    
                    var dataType = "NVARCHAR";
                    var length = "MAX";
                    var typeForColumn = System.Type.GetType("System.String"); 

                    if (classAttribute.attType.ID_DataType == globals.DType_Bool.GUID)
                    {
                        dataType = "BIT";
                        length = "0";
                        typeForColumn = System.Type.GetType("System.Boolean"); 
                    }
                    else if (classAttribute.attType.ID_DataType == globals.DType_DateTime.GUID)
                    {
                        dataType = "DATETIME2";
                        length = "0";
                        typeForColumn = System.Type.GetType("System.DateTime");
                    }
                    else if (classAttribute.attType.ID_DataType == globals.DType_Int.GUID)
                    {
                        dataType = "BIGINT";
                        length = "0";
                        typeForColumn = System.Type.GetType("System.Int64");
                    }
                    else if (classAttribute.attType.ID_DataType == globals.DType_Real.GUID)
                    {
                        dataType = "Real";
                        length = "0";
                        typeForColumn = System.Type.GetType("System.Double");
                    }

                    createATableAttT.GetData(classAttribute.classAtt.Name_Class, classAttribute.classAtt.Name_AttributeType, dataType, length);

                    var attributeTableNameMasked = CreateAttributeTableName(classAttribute.classAtt.Name_Class, classAttribute.classAtt.Name_AttributeType);
                    var tableName = $"import.importAattT_{attributeTableNameMasked.Item1}_{attributeTableNameMasked.Item2}";

                    var dataTableAttribute = new DataTable();
                    dataTableAttribute.TableName = tableName;

                    var dataColumnPrimary = new DataColumn($"GUID_Attribute_{attributeTableNameMasked.Item2}", typeof(string));

                    dataColumnPrimary.AllowDBNull = false;
                    dataColumnPrimary.MaxLength = 36;

                    dataTableAttribute.Columns.Add(dataColumnPrimary);
                    dataTableAttribute.PrimaryKey = new DataColumn[] { dataColumnPrimary };

                    var dataColumn = new DataColumn($"GUID_AT_{attributeTableNameMasked.Item2}", typeof(string));
                    dataColumn.MaxLength = 255;
                    dataColumn.AllowDBNull = false;
                    dataTableAttribute.Columns.Add(dataColumn);

                    dataColumn = new DataColumn($"Name_AT_{attributeTableNameMasked.Item2}", typeof(string));
                    dataColumn.MaxLength = 255;
                    dataColumn.AllowDBNull = false;
                    dataTableAttribute.Columns.Add(dataColumn);

                    dataColumn = new DataColumn("GUID_Object", typeof(string));
                    dataColumn.MaxLength = 36;
                    dataColumn.AllowDBNull = false;
                    dataTableAttribute.Columns.Add(dataColumn);

                    dataColumn = new DataColumn("OrderID", typeof(long));
                    dataColumn.AllowDBNull = false;
                    dataTableAttribute.Columns.Add(dataColumn);

                    dataColumn = new DataColumn($"val", typeForColumn);
                    dataColumn.AllowDBNull = true;
                    dataTableAttribute.Columns.Add(dataColumn);

                    var searchAttributes = new List<clsObjectAtt>
                    {
                        new clsObjectAtt
                        {
                            ID_AttributeType = classAttribute.classAtt.ID_AttributeType,
                            ID_Class = classAttribute.classAtt.ID_Class
                        }
                    };

                    result = dbReaderObjectClasses.GetDataObjectAtt(searchAttributes);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var pos = 0;
                    var count = dbReaderObjectClasses.ObjAtts.Count;
                    var take = 500;

                    bulkCopy.DestinationTableName = dataTableAttribute.TableName;

                    do
                    {

                        if (dbReaderObjectClasses.ObjAtts.Count - (pos + take) < 0)
                        {
                            take = dbReaderObjectClasses.ObjAtts.Count - pos;

                        }
                        var packet = dbReaderObjectClasses.ObjAtts.GetRange(pos, take);

                        packet.ForEach(row =>
                        {
                            var addRow = true;
                            var rowItem = dataTableAttribute.NewRow();

                            rowItem[$"GUID_Attribute_{attributeTableNameMasked.Item2}"] = row.ID_Attribute;
                            rowItem[$"GUID_AT_{attributeTableNameMasked.Item2}"] = row.ID_AttributeType;
                            rowItem[$"Name_AT_{attributeTableNameMasked.Item2}"] = classAttribute.attType.Name;
                            rowItem["GUID_Object"] = row.ID_Object.Replace("-","");
                            if (row.ID_DataType == globals.DType_Bool.GUID)
                            {
                                rowItem["val"] = row.Val_Bit;
                                if (row.Val_Bit == null) addRow = false;
                            }
                            else if (row.ID_DataType == globals.DType_DateTime.GUID)
                            {
                                rowItem["val"] = row.Val_Datetime;
                                if (row.Val_Datetime == null) addRow = false;
                                if (row.Val_Datetime != null && (row.Val_Datetime.Value.Year < 1753 || row.Val_Datetime.Value.Year > 9999)) addRow = false;
                            }
                            else if (row.ID_DataType == globals.DType_Int.GUID)
                            {
                                rowItem["val"] = row.Val_Lng;
                                if (row.Val_Lng == null) addRow = false;
                            }
                            else if (row.ID_DataType == globals.DType_Real.GUID)
                            {
                                rowItem["val"] = row.Val_Real;
                                if (row.Val_Real == null) addRow = false;
                            }
                            else
                            {
                                rowItem["val"] = row.Val_String;
                                if (row.Val_String == null) addRow = false;
                            }


                            rowItem["OrderID"] = row.OrderID;
                            if (addRow)
                            {
                                dataTableAttribute.Rows.Add(rowItem);
                            }
                            
                        });
                        pos += take;
                        bulkCopy.WriteToServer(dataTableAttribute);
                        dataTableAttribute.Rows.Clear();
                    } while (pos < count);

                    TableUpdateItems.Add(new TableUpdateItem
                    {
                        UpdateItemType = UpdateItemType.Attribute,
                        Item1 = attributeTableNameMasked.Item1,
                        Item2 = attributeTableNameMasked.Item2
                    });
                    
                }


                connectionBulkImport.Close();

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SyncSQLDbRelationsOr(List<clsClassRel> classRelations, ReportItem report)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var server = report.NameServer ?? globals.Server;
                var dbOrIndex = report.NameDatabaseOrIndex ?? globals.Rep_Database;
                var dataSource = server + (string.IsNullOrEmpty(report.NameInstance) ? "" : @"\" + report.NameInstance);
                var connectionStringSQLConnection = "Data Source=" + dataSource + ";Initial Catalog=" + dbOrIndex +
                    ";Integrated Security=True";
                var connectionStringOleDb = "Provider=SQLNCLI11;Data Source=" + dataSource + ";Initial Catalog=" + dbOrIndex +
                    ";Integrated Security=SSPI";

                var createATableRelT = new OntoDbReportsTableAdapters.create_BulkTable_relTTableAdapter();
                var createATableRelT_OR = new OntoDbReportsTableAdapters.create_BulkTable_relT_ORTableAdapter();

                

                createATableRelT.Connection = new System.Data.SqlClient.SqlConnection(connectionStringSQLConnection);
                createATableRelT_OR.Connection = new System.Data.SqlClient.SqlConnection(connectionStringSQLConnection);

                var connectionBulkImport = new System.Data.SqlClient.SqlConnection(connectionStringSQLConnection);
                var connectionBulkImportOr = new System.Data.SqlClient.SqlConnection(connectionStringSQLConnection);

                SqlBulkCopy bulkCopy =
                    new SqlBulkCopy
                    (
                        connectionBulkImport,
                        SqlBulkCopyOptions.TableLock |
                        SqlBulkCopyOptions.FireTriggers |
                        SqlBulkCopyOptions.UseInternalTransaction,
                        null
                    );

                SqlBulkCopy bulkCopyOr =
                    new SqlBulkCopy
                    (
                        connectionBulkImport,
                        SqlBulkCopyOptions.TableLock |
                        SqlBulkCopyOptions.FireTriggers |
                        SqlBulkCopyOptions.UseInternalTransaction,
                        null
                    );

                
                var dbReaderClassRelations = new OntologyModDBConnector(globals);
                var dbReaderClassRelationsOr = new OntologyModDBConnector(globals);

                result = dbReaderClassRelations.GetDataClassRel(classRelations);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
                result = dbReaderClassRelationsOr.GetDataClassRel(classRelations, doOr: true);

                connectionBulkImport.Open();
                connectionBulkImportOr.Open();

                var classRelationsOr = dbReaderClassRelations.ClassRels;
                classRelationsOr.AddRange(dbReaderClassRelationsOr.ClassRels);

                var searchObjects = classRelationsOr.Select(clsRel => new clsObjectRel
                {
                    ID_Parent_Object = clsRel.ID_Class_Left,
                    ID_Parent_Other = clsRel.ID_Class_Right,
                    ID_RelationType = clsRel.ID_RelationType
                }).ToList();
                var dbReaderObjectRelations = new OntologyModDBConnector(globals);

                result = dbReaderObjectRelations.GetDataObjectRel(searchObjects);
                if (result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var classGroup = dbReaderObjectRelations.ObjectRels.GroupBy(rel => new { rel.ID_Parent_Object, rel.Name_Parent_Object, rel.ID_Parent_Other, rel.ID_RelationType, rel.Name_RelationType, rel.Name_Parent_Other }).Select(group => group.Key);
                var classes = classGroup.Where(group => !string.IsNullOrEmpty(group.ID_Parent_Other)
                                                && !string.IsNullOrEmpty(group.Name_Parent_Other) 
                                                && !string.IsNullOrEmpty(group.ID_RelationType) 
                                                && !string.IsNullOrEmpty(group.Name_RelationType))
                    .Select(relGrp => new clsClassRel { ID_Class_Left = relGrp.ID_Parent_Object, Name_Class_Left = relGrp.Name_Parent_Object, ID_Class_Right = relGrp.ID_Parent_Other, Name_Class_Right = relGrp.Name_Parent_Other, ID_RelationType = relGrp.ID_RelationType, Name_RelationType = relGrp.Name_RelationType });

                foreach (var classRelation in classes)
                {

                    createATableRelT.Connection = createATableRelT.Connection;
                    createATableRelT_OR.Connection = createATableRelT_OR.Connection;

                    createATableRelT_OR.GetData(classRelation.Name_Class_Left, classRelation.Name_RelationType);

                    var relationTableNameMasked = CreateRelationTableName(classRelation.Name_Class_Left, classRelation.Name_Class_Right, classRelation.Name_RelationType);

                    


                    var tableNameOr = $"import.importRelT_{relationTableNameMasked.Item1}_To__{relationTableNameMasked.Item3}";

                    var dataTableRelationTypeOr = new DataTable();
                    dataTableRelationTypeOr.TableName = tableNameOr;

                    var dataColumnPrimaryOr1 = new DataColumn($"GUID_L_{relationTableNameMasked.Item1}", typeof(string));

                    dataColumnPrimaryOr1.AllowDBNull = false;
                    dataColumnPrimaryOr1.MaxLength = 36;

                    dataTableRelationTypeOr.Columns.Add(dataColumnPrimaryOr1);

                    var dataColumnPrimaryOr2 = new DataColumn($"GUID_R", typeof(string));

                    dataColumnPrimaryOr2.AllowDBNull = false;
                    dataColumnPrimaryOr2.MaxLength = 36;

                    dataTableRelationTypeOr.Columns.Add(dataColumnPrimaryOr2);

                    var dataColumnPrimaryOr3 = new DataColumn($"GUID_RT_{relationTableNameMasked.Item3}", typeof(string));

                    dataColumnPrimaryOr3.AllowDBNull = false;
                    dataColumnPrimaryOr3.MaxLength = 36;

                    dataTableRelationTypeOr.Columns.Add(dataColumnPrimaryOr3);

                    dataTableRelationTypeOr.PrimaryKey = new DataColumn[] { dataColumnPrimaryOr1, dataColumnPrimaryOr2, dataColumnPrimaryOr3 };

                    var dataColumnOr = new DataColumn("Name_R", typeof(string));
                    dataColumnOr.AllowDBNull = false;
                    dataColumnOr.MaxLength = 255;
                    dataTableRelationTypeOr.Columns.Add(dataColumnOr);

                    dataColumnOr = new DataColumn("GUID_Parent_R", typeof(string));
                    dataColumnOr.AllowDBNull = false;
                    dataColumnOr.MaxLength = 36;
                    dataTableRelationTypeOr.Columns.Add(dataColumnOr);

                    dataColumnOr = new DataColumn("Name_Parent_R", typeof(string));
                    dataColumnOr.AllowDBNull = true;
                    dataColumnOr.MaxLength = 255;
                    dataTableRelationTypeOr.Columns.Add(dataColumnOr);

                    dataColumnOr = new DataColumn($"Name_RT_{relationTableNameMasked.Item3}", typeof(string));
                    dataColumnOr.AllowDBNull = false;
                    dataColumnOr.MaxLength = 255;
                    dataTableRelationTypeOr.Columns.Add(dataColumnOr);

                    dataColumnOr = new DataColumn("OrderID", typeof(long));
                    dataColumnOr.AllowDBNull = false;
                    dataTableRelationTypeOr.Columns.Add(dataColumnOr);

                    dataColumnOr = new DataColumn($"Exist", typeof(bool));
                    dataColumnOr.AllowDBNull = false;
                    dataColumnOr.DefaultValue = true;
                    dataTableRelationTypeOr.Columns.Add(dataColumnOr);


                    var relationTableNameMasked2 = CreateRelationTableName(classRelation.Name_Class_Left, classRelation.Name_Class_Right, classRelation.Name_RelationType);
                    var tableName = $"import.importRelT_{relationTableNameMasked2.Item1}_To_{relationTableNameMasked2.Item2}_{relationTableNameMasked2.Item3}";
                    createATableRelT.GetData(relationTableNameMasked2.Item1, relationTableNameMasked2.Item2, relationTableNameMasked2.Item3);

                    
                    var dataTableRelationType = new DataTable();
                    dataTableRelationType.TableName = tableName;

                    var dataColumnPrimary1 = new DataColumn($"GUID_L_{relationTableNameMasked2.Item1}", typeof(string));

                    dataColumnPrimary1.AllowDBNull = false;
                    dataColumnPrimary1.MaxLength = 36;

                    dataTableRelationType.Columns.Add(dataColumnPrimary1);

                    var dataColumnPrimary2 = new DataColumn($"GUID_R_{relationTableNameMasked2.Item2}", typeof(string));

                    dataColumnPrimary2.AllowDBNull = false;
                    dataColumnPrimary2.MaxLength = 36;

                    dataTableRelationType.Columns.Add(dataColumnPrimary2);

                    var dataColumnPrimary3 = new DataColumn($"GUID_RT_{relationTableNameMasked2.Item3}", typeof(string));

                    dataColumnPrimary3.AllowDBNull = false;
                    dataColumnPrimary3.MaxLength = 36;

                    dataTableRelationType.Columns.Add(dataColumnPrimary3);

                    dataTableRelationType.PrimaryKey = new DataColumn[] { dataColumnPrimary1, dataColumnPrimary2, dataColumnPrimary3 };

                    var dataColumn = new DataColumn($"Name_RT_{relationTableNameMasked2.Item3}", typeof(string));
                    dataColumn.MaxLength = 255;
                    dataColumn.AllowDBNull = false;
                    dataTableRelationType.Columns.Add(dataColumn);

                    dataColumn = new DataColumn("OrderID", typeof(long));
                    dataColumn.AllowDBNull = false;
                    dataTableRelationType.Columns.Add(dataColumn);

                    dataColumn = new DataColumn($"Exist", typeof(bool));
                    dataColumn.AllowDBNull = false;
                    dataColumn.DefaultValue = true;
                    dataTableRelationType.Columns.Add(dataColumn);

                    TableUpdateItems.Add(new TableUpdateItem
                    {
                        UpdateItemType = UpdateItemType.Relation,
                        Item1 = relationTableNameMasked2.Item1,
                        Item2 = relationTableNameMasked2.Item2,
                        Item3 = relationTableNameMasked2.Item3,
                        Id1 = classRelation.ID_Class_Left,
                        Id2 = classRelation.ID_Class_Right,
                        Id3 = classRelation.ID_RelationType,
                        Table1 = dataTableRelationType,
                        Table2 = dataTableRelationTypeOr
                    });

                    
                }


                var pos = 0;
                var count = dbReaderObjectRelations.ObjectRels.Count;
                var take = 5000;

                
                var baseSet = dbReaderObjectRelations.ObjectRels.OrderBy(rel => rel.ID_Parent_Object).ThenBy(rel => rel.ID_RelationType).ToList();

                var doubleItems = baseSet.Where(part => part.ID_Object == "ec0cc6bd43df4a2fb541e9e5175bcdb9");
                do
                {
                    if (baseSet.Count - (pos + take) < 0)
                    {
                        take = baseSet.Count - pos;

                    }
                    var packet = baseSet.GetRange(pos, take);

                    var objectRels = packet.Where(pack => pack.Ontology == globals.Type_Object && pack.ID_Parent_Other != null).ToList();

                    var rows = new List<RowManipulationItem>();
                    foreach (var row in objectRels)
                    {
                        var tableUpdateiTem = TableUpdateItems.FirstOrDefault(tableUpdateItem => tableUpdateItem.UpdateItemType == UpdateItemType.Relation && tableUpdateItem.Id1 == row.ID_Parent_Object && tableUpdateItem.Id2 == row.ID_Parent_Other && tableUpdateItem.Id3 == row.ID_RelationType);
                        if (tableUpdateiTem != null)
                        {
                            var rowItem = tableUpdateiTem.Table1.NewRow();
                            rowItem[$"GUID_L_{tableUpdateiTem.Item1}"] = row.ID_Object;
                            rowItem[$"GUID_R_{tableUpdateiTem.Item2}"] = row.ID_Other;
                            rowItem[$"GUID_RT_{tableUpdateiTem.Item3}"] = row.ID_RelationType;
                            rowItem[$"Name_RT_{tableUpdateiTem.Item3}"] = !string.IsNullOrEmpty(row.Name_RelationType) ? row.Name_RelationType : "-";
                            rowItem["OrderID"] = row.OrderID;
                            rowItem["Exist"] = 1;

                            var rowItemOr = tableUpdateiTem.Table2.NewRow();
                            rowItemOr[$"GUID_L_{tableUpdateiTem.Item1}"] = row.ID_Object;
                            rowItemOr["GUID_R"] = row.ID_Other;
                            rowItemOr[$"GUID_RT_{tableUpdateiTem.Item3}"] = row.ID_RelationType;
                            rowItemOr["Name_R"] = row.Name_Other.Length > 255 ? row.Name_Other.Substring(0,254) : row.Name_Other;
                            rowItemOr["GUID_Parent_R"] = row.ID_Parent_Other;
                            rowItemOr["Name_Parent_R"] = row.Name_Parent_Other;
                            rowItemOr[$"Name_RT_{tableUpdateiTem.Item3}"] = !string.IsNullOrEmpty(row.Name_RelationType) ? row.Name_RelationType : "-";
                            rowItemOr["OrderID"] = row.OrderID;
                            rowItemOr["Exist"] = 1;

                            rows.Add(new RowManipulationItem { TableUpdateItem = tableUpdateiTem, TableRow= rowItem, TableRowOr = rowItemOr });
                        }
                        
                    }


                    var tableUpdateItems = (from tableUpdateItem in TableUpdateItems
                                            join row in rows on tableUpdateItem equals row.TableUpdateItem
                                            select tableUpdateItem).GroupBy(tbl => tbl).Select(tbl => tbl.Key);
                    foreach (var tableUpdateItem in tableUpdateItems)
                    {
                        var rowItems = rows.Where(row => row.TableUpdateItem == tableUpdateItem);

                        bulkCopyOr.DestinationTableName = tableUpdateItem.Table2.TableName;
                        bulkCopy.DestinationTableName = tableUpdateItem.Table1.TableName;
                        bulkCopy.WriteToServer(rowItems.Select(row => row.TableRow).ToArray());
                        bulkCopyOr.WriteToServer(rowItems.Select(row => row.TableRowOr).ToArray());
                    }



                    var nonObjectRels = packet.Where(pack => pack.Ontology != globals.Type_Object).ToList();

                    if (nonObjectRels.Any())
                    {
                        var rowItems = new List<DataRow>();
                        foreach (var row in nonObjectRels)
                        {
                            var tableUpdateiTem = TableUpdateItems.FirstOrDefault(tableUpdateItem => tableUpdateItem.UpdateItemType == UpdateItemType.Relation && tableUpdateItem.Id1 == row.ID_Parent_Object && tableUpdateItem.Id3 == row.ID_RelationType);
                            if (tableUpdateiTem != null)
                            {
                                var rowItemOr = tableUpdateiTem.Table2.NewRow();
                                rowItemOr[$"GUID_L_{tableUpdateiTem.Item1}"] = row.ID_Object;
                                rowItemOr["GUID_R"] = row.ID_Other;
                                rowItemOr[$"GUID_RT_{tableUpdateiTem.Item3}"] = row.ID_RelationType;
                                rowItemOr["Name_R"] = row.Name_Other;
                                rowItemOr["GUID_Parent_R"] = row.ID_Parent_Other;
                                rowItemOr["Name_Parent_R"] = row.Name_Parent_Other;
                                rowItemOr[$"Name_RT_{tableUpdateiTem.Item3}"] = row.Name_RelationType;
                                rowItemOr["OrderID"] = row.OrderID;
                                rowItemOr["Exist"] = 1;
                                rowItems.Add(rowItemOr);
                            }
                        };

                        bulkCopyOr.WriteToServer(rowItems.ToArray());
                    }

                    pos += take;
                } while (pos < count);

                


                connectionBulkImport.Close();

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> UpdateTables(ReportItem report)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
            var result = globals.LState_Success.Clone();

            var server = report.NameServer ?? globals.Server;
            var dbOrIndex = report.NameDatabaseOrIndex ?? globals.Rep_Database;
            var dataSource = server + (string.IsNullOrEmpty(report.NameInstance) ? "" : @"\" + report.NameInstance);
                var connectionStringSQLConnection = "Data Source=" + dataSource + ";Initial Catalog=" + dbOrIndex +
                    ";Integrated Security=True";

                var updateAAttT = new OntoDbReportsTableAdapters.update_Table_attTTableAdapter();
                var updateAOrgT = new OntoDbReportsTableAdapters.update_Table_orgTTableAdapter();
                var updateARelT = new OntoDbReportsTableAdapters.update_Table_relTTableAdapter();
                var updateARelT_OR = new OntoDbReportsTableAdapters.update_Table_relT_ORTableAdapter();
                updateAAttT.Connection = new SqlConnection(connectionStringSQLConnection);
                updateAOrgT.Connection = updateAAttT.Connection;
                updateARelT.Connection = updateAAttT.Connection;
                updateARelT_OR.Connection = updateAAttT.Connection;

                

                foreach (var tableUpdateItem in TableUpdateItems)
                {
                    if (tableUpdateItem.UpdateItemType == UpdateItemType.Attribute)
                    {
                        updateAAttT.GetData(tableUpdateItem.Item1, tableUpdateItem.Item2);
                    }
                    else if (tableUpdateItem.UpdateItemType == UpdateItemType.Class)
                    {
                        updateAOrgT.GetData(tableUpdateItem.Item1);
                    }
                    else if (tableUpdateItem.UpdateItemType == UpdateItemType.Relation)
                    {
                        
                        updateARelT.GetData(tableUpdateItem.Item1, tableUpdateItem.Item2, tableUpdateItem.Item3);
                        
                        
                    }
                    
                }

                var relationOrItems = TableUpdateItems.Where(itm => itm.UpdateItemType == UpdateItemType.Relation).GroupBy(itm => new { itm.Item1, itm.Item3 }).Select(itm => itm.Key);
                foreach (var tableUpdateItem in relationOrItems)
                {
                    updateARelT_OR.GetData(tableUpdateItem.Item1, tableUpdateItem.Item3);
                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SyncSQLDbRelations(List<clsClassRel> classRelations, ReportItem report)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var server = report.NameServer ?? globals.Server;
                var dbOrIndex = report.NameDatabaseOrIndex ?? globals.Rep_Database;
                var dataSource = server + (string.IsNullOrEmpty(report.NameInstance) ? "" : @"\" + report.NameInstance);
                var connectionStringSQLConnection = "Data Source=" + dataSource + ";Initial Catalog=" + dbOrIndex +
                    ";Integrated Security=True";
                var connectionStringOleDb = "Provider=SQLNCLI11;Data Source=" + dataSource + ";Initial Catalog=" + dbOrIndex +
                    ";Integrated Security=SSPI";

                var createATableRelT = new OntoDbReportsTableAdapters.create_BulkTable_relTTableAdapter();

                

                createATableRelT.Connection = new System.Data.SqlClient.SqlConnection(connectionStringSQLConnection);

                var connectionBulkImport = new System.Data.SqlClient.SqlConnection(connectionStringSQLConnection);

                SqlBulkCopy bulkCopy =
                    new SqlBulkCopy
                    (
                        connectionBulkImport,
                        SqlBulkCopyOptions.TableLock |
                        SqlBulkCopyOptions.FireTriggers |
                        SqlBulkCopyOptions.UseInternalTransaction,
                        null
                    );

                var dbReaderObjectRelations = new OntologyModDBConnector(globals);
                var dbReaderClassRelations = new OntologyModDBConnector(globals);

                result = dbReaderClassRelations.GetDataClassRel(classRelations, doOr:false);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                connectionBulkImport.Open();

                classRelations = dbReaderClassRelations.ClassRels;


                foreach (var classRelation in classRelations)
                {

                    createATableRelT.Connection = createATableRelT.Connection;


                    createATableRelT.GetData(classRelation.Name_Class_Left, classRelation.Name_Class_Right, classRelation.Name_RelationType);

                    DataTable dataTableRelationType = null;
                    var relationTableNameMasked = CreateRelationTableName(classRelation.Name_Class_Left, classRelation.Name_Class_Right, classRelation.Name_RelationType);
                       
                    var tableName = $"import.importRelT_{relationTableNameMasked.Item1}_To_{relationTableNameMasked.Item2}_{relationTableNameMasked.Item3}";

                    dataTableRelationType = new DataTable();
                    dataTableRelationType.TableName = tableName;

                    var dataColumnPrimary1 = new DataColumn($"GUID_L_{relationTableNameMasked.Item1}", typeof(string));

                    dataColumnPrimary1.AllowDBNull = false;
                    dataColumnPrimary1.MaxLength = 36;

                    dataTableRelationType.Columns.Add(dataColumnPrimary1);

                    var dataColumnPrimary2 = new DataColumn($"GUID_R_{relationTableNameMasked.Item2}", typeof(string));

                    dataColumnPrimary2.AllowDBNull = false;
                    dataColumnPrimary2.MaxLength = 36;

                    dataTableRelationType.Columns.Add(dataColumnPrimary2);

                    var dataColumnPrimary3 = new DataColumn($"GUID_RT_{relationTableNameMasked.Item3}", typeof(string));

                    dataColumnPrimary3.AllowDBNull = false;
                    dataColumnPrimary3.MaxLength = 36;

                    dataTableRelationType.Columns.Add(dataColumnPrimary3);

                    dataTableRelationType.PrimaryKey = new DataColumn[] { dataColumnPrimary1, dataColumnPrimary2, dataColumnPrimary3 };

                    var dataColumn = new DataColumn($"Name_RT_{relationTableNameMasked.Item3}", typeof(string));
                    dataColumn.MaxLength = 255;
                    dataColumn.AllowDBNull = false;
                    dataTableRelationType.Columns.Add(dataColumn);

                    dataColumn = new DataColumn("OrderID", typeof(long));
                    dataColumn.AllowDBNull = false;
                    dataTableRelationType.Columns.Add(dataColumn);

                    dataColumn = new DataColumn($"Exist", typeof(bool));
                    dataColumn.AllowDBNull = false;
                    dataColumn.DefaultValue = true;
                    dataTableRelationType.Columns.Add(dataColumn);

                    var searchRelationTypes = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Parent_Object = classRelation.ID_Class_Left,
                            ID_Parent_Other = classRelation.ID_Class_Right,
                            ID_RelationType = classRelation.ID_RelationType
                        }
                    };

                    result = dbReaderObjectRelations.GetDataObjectRel(searchRelationTypes);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var pos = 0;
                    var count = dbReaderObjectRelations.ObjectRels.Count;
                    var take = 500;

                    bulkCopy.DestinationTableName = dataTableRelationType.TableName;

                    if (count > 0)
                    {
                        do
                        {
                            if (dbReaderObjectRelations.ObjectRels.Count - (pos + take) < 0)
                            {
                                take = dbReaderObjectRelations.ObjectRels.Count - pos;

                            }
                            var packet = dbReaderObjectRelations.ObjectRels.GetRange(pos, take);

                            packet.ForEach(row =>
                            {

                                var rowItem = dataTableRelationType.NewRow();
                                rowItem[$"GUID_L_{relationTableNameMasked.Item1}"] = row.ID_Object.Replace("-", "");
                                rowItem[$"GUID_R_{relationTableNameMasked.Item2}"] = row.ID_Other.Replace("-", "");
                                rowItem[$"GUID_RT_{relationTableNameMasked.Item3}"] = row.ID_RelationType;
                                rowItem[$"Name_RT_{relationTableNameMasked.Item3}"] = row.Name_RelationType;
                                rowItem["OrderID"] = row.OrderID;
                                rowItem["Exist"] = 1;
                                dataTableRelationType.Rows.Add(rowItem);


                            });

                            pos += take;
                            bulkCopy.WriteToServer(dataTableRelationType);
                            dataTableRelationType.Clear();
                        } while (pos < count);
                    }

                    TableUpdateItems.Add(new TableUpdateItem
                    {
                        UpdateItemType = UpdateItemType.Relation,
                        Item1 = relationTableNameMasked.Item1,
                        Item2 = relationTableNameMasked.Item2,
                        Item3 = relationTableNameMasked.Item3
                    });
                    
                }


                connectionBulkImport.Close();

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SyncSQLDBClasses(List<clsOntologyItem> classes, ReportItem report)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var server = report.NameServer ?? globals.Server;
                var dbOrIndex = report.NameDatabaseOrIndex ?? globals.Rep_Database;
                var dataSource = server + (string.IsNullOrEmpty(report.NameInstance) ? "" : @"\" + report.NameInstance);
                var connectionStringSQLConnection = "Data Source=" + dataSource + ";Initial Catalog=" + dbOrIndex +
                    ";Integrated Security=True";
                var connectionStringOleDb = "Provider=SQLNCLI11;Data Source=" + dataSource + ";Initial Catalog=" + dbOrIndex +
                    ";Integrated Security=SSPI";

                var createATableOrgT = new OntoDbReportsTableAdapters.create_BulkTable_orgTTableAdapter();
                

                createATableOrgT.Connection = new System.Data.SqlClient.SqlConnection(connectionStringSQLConnection);

                var connectionBulkImport = new System.Data.SqlClient.SqlConnection(connectionStringSQLConnection);

                SqlBulkCopy bulkCopy =
                    new SqlBulkCopy
                    (
                        connectionBulkImport,
                        SqlBulkCopyOptions.TableLock |
                        SqlBulkCopyOptions.FireTriggers |
                        SqlBulkCopyOptions.UseInternalTransaction,
                        null
                    );

                var dbReaderClasses = new OntologyModDBConnector(globals);
                var dbReaderObjects = new OntologyModDBConnector(globals);
                result = dbReaderClasses.GetDataClasses(classes);
                
                if (result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                connectionBulkImport.Open();

                foreach (var classItm in dbReaderClasses.Classes1)
                {
                    createATableOrgT.GetData(globals.Type_Class, classItm.Name);

                    var classNameMasked = CreateClassTableName(classItm.Name);
                    var tableName = $"import.importT_{classNameMasked}";

                    var dataTableObject = new DataTable();
                    dataTableObject.TableName = tableName;

                    var dataColumnPrimary = new DataColumn($"GUID_{classNameMasked}", typeof(string));

                    dataColumnPrimary.AllowDBNull = false;
                    dataColumnPrimary.MaxLength = 36;

                    dataTableObject.Columns.Add(dataColumnPrimary);
                    dataTableObject.PrimaryKey = new DataColumn[] { dataColumnPrimary };

                    var dataColumn = new DataColumn($"Name_{classNameMasked}", typeof(string));
                    dataColumn.MaxLength = 255;
                    dataColumn.AllowDBNull = false;
                    dataTableObject.Columns.Add(dataColumn);

                    dataColumn = new DataColumn($"GUID_Class_{classNameMasked}", typeof(string));
                    dataColumn.MaxLength = 36;
                    dataColumn.AllowDBNull = false;
                    dataTableObject.Columns.Add(dataColumn);

                    dataColumn = new DataColumn($"Name_Class_{classNameMasked}", typeof(string));
                    dataColumn.MaxLength = 255;
                    dataColumn.AllowDBNull = false;
                    dataTableObject.Columns.Add(dataColumn);

                    dataColumn = new DataColumn($"Exist", typeof(bool));
                    dataColumn.AllowDBNull = false;
                    dataColumn.DefaultValue = true;
                    dataTableObject.Columns.Add(dataColumn);


                    var searchObjects = new List<clsOntologyItem>
                    {
                        new clsOntologyItem
                        {
                            GUID_Parent = classItm.GUID
                        }
                    };

                    result = dbReaderObjects.GetDataObjects(searchObjects);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var count = dbReaderObjects.Objects1.Count;
                    var take = 500;

                    bulkCopy.DestinationTableName = dataTableObject.TableName;

                    var pos = 0;
                    do
                    {

                        if (dbReaderObjects.Objects1.Count - (pos + take) < 0)
                        {
                            take = dbReaderObjects.Objects1.Count - pos;

                        }
                        var packet = dbReaderObjects.Objects1.GetRange(pos, take);

                        packet.ForEach(row =>
                        {
                            var rowItem = dataTableObject.NewRow();

                            var guid = row.GUID.Replace("-", "");
                            if (guid.Length > 36)
                            {
                                guid = guid.Substring(0, 36);
                            }
                            rowItem[$"GUID_{classNameMasked}"] = guid;
                            rowItem[$"Name_{classNameMasked}"] = row.Name.Length > 255 ? row.Name.Substring(0, 254) : row.Name;
                            rowItem[$"GUID_Class_{classNameMasked}"] = row.GUID_Parent;
                            rowItem[$"Name_Class_{classNameMasked}"] = classItm.Name;

                            dataTableObject.Rows.Add(rowItem);
                        });

                        bulkCopy.WriteToServer(dataTableObject);
                        dataTableObject.Rows.Clear();
                        pos += take;
                    } while (pos < count);

                    TableUpdateItems.Add(new TableUpdateItem
                    {
                        UpdateItemType = UpdateItemType.Class,
                        Item1 = classItm.Name
                    });
                }

                
                connectionBulkImport.Close();
                return result;
            });

            return taskResult;
        }

        private string CreateClassTableName(string className)
        {
            var result = className.Replace(" ", "_").
                    Replace(":", "_").
                    Replace("-", "_").
                    Replace("(", "_").
                    Replace(")", "_").
                    Replace("/", "_").
                    Replace("\\", "_").
                    Replace(">", "_").
                    Replace("<", "_").
                    Replace("*", "_").
                    Replace(".", "_").
                    Replace(",", "_").
                    Replace("\"", "_").
                    Replace("&", "_").
                    Replace("+", "_").
                    Replace("%", "_");

            return result;
        }

        private TableUpdateItem CreateAttributeTableName(string className, string attributeTypeName)
        {
            className = className.Replace(" ", "").
                    Replace(":", "").
                    Replace("-", "").
                    Replace("(", "").
                    Replace(")", "").
                    Replace("/", "").
                    Replace("\\", "").
                    Replace(">", "").
                    Replace("<", "").
                    Replace("*", "").
                    Replace(".", "").
                    Replace(",", "").
                    Replace("\"", "").
                    Replace("&", "").
                    Replace("+", "").
                    Replace("%", "");
            attributeTypeName = attributeTypeName.Replace(" ", "_").
                    Replace(":", "_").
                    Replace("-", "_").
                    Replace("(", "_").
                    Replace(")", "_").
                    Replace("/", "_").
                    Replace("\\", "_").
                    Replace(">", "_").
                    Replace("<", "_").
                    Replace("*", "_").
                    Replace(".", "_").
                    Replace(",", "_").
                    Replace("\"", "_").
                    Replace("&", "_").
                    Replace("+", "_").
                    Replace("%", "_");


            var result = new TableUpdateItem
            {
                Item1 = className,
                Item2 = attributeTypeName,
                UpdateItemType = UpdateItemType.Attribute
            };

            return result;
        }

        private TableUpdateItem CreateRelationTableName(string className_Left, string className_Right, string relationTypeName)
        {
            className_Left = className_Left.Replace(" ", "").
                    Replace(":", "").
                    Replace("-", "").
                    Replace("(", "").
                    Replace(")", "").
                    Replace("/", "").
                    Replace("\\", "").
                    Replace(">", "").
                    Replace("<", "").
                    Replace("*", "").
                    Replace(".", "").
                    Replace(",", "").
                    Replace("\"", "").
                    Replace("&", "").
                    Replace("+", "").
                    Replace("%", "").
                    Replace(" ", "").
                    Replace("_", "");

            if (!string.IsNullOrEmpty(className_Right))
            {
                className_Right = className_Right.Replace(" ", "").
                    Replace(":", "").
                    Replace("-", "").
                    Replace("(", "").
                    Replace(")", "").
                    Replace("/", "").
                    Replace("\\", "").
                    Replace(">", "").
                    Replace("<", "").
                    Replace("*", "").
                    Replace(".", "").
                    Replace(",", "").
                    Replace("\"", "").
                    Replace("&", "").
                    Replace("+", "").
                    Replace("%", "").
                    Replace(" ", "").
                    Replace("_", "");
            }


            relationTypeName = relationTypeName.Replace(" ", "").
                    Replace(":", "").
                    Replace("-", "").
                    Replace("(", "").
                    Replace(")", "").
                    Replace("/", "").
                    Replace("\\", "").
                    Replace(">", "").
                    Replace("<", "").
                    Replace("*", "").
                    Replace(".", "").
                    Replace(",", "").
                    Replace("\"", "").
                    Replace("&", "").
                    Replace("+", "").
                    Replace("%", "").
                    Replace("_", "");



            var result = new TableUpdateItem
            {
                Item1 = className_Left,
                Item2 = className_Right,
                Item3 = relationTypeName,
                UpdateItemType = UpdateItemType.Relation
            };
            return result;
        }

        public ServiceAgentMSSQL(Globals globals)
        {
            this.globals = globals;
        }
    }
}
