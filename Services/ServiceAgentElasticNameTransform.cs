﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using ReportModule.Models;
using ReportModule.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Services
{
    public class ServiceAgentElasticNameTransform : ElasticBaseAgent
    {

        public async Task<ResultItem<NameTransformProviderModel>> GetNameTransformProviderModel(List<clsOntologyItem> configs)
        {
            var taskResult = await Task.Run<ResultItem<NameTransformProviderModel>>(() =>
           {
               var result = new ResultItem<NameTransformProviderModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new NameTransformProviderModel()
               };

               result.Result.NameTransformConfigs = configs;

               var searchReports = result.Result.NameTransformConfigs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = NameTransform.Config.LocalData.ClassRel_NameTransform_Provider__Reports__belonging_Reports.ID_RelationType,
                   ID_Parent_Other = NameTransform.Config.LocalData.ClassRel_NameTransform_Provider__Reports__belonging_Reports.ID_Class_Right
               }).ToList();

               var dbReaderReports = new OntologyModDBConnector(globals);

               if (searchReports.Any())
               {
                   result.ResultState = dbReaderReports.GetDataObjectRel(searchReports);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the related Reports!";
                       return result;
                   }
                   result.Result.ConfigToReports = dbReaderReports.ObjectRels;
               }

               result.ResultState = ValidationController.ValidateNameTransformProviderModel(result.Result, globals, nameof(NameTransformProviderModel.ConfigToReports));
               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               var searchTableTemplates = result.Result.NameTransformConfigs.Select(config => new clsObjectAtt
               {
                   ID_Object = config.GUID,
                   ID_AttributeType = NameTransform.Config.LocalData.AttributeType_Table_Template.GUID
               }).ToList();

               var dbReaderTableTemplates = new OntologyModDBConnector(globals);

               if (searchTableTemplates.Any())
               {
                   result.ResultState = dbReaderTableTemplates.GetDataObjectAtt(searchTableTemplates);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Table-Templates!";
                       return result;
                   }

                   result.Result.TableTemplates = dbReaderTableTemplates.ObjAtts;
               }

               var searchRowTemplates = result.Result.NameTransformConfigs.Select(config => new clsObjectAtt
               {
                   ID_Object = config.GUID,
                   ID_AttributeType = NameTransform.Config.LocalData.AttributeType_Row_Template.GUID
               }).ToList();

               var dbReaderRowTemplates = new OntologyModDBConnector(globals);

               if (searchRowTemplates.Any())
               {
                   result.ResultState = dbReaderRowTemplates.GetDataObjectAtt(searchRowTemplates);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Table-Templates!";
                       return result;
                   }

                   result.Result.RowTemplate = dbReaderRowTemplates.ObjAtts;
               }

               var searchStyles = result.Result.NameTransformConfigs.Select(config => new clsObjectAtt
               {
                   ID_Object = config.GUID,
                   ID_AttributeType = NameTransform.Config.LocalData.AttributeType_Style.GUID
               }).ToList();

               var dbReaderStyles = new OntologyModDBConnector(globals);
               if (searchStyles.Any())
               {
                   result.ResultState = dbReaderStyles.GetDataObjectAtt(searchStyles);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Styles!";
                       return result;
                   }

                   result.Result.Style = dbReaderStyles.ObjAtts;
               }

               var searchReportFilter = result.Result.NameTransformConfigs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = NameTransform.Config.LocalData.ClassRel_NameTransform_Provider__Reports__belonging_Report_Filter.ID_RelationType,
                   ID_Parent_Other = NameTransform.Config.LocalData.ClassRel_NameTransform_Provider__Reports__belonging_Report_Filter.ID_Class_Right
               }).ToList();

               var dbReaderReportFilter = new OntologyModDBConnector(globals);

               if (searchReportFilter.Any())
               {
                   result.ResultState = dbReaderReportFilter.GetDataObjectRel(searchReportFilter);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Report-Filters!";
                       return result;
                   }

                   result.Result.ConfigToReportFilters = dbReaderReportFilter.ObjectRels;
               }

               var searchReportSort = result.Result.NameTransformConfigs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = NameTransform.Config.LocalData.ClassRel_NameTransform_Provider__Reports__belonging_Report_Sort.ID_RelationType,
                   ID_Parent_Other = NameTransform.Config.LocalData.ClassRel_NameTransform_Provider__Reports__belonging_Report_Sort.ID_Class_Right
               }).ToList();

               var dbReaderReportSort = new OntologyModDBConnector(globals);
               if (searchReportSort.Any())
               {
                   result.ResultState = dbReaderReportSort.GetDataObjectRel(searchReportSort);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Report-Sorts!";
                       return result;
                   }

                   result.Result.ConfigToReportSorts = dbReaderReportSort.ObjectRels;
               }

               return result;
           });
            return taskResult;
        }

        public ServiceAgentElasticNameTransform(Globals globals) : base(globals)
        {
            
        }
    }
}
