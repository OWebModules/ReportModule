﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using ReportModule.Models;
using ReportModule.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Services
{
    public class ServiceAgentElastic : ElasticBaseAgent
    {

        public async Task<ResultItem<GetModelPDFReportResult>> GetModelPDFReport(CreatePDFReportRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetModelPDFReportResult>>(() =>
            {
                var result = new ResultItem<GetModelPDFReportResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new GetModelPDFReportResult()
                };

                if (string.IsNullOrEmpty(request.IdConfig))
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No valid IdConfig (empty)!";
                    return result;
                }

                if (!globals.is_GUID(request.IdConfig))
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No valid IdConfig (guid)!";
                    return result;
                }

                var dbReaderRootElement = new OntologyModDBConnector(globals);

                var searchRootElement = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig,
                        GUID_Parent = PDFExport.Config.LocalData.Class_PDFReport.GUID
                    }
                };

                result.ResultState = dbReaderRootElement.GetDataObjects(searchRootElement);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Root-Config!";
                    return result;
                }

                result.Result.RootConfig = dbReaderRootElement.Objects1.FirstOrDefault();

                if (result.Result.RootConfig == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Root-Config cannot be found!";
                    return result;
                }

                var searchSubConfigs = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.RootConfig.GUID,
                        ID_RelationType = PDFExport.Config.LocalData.ClassRel_PDFReport_contains_PDFReport.ID_RelationType,
                        ID_Parent_Other = PDFExport.Config.LocalData.ClassRel_PDFReport_contains_PDFReport.ID_Class_Right
                    }
                };

                var dbReaderSubConfigs = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderSubConfigs.GetDataObjectRel(searchSubConfigs);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Sub-Configs!";
                    return result;
                }

                result.Result.Configs = dbReaderSubConfigs.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).ToList();

                if (!result.Result.Configs.Any())
                {
                    result.Result.Configs.Add(result.Result.RootConfig);
                }

                var searchPaths = result.Result.Configs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = PDFExport.Config.LocalData.ClassRel_PDFReport_export_to_Path.ID_RelationType,
                    ID_Parent_Other = PDFExport.Config.LocalData.ClassRel_PDFReport_export_to_Path.ID_Class_Right
                }).ToList();


                var dbReaderPaths = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderPaths.GetDataObjectRel(searchPaths);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Paths!";
                    return result;
                }
                result.Result.ConfigsToPaths = dbReaderPaths.ObjectRels;


                if (!result.Result.ConfigsToPaths.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No paths found!";
                    return result;
                }

                var searchFieldFormats = result.Result.Configs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = PDFExport.Config.LocalData.ClassRel_PDFReport_contains_Field_Format__PDF_.ID_RelationType,
                    ID_Parent_Other = PDFExport.Config.LocalData.ClassRel_PDFReport_contains_Field_Format__PDF_.ID_Class_Right
                }).ToList();

                var dbReaderFieldFormats = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderFieldFormats.GetDataObjectRel(searchFieldFormats);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Field-Formats!";
                    return result;
                }

                result.Result.ConfigsToFieldFormat = dbReaderFieldFormats.ObjectRels;

                var searchFieldFormatIsHtml = result.Result.ConfigsToFieldFormat.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.ID_Other,
                    ID_AttributeType = PDFExport.Config.LocalData.AttributeType_IsHtml.GUID
                }).ToList();

                if (searchFieldFormatIsHtml.Any())
                {
                    var dbReaderIsHtml = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderIsHtml.GetDataObjectAtt(searchFieldFormatIsHtml);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the IsHtml-Flag of Field-Formats!";
                        return result;
                    }

                    result.Result.FieldFormatIsHtml = dbReaderIsHtml.ObjAtts;
                }

                var searchFieldFormatsToReportFields = result.Result.ConfigsToFieldFormat.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Other,
                    ID_RelationType = PDFExport.Config.LocalData.ClassRel_Field_Format__PDF__belongs_to_Report_Field.ID_RelationType,
                    ID_Parent_Other = PDFExport.Config.LocalData.ClassRel_Field_Format__PDF__belongs_to_Report_Field.ID_Class_Right
                }).ToList();

                if (searchFieldFormatsToReportFields.Any())
                {
                    var dbReaderFieldFormatToReportField = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderFieldFormatToReportField.GetDataObjectRel(searchFieldFormatsToReportFields);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Report-Fields of Field-Formats!";
                        return result;
                    }

                    result.Result.FieldFormatToReportFields = dbReaderFieldFormatToReportField.ObjectRels;
                }

                var searchReportFilters = result.Result.Configs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = PDFExport.Config.LocalData.ClassRel_PDFReport_belonging_Report_Filter.ID_RelationType,
                    ID_Parent_Other = PDFExport.Config.LocalData.ClassRel_PDFReport_belonging_Report_Filter.ID_Class_Right
                }).ToList();

                var dbReaderReportFilters = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderReportFilters.GetDataObjectRel(searchReportFilters);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Report-Filters!";
                    return result;
                }

                result.Result.ConfigsToFilter = dbReaderReportFilters.ObjectRels;

                var searchFilterValues = result.Result.ConfigsToFilter.Select(rel => new clsObjectAtt
                {
                    ID_Object = rel.ID_Other,
                    ID_AttributeType = PDFExport.Config.LocalData.AttributeType_Value.GUID
                }).ToList();

                if (searchFilterValues.Any())
                {
                    var dbReaderFilterValues = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderFilterValues.GetDataObjectAtt(searchFilterValues);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Report-Filter Values!";
                        return result;
                    }

                    result.Result.FilterValues = dbReaderFilterValues.ObjAtts;
                }

                var searchConfigsToReports = result.Result.Configs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = PDFExport.Config.LocalData.ClassRel_PDFReport_belonging_Reports.ID_RelationType,
                    ID_Parent_Other = PDFExport.Config.LocalData.ClassRel_PDFReport_belonging_Reports.ID_Class_Right
                }).ToList();

                var dbReaderConfigsToReports = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfigsToReports.GetDataObjectRel(searchConfigsToReports);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Reports!";
                    return result;
                }

                result.Result.ConfigsToReports = dbReaderConfigsToReports.ObjectRels;

                if (!result.Result.ConfigsToReports.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Reports found!";
                    return result;
                }

                var searchSorts = result.Result.Configs.Select(config => new clsObjectRel
                {
                    ID_Object = config.GUID,
                    ID_RelationType = PDFExport.Config.LocalData.ClassRel_PDFReport_belonging_Report_Sort.ID_RelationType,
                    ID_Parent_Other = PDFExport.Config.LocalData.ClassRel_PDFReport_belonging_Report_Sort.ID_Class_Right
                }).ToList();

                var dbReaderSorts = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderSorts.GetDataObjectRel(searchSorts);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Sorts!";
                    return result;
                }

                result.Result.ConfigsToSort = dbReaderSorts.ObjectRels;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ReportFilterItemModel>> GetReportFilterItemModel(GetFilterItemsRequest request, bool getFilterOItems)
        {
            var taskResult = await Task.Run<ResultItem<ReportFilterItemModel>>(() =>
           {
               var result = new ResultItem<ReportFilterItemModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new ReportFilterItemModel()
               };

               var dbReaderFilterItems = new OntologyModDBConnector(globals);

               if (request.RequestType == FilterRequestType.GetFiltersByFilterOItems)
               {
                   request.MessageOutput?.OutputInfo("Type: GetFiltersByFilterOItems");
                   if (getFilterOItems)
                   {
                       request.MessageOutput?.OutputInfo("Get the filteritem-objects...");
                       result.ResultState = dbReaderFilterItems.GetDataObjects(request.FilterOItems);

                       if (result.ResultState.GUID == globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Error while getting the filter-Items!";
                           request.MessageOutput?.OutputError(result.ResultState.Additional1);
                           return result;
                       }

                       result.Result.FilterItems = dbReaderFilterItems.Objects1;
                   }
                   else
                   {
                       result.Result.FilterItems = request.FilterOItems;
                   }

                   request.MessageOutput?.OutputInfo("Have filteritem-objects.");

                   request.MessageOutput?.OutputInfo("Get reports...");
                   var searchReports = result.Result.FilterItems.Select(filterItem => new clsObjectRel
                   {
                       ID_Object = filterItem.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_Report_Filter_belongs_to_Reports.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_Report_Filter_belongs_to_Reports.ID_Class_Right
                   }).ToList();

                   var dbReaderReports = new OntologyModDBConnector(globals);

                   if (searchReports.Any())
                   {
                       result.ResultState = dbReaderReports.GetDataObjectRel(searchReports);
                       if (result.ResultState.GUID == globals.LState_Error.GUID)
                       {
                           result.ResultState.Additional1 = "Error while getting the Values!";
                           request.MessageOutput?.OutputError(result.ResultState.Additional1);
                           return result;
                       }
                       result.Result.FilterItemsToReports = dbReaderReports.ObjectRels;
                       result.ResultState = ValidationController.ValidateReportFilterItemModel(result.Result, globals, nameof(ReportFilterItemModel.FilterItemsToReports));
                       if (result.ResultState.GUID == globals.LState_Error.GUID)
                       {
                           request.MessageOutput?.OutputError(result.ResultState.Additional1);
                           return result;
                       }
                   }

                   request.MessageOutput?.OutputInfo("Have reports.");

               }
               else if (request.RequestType == FilterRequestType.GetFiltersByReport)
               {
                   request.MessageOutput?.OutputInfo("Type: GetFiltersByReport");
                   var searchReports = new List<clsOntologyItem>
                   {
                       new clsOntologyItem
                       {
                           GUID = request.ReportOItem.GUID
                       }
                   };

                   var dbReaderReports = new OntologyModDBConnector(globals);

                   request.MessageOutput?.OutputInfo("Get report...");
                   result.ResultState = dbReaderReports.GetDataObjects(searchReports);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Report!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   var reportOItem = dbReaderReports.Objects1.FirstOrDefault();

                   if (reportOItem == null)
                   {
                       result.ResultState = globals.LState_Error.Clone();
                       result.ResultState.Additional1 = "The Report cannot be found!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                            
                   }

                   request.MessageOutput?.OutputInfo("Have report.");

                   var searchReportsFields = new List<clsObjectRel>
                   {
                       new clsObjectRel
                       {
                           ID_Other = reportOItem.GUID,
                           ID_RelationType = Config.LocalData.ClassRel_Report_Filter_belongs_to_Reports.ID_RelationType,
                           ID_Parent_Object = Config.LocalData.ClassRel_Report_Filter_belongs_to_Reports.ID_Class_Left
                       }
                   };

                   var dbReaderReportFields = new OntologyModDBConnector(globals);

                   request.MessageOutput?.OutputInfo("Get the filteritem-objects...");
                   result.ResultState = dbReaderReportFields.GetDataObjectRel(searchReportsFields);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the filter-Items!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   result.Result.FilterItemsToReports = dbReaderReportFields.ObjectRels;

                   result.Result.FilterItems = result.Result.FilterItemsToReports.Select(filtToRep => new clsOntologyItem
                   {
                       GUID = filtToRep.ID_Object,
                       Name = filtToRep.Name_Object,
                       GUID_Parent = filtToRep.ID_Parent_Object,
                       Type = globals.Type_Object
                   }).ToList();

                   request.MessageOutput?.OutputInfo("Have the filteritem-objects.");

               }

               request.MessageOutput?.OutputInfo("Get filter-values...");
               var searchValues = result.Result.FilterItems.Select(filterItem => new clsObjectAtt
               {
                   ID_Object = filterItem.GUID,
                   ID_AttributeType = Config.LocalData.AttributeType_Value.GUID
               }).ToList();

               var dbReaderValues = new OntologyModDBConnector(globals);

               if (searchValues.Any())
               {
                   result.ResultState = dbReaderValues.GetDataObjectAtt(searchValues);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Values!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }
                   result.Result.FilterItemValues = dbReaderValues.ObjAtts;

                   result.ResultState = ValidationController.ValidateReportFilterItemModel(result.Result, globals, nameof(ReportFilterItemModel.FilterItemValues));
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }
               }

               request.MessageOutput?.OutputInfo("Have filter-values...");

               request.MessageOutput?.OutputInfo("Get filter-values...");
               var searchStandard = result.Result.FilterItems.Select(filterItem => new clsObjectAtt
               {
                   ID_Object = filterItem.GUID,
                   ID_AttributeType = Config.LocalData.AttributeType_Standard.GUID
               }).ToList();

               var dbReaderStandard = new OntologyModDBConnector(globals);

               if (searchStandard.Any())
               {
                   result.ResultState = dbReaderStandard.GetDataObjectAtt(searchStandard);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Standard-Flags!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }
                   result.Result.FilterItemStandard = dbReaderStandard.ObjAtts;

                   result.ResultState = ValidationController.ValidateReportFilterItemModel(result.Result, globals, nameof(ReportFilterItemModel.FilterItemStandard));
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }
               }

               request.MessageOutput?.OutputInfo("Have filter-values.");

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<VariableToFieldModel>> GetVariableToFieldModel(GetVariablesToFieldsRequest request)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<VariableToFieldModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new VariableToFieldModel()
                };

                result.Result.VariableToFieldMaps = request.VariablesToFieldsOItems;
                var dbReaderVariableToFieldMaps = new OntologyModDBConnector(globals);
                if (result.Result.VariableToFieldMaps.Any(varFieldMap => string.IsNullOrEmpty(varFieldMap.Name) ||
                    string.IsNullOrEmpty(varFieldMap.GUID_Parent) ||
                    string.IsNullOrEmpty(varFieldMap.Type)))
                {
                    result.ResultState = dbReaderVariableToFieldMaps.GetDataObjects(result.Result.VariableToFieldMaps);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Variable-Field-Maps!";
                        return result;
                    }

                    result.Result.VariableToFieldMaps = dbReaderVariableToFieldMaps.Objects1;
                    result.ResultState = ValidationController.ValidateVariablesToFieldsModels(result.Result, globals, nameof(VariableToFieldModel.VariableToFieldMaps));
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                var searchVariables = result.Result.VariableToFieldMaps.Select(varFieldMap => new clsObjectRel
                {
                    ID_Object = varFieldMap.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Variable_To_Field_belongs_to_Variable.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Variable_To_Field_belongs_to_Variable.ID_Class_Right
                }).ToList();

                var dbReaderVariables = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderVariables.GetDataObjectRel(searchVariables);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Varialbes!";
                    return result;
                }

                result.Result.MapsToVariables = dbReaderVariables.ObjectRels;

                result.ResultState = ValidationController.ValidateVariablesToFieldsModels(result.Result, globals, nameof(VariableToFieldModel.MapsToVariables));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchReportFields = result.Result.VariableToFieldMaps.Select(varFieldMap => new clsObjectRel
                {
                    ID_Object = varFieldMap.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Variable_To_Field_belongs_to_Report_Field.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Variable_To_Field_belongs_to_Report_Field.ID_Class_Right
                }).ToList();

                var dbReaderReportFields = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderReportFields.GetDataObjectRel(searchReportFields);

                result.Result.MapsToFields = dbReaderReportFields.ObjectRels;

                result.ResultState = ValidationController.ValidateVariablesToFieldsModels(result.Result, globals, nameof(VariableToFieldModel.MapsToFields));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ReportSortItemModel>> GetReportSortItemModel(GetSortItemsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ReportSortItemModel>>(() =>
            {
                var result = new ResultItem<ReportSortItemModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ReportSortItemModel()
                };

                var dbReaderSortItems = new OntologyModDBConnector(globals);

                if (request.RequestSortOItems)
                {
                    result.ResultState = dbReaderSortItems.GetDataObjects(request.SortOItems);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Report-Items!";
                        return result;
                    }

                    result.Result.SortItems = dbReaderSortItems.Objects1;
                }
                else
                {
                    result.Result.SortItems = request.SortOItems;
                }

                var searchValues = result.Result.SortItems.Select(SortItem => new clsObjectAtt
                {
                    ID_Object = SortItem.GUID,
                    ID_AttributeType = Config.LocalData.AttributeType_Value.GUID
                }).ToList();

                var dbReaderValues = new OntologyModDBConnector(globals);

                if (searchValues.Any())
                {
                    result.ResultState = dbReaderValues.GetDataObjectAtt(searchValues);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Values!";
                        return result;
                    }
                    result.Result.SortItemValues = dbReaderValues.ObjAtts;

                    result.ResultState = ValidationController.ValidateReportSortItemModel(result.Result, globals, nameof(ReportSortItemModel.SortItemValues));
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                var searchReports = result.Result.SortItems.Select(SortItem => new clsObjectRel
                {
                    ID_Object = SortItem.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Report_Sort_belongs_to_Reports.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Report_Sort_belongs_to_Reports.ID_Class_Right
                }).ToList();

                var dbReaderReports = new OntologyModDBConnector(globals);

                if (searchReports.Any())
                {
                    result.ResultState = dbReaderReports.GetDataObjectRel(searchReports);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Values!";
                        return result;
                    }
                    result.Result.SortItemsToReports = dbReaderReports.ObjectRels;
                    result.ResultState = ValidationController.ValidateReportSortItemModel(result.Result, globals, nameof(ReportSortItemModel.SortItemsToReports));
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<clsOntologyItem>> GetOItem(string id, string type)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var dbReader = new OntologyModDBConnector(globals);
                var resultItem = dbReader.GetOItem(id, type);

                if (resultItem.GUID_Related == globals.LState_Error.GUID)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    return result;
                };

                result.Result = resultItem;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetOItems(List<string> ids, string type)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                var dbReader = new OntologyModDBConnector(globals);

                if (type == globals.Type_AttributeType)
                {
                    result.ResultState = dbReader.GetDataAttributeType(ids.Select(id => new clsOntologyItem { GUID = id }).ToList());
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting AttributeTypes!";
                        return result;
                    }
                    result.Result = dbReader.AttributeTypes;
                }
                else if (type == globals.Type_RelationType)
                {
                    result.ResultState = dbReader.GetDataRelationTypes(ids.Select(id => new clsOntologyItem { GUID = id }).ToList());
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting RelationTypes!";
                        return result;
                    }
                    result.Result = dbReader.RelationTypes;
                }
                else if (type == globals.Type_Class)
                {
                    result.ResultState = dbReader.GetDataClasses(ids.Select(id => new clsOntologyItem { GUID = id }).ToList());
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting Classes!";
                        return result;
                    }
                    result.Result = dbReader.Classes1;
                }
                else if (type == globals.Type_Object)
                {
                    result.ResultState = dbReader.GetDataObjects(ids.Select(id => new clsOntologyItem { GUID = id }).ToList());
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting Objects!";
                        return result;
                    }
                    result.Result = dbReader.Objects1;
                }
                else
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Provided Type is not valid!";
                    return result;
                }


                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<MoveObjectsByReportModel>> GetMoveObjectsByReportModel(MoveObjectsByReportRequest request)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<MoveObjectsByReportModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new MoveObjectsByReportModel()
                };

                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig
                    }
                };

                var dbReaderConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Config!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMoveObjectsByReportModel(
                    dbReaderConfig, 
                    result.Result, 
                    globals, 
                    nameof(MoveObjectsByReportModel.Config));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchReport = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = MoveObjects.Config.LocalData.ClassRel_Move_Objects_By_Reports_uses_Reports.ID_RelationType,
                        ID_Parent_Other = MoveObjects.Config.LocalData.ClassRel_Move_Objects_By_Reports_uses_Reports.ID_Class_Right
                    }
                };

                var dbReaderReport = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderReport.GetDataObjectRel(searchReport);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Report!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMoveObjectsByReportModel(
                    dbReaderReport, 
                    result.Result, 
                    globals, 
                    nameof(MoveObjectsByReportModel.Report));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchReportFields = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = MoveObjects.Config.LocalData.ClassRel_Move_Objects_By_Reports_identified_by_Report_Field.ID_RelationType,
                        ID_Parent_Other = MoveObjects.Config.LocalData.ClassRel_Move_Objects_By_Reports_identified_by_Report_Field.ID_Class_Right
                    }
                };

                var dbReaderReportFields = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderReportFields.GetDataObjectRel(searchReportFields);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Report-Field!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMoveObjectsByReportModel(
                    dbReaderReportFields,
                    result.Result,
                    globals,
                    nameof(MoveObjectsByReportModel.ReportFieldId));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchFilter = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = MoveObjects.Config.LocalData.ClassRel_Move_Objects_By_Reports_uses_Report_Filter.ID_RelationType,
                        ID_Parent_Other = MoveObjects.Config.LocalData.ClassRel_Move_Objects_By_Reports_uses_Report_Filter.ID_Class_Right
                    }
                };

                var dbFilter = new OntologyModDBConnector(globals);

                result.ResultState = dbFilter.GetDataObjectRel(searchFilter);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Report-Filter!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMoveObjectsByReportModel(
                    dbFilter,
                    result.Result,
                    globals,
                    nameof(MoveObjectsByReportModel.ReportFilter));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchClass = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = MoveObjects.Config.LocalData.ClassRel_Move_Objects_By_Reports_archive_class.ID_RelationType
                    }
                };

                var dbReaderClass = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderClass.GetDataObjectRel(searchClass);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the destination-class!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetMoveObjectsByReportModel(
                    dbReaderClass,
                    result.Result,
                    globals,
                    nameof(MoveObjectsByReportModel.DestinationClass));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<clsObjectRel>>> GetMSSQLDataMaps()
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsObjectRel>()
                };

                var searchMap = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Object = Config.LocalData.ClassRel_Field_Type_is_of_Type_DataTypes__Ms_SQL_.ID_Class_Left,
                        ID_RelationType = Config.LocalData.ClassRel_Field_Type_is_of_Type_DataTypes__Ms_SQL_.ID_RelationType,
                        ID_Parent_Other = Config.LocalData.ClassRel_Field_Type_is_of_Type_DataTypes__Ms_SQL_.ID_Class_Right
                    }
                };

                var dbReaderMap = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderMap.GetDataObjectRel(searchMap);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Datatype-Map!";
                    return result;
                }

                result.Result = dbReaderMap.ObjectRels;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<ObjectsFromReportModel>> GetObjectsFromReportModel(GetObjectsFormReportRequest request)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<ObjectsFromReportModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new ObjectsFromReportModel()
                };

                var searchConfig = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdConfig
                    }
                };

                var dbReaderConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderConfig.GetDataObjects(searchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Config!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetObjectsFromReportModel(dbReaderConfig, result.Result, globals, nameof(ObjectsFromReportModel.Config));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchReport = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = GetObjectsFormReport.Config.LocalData.ClassRel_Get_Objects_From_Report_belonging_Reports.ID_RelationType,
                        ID_Parent_Other = GetObjectsFormReport.Config.LocalData.ClassRel_Get_Objects_From_Report_belonging_Reports.ID_Class_Right
                    }
                };

                var dbReaderReport = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderReport.GetDataObjectRel(searchReport);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Report!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetObjectsFromReportModel(dbReaderReport, result.Result, globals, nameof(ObjectsFromReportModel.Report));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchReportFields = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = GetObjectsFormReport.Config.LocalData.ClassRel_Get_Objects_From_Report_belonging_Report_Field.ID_RelationType,
                        ID_Parent_Other = GetObjectsFormReport.Config.LocalData.ClassRel_Get_Objects_From_Report_belonging_Report_Field.ID_Class_Right
                    }
                };

                var dbReaderReportFields = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderReportFields.GetDataObjectRel(searchReportFields);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Report-Fields!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetObjectsFromReportModel(dbReaderReportFields, result.Result, globals, nameof(ObjectsFromReportModel.ReportFields));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }


                var searchReportFilter= new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = result.Result.Config.GUID,
                        ID_RelationType = GetObjectsFormReport.Config.LocalData.ClassRel_Get_Objects_From_Report_belonging_Report_Filter.ID_RelationType,
                        ID_Parent_Other = GetObjectsFormReport.Config.LocalData.ClassRel_Get_Objects_From_Report_belonging_Report_Filter.ID_Class_Right
                    }
                };

                var dbReaderReportFilter = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderReportFilter.GetDataObjectRel(searchReportFilter);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Report-Filter!";
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetObjectsFromReportModel(dbReaderReportFilter, result.Result, globals, nameof(ObjectsFromReportModel.ReportFilter));

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public ServiceAgentElastic(Globals globals) : base(globals)
        {
        }
    }
}
