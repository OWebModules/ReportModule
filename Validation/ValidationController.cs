﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using ReportModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateNameTransformProviderModel(NameTransformProviderModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (propertyName == null || propertyName == nameof(NameTransformProviderModel.ConfigToReports))
            {
                if (model.NameTransformConfigs.Count != model.ConfigToReports.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Report is related with the provider-config!";
                    return result;
                }
            }

            return result;
        }

        public static clsOntologyItem ValidateGetFilterItemsRequest(GetFilterItemsRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();


            if ( (request.RequestType == FilterRequestType.GetFiltersByFilterOItems && !request.FilterOItems.Any()) ||
                 (request.RequestType == FilterRequestType.GetFiltersByReport && request.ReportOItem == null))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The provided type is not valid! You have to provide FilterOItems if you the request is of type GetFiltersByFilterOItems or you have to provide an ReportOItem.";
                return result;
            }

            var requestFilterItems = false;
            foreach (var item in request.FilterOItems)
            {
                if (string.IsNullOrEmpty(item.GUID))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"At least one item has no valid GUID!";
                    return result;
                }

                if (string.IsNullOrEmpty(item.Name))
                {
                    requestFilterItems = true;
                }

                if (string.IsNullOrEmpty(item.GUID_Parent) || item.GUID_Parent != Config.LocalData.Class_Report_Filter.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The item with the GUID {item.GUID} has a wrong Class-GUID!";
                    return result;
                }
            }

            result.Mark = requestFilterItems;

            return result;
        }

        public static clsOntologyItem ValidateReportFilterItemModel(ReportFilterItemModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (propertyName == null || propertyName == nameof(ReportFilterItemModel.FilterItemValues))
            {
                
                if (model.FilterItems.Count != model.FilterItemValues.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"{model.FilterItems.Count} Filter-Items and {model.FilterItemValues.Count} Values!";
                    return result;
                }
            }

            if (propertyName == null || propertyName == nameof(ReportFilterItemModel.FilterItemsToReports))
            {
                if (model.FilterItems.Count != model.FilterItemsToReports.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"{model.FilterItems.Count} Filter-Items and {model.FilterItemsToReports.Count} report-relations!";
                    return result;
                }
            }

            if (propertyName == null || propertyName == nameof(ReportFilterItemModel.FilterItemStandard))
            {

                if (model.FilterItems.Count < model.FilterItemStandard.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"{model.FilterItems.Count} Filter-Items and {model.FilterItemStandard.Count} Values! It's only max one standard-flag per filter-item allowed!";
                    return result;
                }
            }

            return result;
        }

        public static clsOntologyItem ValidateGetSortItemsRequest(GetSortItemsRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            var requestSortItems = false;
            foreach (var item in request.SortOItems)
            {
                if (string.IsNullOrEmpty(item.GUID))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"At least one item has no valid GUID!";
                    return result;
                }

                if (string.IsNullOrEmpty(item.Name))
                {
                    requestSortItems = true;
                }

                if (string.IsNullOrEmpty(item.GUID_Parent) || item.GUID_Parent != Config.LocalData.Class_Report_Sort.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"The item with the GUID {item.GUID} has a wrong Class-GUID!";
                    return result;
                }
            }

            request.RequestSortOItems = requestSortItems;

            return result;
        }

        public static clsOntologyItem ValidateReportSortItemModel(ReportSortItemModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (propertyName == null || propertyName == nameof(ReportSortItemModel.SortItemValues))
            {
                if (model.SortItems.Count != model.SortItemValues.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"{model.SortItems.Count} Sort-Items and {model.SortItemValues.Count} Values!";
                    return result;
                }
            }

            if (propertyName == null || propertyName == nameof(ReportSortItemModel.SortItemsToReports))
            {
                if (model.SortItems.Count != model.SortItemsToReports.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"{model.SortItems.Count} Sort-Items and {model.SortItemsToReports.Count} report-relations!";
                    return result;
                }
            }

            return result;
        }

        public static clsOntologyItem ValidateGetRowDiffRequest(GetRowDiffRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (request.ReportItem == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = Primitives.LogMessages.ValidateGetRowDiffRequest_NoReport;
                return result;
            }

            if (!request.ReportFields.Any())
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = Primitives.LogMessages.ValidateGetRowDiffRequest_NoReportFields;
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateGetRowDiffResult(GetRowDiffResult model, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (model.Field1 == null || model.Field2 == null)
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = Primitives.LogMessages.ValidateGetRowDiffResult_EmptyField;
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateGetVariablesToFieldsRequest(GetVariablesToFieldsRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (request.VariablesToFieldsOItems.Any(varFieldMap => string.IsNullOrEmpty( varFieldMap.GUID) || !globals.is_GUID(varFieldMap.GUID)))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "The provided list of maps is invalid. At least one GUID of a map is not a valid guid!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateVariablesToFieldsModels(VariableToFieldModel model, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            //if (propertyName == null || propertyName == nameof(VariableToFieldModel.VariableToFieldMaps))
            //{

            //}

            if (propertyName == null || propertyName == nameof(VariableToFieldModel.MapsToVariables))
            {
                if (model.MapsToVariables.Count != model.VariableToFieldMaps.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You need per map one variable!";
                    return result;
                }
            }

            if (propertyName == null || propertyName == nameof(VariableToFieldModel.MapsToFields))
            {
                if (model.MapsToFields.Count != model.VariableToFieldMaps.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You need per map one report-field!";
                    return result;
                }
            }

            return result;
        }

        public static clsOntologyItem ValidateMoveObjectsByReportRequest(MoveObjectsByReportRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty( request.IdConfig) )
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"{nameof(MoveObjectsByReportRequest.IdConfig) } is Empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = $"{nameof(MoveObjectsByReportRequest.IdConfig) } is no valid Id!";
                return result;
            }

            

            return result;
        }

        public static clsOntologyItem ValidateAndSetMoveObjectsByReportModel(OntologyModDBConnector dbReader, MoveObjectsByReportModel model, Globals globals, string propertyName)
        {
            var result = globals.LState_Success.Clone();

            if (propertyName == nameof(MoveObjectsByReportModel.Config))
            {
                if (dbReader.Objects1.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "More than one Config is Found!";
                    return result;
                }

                model.Config = dbReader.Objects1.First();

                if (model.Config.GUID_Parent != MoveObjects.Config.LocalData.Class_Move_Objects_By_Reports.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"Provied config is not of class {MoveObjects.Config.LocalData.Class_Move_Objects_By_Reports.Name}";
                    return result;
                }
            }
            else if (propertyName == nameof(MoveObjectsByReportModel.Report))
            {
                if (dbReader.ObjectRels.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Exact one Report must be found!";
                    return result;
                }

                model.Report = dbReader.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).First();
            }
            else if (propertyName == nameof(MoveObjectsByReportModel.ReportFieldId))
            {
                if (dbReader.ObjectRels.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "More than one Report-Field is Found!";
                    return result;
                }

                model.ReportFieldId = dbReader.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).First();

            }
            else if (propertyName == nameof(MoveObjectsByReportModel.ReportFilter))
            {
                if (dbReader.ObjectRels.Count > 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Max one Report-Filter must be defined!";
                    return result;
                }

                model.ReportFilter = dbReader.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).FirstOrDefault();
            }
            else if (propertyName == nameof(MoveObjectsByReportModel.DestinationClass))
            {
                if (dbReader.ObjectRels.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "Exact one Destination-Class must be defined!";
                    return result;
                }

                model.DestinationClass = dbReader.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).First();

                if (model.DestinationClass.Type != globals.Type_Class)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "You have no class related!";
                    return result;
                }
            }
            return result;
        }

        public static clsOntologyItem ValidateGetObjectsFromReportRequest(GetObjectsFormReportRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty( request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is invlid!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateAndSetObjectsFromReportModel(OntologyModDBConnector dbReader, ObjectsFromReportModel model, Globals globals, string propertyName)
        {
            var result = globals.LState_Success.Clone();
            if (propertyName == nameof(ObjectsFromReportModel.Config))
            {
                if (dbReader.Objects1.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one config, but there are {dbReader.Objects1.Count} Items!";
                    return result;
                }

                model.Config = dbReader.Objects1.First();

                if (model.Config.GUID_Parent != GetObjectsFormReport.Config.LocalData.Class_Get_Objects_From_Report.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"Your config is of invalid class!";
                    return result;
                }
            }

            if (propertyName == nameof(ObjectsFromReportModel.Report))
            {
                if (dbReader.ObjectRels.Count != 1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need exact one Report, but there are {dbReader.ObjectRels.Count} Items!";
                    return result;
                }

                model.Report = dbReader.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).First();

                if (model.Report.GUID_Parent != GetObjectsFormReport.Config.LocalData.ClassRel_Get_Objects_From_Report_belonging_Reports.ID_Class_Right)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"Your Report is of invalid class!";
                    return result;
                }
            }

            if (propertyName == nameof(ObjectsFromReportModel.ReportFilter))
            {
                if (dbReader.ObjectRels.Count >1)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need none or one Report-Filter, but there are {dbReader.ObjectRels.Count} Items!";
                    return result;
                }

                model.ReportFilter = dbReader.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).FirstOrDefault();

                if (model.ReportFilter != null && model.ReportFilter.GUID_Parent != GetObjectsFormReport.Config.LocalData.ClassRel_Get_Objects_From_Report_belonging_Report_Filter.ID_Class_Right)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"Your Report-Filter is of invalid class!";
                    return result;
                }
            }

            if (propertyName == nameof(ObjectsFromReportModel.ReportFields))
            {
                if (!dbReader.ObjectRels.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You need none or one Report-Filter, but there are {dbReader.ObjectRels.Count} Items!";
                    return result;
                }

                model.ReportFields = dbReader.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).ToList();

                var invalidReportFields = model.ReportFields.Where(rf => rf.GUID_Parent != GetObjectsFormReport.Config.LocalData.ClassRel_Get_Objects_From_Report_belonging_Report_Field.ID_Class_Right).Select(invField => invField.Name).ToList();
                if (invalidReportFields.Any())
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"{string.Join(", ", invalidReportFields)} are no Report-Fields!";
                    return result;
                }
            }

            return result;
        }
    }
}
