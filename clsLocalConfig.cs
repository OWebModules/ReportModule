﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OntologyAppDBConnector;
using ImportExport_Module;
using OntologyClasses.BaseClasses;
using System.Reflection;
using OntologyClasses.Interfaces;
using System.Runtime.InteropServices;

namespace ReportModule
{
    public class clsLocalConfig : ILocalConfig
    {
        private const string cstrID_Ontology = "ca8bccf7e9974a929604d283a55636bf";
        private XMLImportWorker objImport;

        public Globals Globals { get; set; }

        private clsOntologyItem objOItem_DevConfig = new clsOntologyItem();
        public clsOntologyItem OItem_BaseConfig { get; set; }

        private OntologyModDBConnector objDBLevel_Config1;
        private OntologyModDBConnector objDBLevel_Config2;

        public clsOntologyItem OItem_attribute_asc { get; set; }
        public clsOntologyItem OItem_attribute_dbpostfix { get; set; }
        public clsOntologyItem OItem_attribute_invisible { get; set; }
        public clsOntologyItem OItem_attribute_is_null { get; set; }
        public clsOntologyItem OItem_attribute_row_header { get; set; }
        public clsOntologyItem OItem_attribute_standard { get; set; }
        public clsOntologyItem OItem_attribute_value { get; set; }
        public clsOntologyItem OItem_attribute_visible { get; set; }
        public clsOntologyItem OItem_attribute_xml_text { get; set; }
        public clsOntologyItem OItem_attributetype_nextline { get; set; }
        public clsOntologyItem OItem_class_clipboardfilter { get; set; }
        public clsOntologyItem OItem_class_clipboardfilter_tags { get; set; }
        public clsOntologyItem OItem_class_datatypes { get; set; }
        public clsOntologyItem OItem_class_field { get; set; }
        public clsOntologyItem OItem_class_types__elastic_search_ { get; set; }
        public clsOntologyItem OItem_object_baseconfig { get; set; }
        public clsOntologyItem OItem_object_bit { get; set; }
        public clsOntologyItem OItem_object_commandlinerun_module { get; set; }
        public clsOntologyItem OItem_object_datetime { get; set; }
        public clsOntologyItem OItem_object_double { get; set; }
        public clsOntologyItem OItem_object_int { get; set; }
        public clsOntologyItem OItem_object_report_connection { get; set; }
        public clsOntologyItem OItem_object_string { get; set; }
        public clsOntologyItem OItem_relationtype_belonging { get; set; }
        public clsOntologyItem OItem_relationtype_belonging_resources { get; set; }
        public clsOntologyItem OItem_relationtype_belonging_source { get; set; }
        public clsOntologyItem OItem_relationtype_belongsto { get; set; }
        public clsOntologyItem OItem_relationtype_bold_tags { get; set; }
        public clsOntologyItem OItem_relationtype_cell_config { get; set; }
        public clsOntologyItem OItem_relationtype_cell_tags { get; set; }
        public clsOntologyItem OItem_relationtype_connected_by { get; set; }
        public clsOntologyItem OItem_relationtype_contains { get; set; }
        public clsOntologyItem OItem_relationtype_formatted_by { get; set; }
        public clsOntologyItem OItem_relationtype_header_tags { get; set; }
        public clsOntologyItem OItem_relationtype_is { get; set; }
        public clsOntologyItem OItem_relationtype_is_of_type { get; set; }
        public clsOntologyItem OItem_relationtype_leads { get; set; }
        public clsOntologyItem OItem_relationtype_located_in { get; set; }
        public clsOntologyItem OItem_relationtype_row_config { get; set; }
        public clsOntologyItem OItem_relationtype_row_tags { get; set; }
        public clsOntologyItem OItem_relationtype_table_config { get; set; }
        public clsOntologyItem OItem_relationtype_table_tags { get; set; }
        public clsOntologyItem OItem_relationtype_type_field { get; set; }
        public clsOntologyItem OItem_relationtype_value_type { get; set; }
        public clsOntologyItem OItem_token_field_type_datetime { get; set; }
        public clsOntologyItem OItem_token_field_type_guid { get; set; }
        public clsOntologyItem OItem_token_field_type_text { get; set; }
        public clsOntologyItem OItem_token_field_type_zahl { get; set; }
        public clsOntologyItem OItem_token_ontology_relation_rule_child_token { get; set; }
        public clsOntologyItem OItem_token_ontology_relation_rule_inner_join { get; set; }
        public clsOntologyItem OItem_token_ontology_relation_rule_left_outer_join { get; set; }
        public clsOntologyItem OItem_token_ontology_relation_rule_name_of_type_parse { get; set; }
        public clsOntologyItem OItem_token_ontology_relation_rule_only_item { get; set; }
        public clsOntologyItem OItem_token_ontology_relation_rule_parent_types { get; set; }
        public clsOntologyItem OItem_token_ontology_relation_rule_relation_break { get; set; }
        public clsOntologyItem OItem_token_ontology_relation_rule_right_outer_join { get; set; }
        public clsOntologyItem OItem_token_report_type_elasticview { get; set; }
        public clsOntologyItem OItem_token_report_type_token_report { get; set; }
        public clsOntologyItem OItem_token_report_type_view { get; set; }
        public clsOntologyItem OItem_token_variable_author { get; set; }
        public clsOntologyItem OItem_token_variable_cell_list { get; set; }
        public clsOntologyItem OItem_token_variable_cell_name { get; set; }
        public clsOntologyItem OItem_token_variable_cell_value { get; set; }
        public clsOntologyItem OItem_token_variable_colcount { get; set; }
        public clsOntologyItem OItem_token_variable_datetime_tz { get; set; }
        public clsOntologyItem OItem_token_variable_id { get; set; }
        public clsOntologyItem OItem_token_variable_report { get; set; }
        public clsOntologyItem OItem_token_variable_report_20 { get; set; }
        public clsOntologyItem OItem_token_variable_row_list { get; set; }
        public clsOntologyItem OItem_token_variable_row_name { get; set; }
        public clsOntologyItem OItem_token_variable_rowcount { get; set; }
        public clsOntologyItem OItem_class_aggregate_type { get; set; }
        public clsOntologyItem OItem_class_aggregate { get; set; }
        public clsOntologyItem OItem_type_comparison_operators { get; set; }
        public clsOntologyItem OItem_type_database { get; set; }
        public clsOntologyItem OItem_type_database_on_server { get; set; }
        public clsOntologyItem OItem_type_datatypes__ms_sql_ { get; set; }
        public clsOntologyItem OItem_type_db_columns { get; set; }
        public clsOntologyItem OItem_type_db_procedure { get; set; }
        public clsOntologyItem OItem_type_db_views { get; set; }
        public clsOntologyItem OItem_type_field_format { get; set; }
        public clsOntologyItem OItem_type_field_type { get; set; }
        public clsOntologyItem OItem_type_file { get; set; }
        public clsOntologyItem OItem_type_indexes__elastic_search_ { get; set; }
        public clsOntologyItem OItem_type_logical_operators { get; set; }
        public clsOntologyItem OItem_type_ontology_item { get; set; }
        public clsOntologyItem OItem_type_ontology_join { get; set; }
        public clsOntologyItem OItem_type_password { get; set; }
        public clsOntologyItem OItem_type_path { get; set; }
        public clsOntologyItem OItem_type_port { get; set; }
        public clsOntologyItem OItem_type_report_field { get; set; }
        public clsOntologyItem OItem_type_report_filter { get; set; }
        public clsOntologyItem OItem_type_report_sort { get; set; }
        public clsOntologyItem OItem_type_report_type { get; set; }
        public clsOntologyItem OItem_type_reports { get; set; }
        public clsOntologyItem OItem_type_server { get; set; }
        public clsOntologyItem OItem_type_server_port { get; set; }
        public clsOntologyItem OItem_type_url { get; set; }
        public clsOntologyItem OItem_type_user { get; set; }
        public clsOntologyItem OItem_type_variable { get; set; }
        public clsOntologyItem OItem_type_xml { get; set; }
        public clsOntologyItem OItem_type_xml_config { get; set; }



        private void get_Data_DevelopmentConfig()
        {
            var objORL_Ontology_To_OntolgyItems = new List<clsObjectRel> {new clsObjectRel {ID_Object = cstrID_Ontology,
                                                                                             ID_RelationType = Globals.RelationType_contains.GUID,
                                                                                             ID_Parent_Other = Globals.Class_OntologyItems.GUID}};

            var objOItem_Result = objDBLevel_Config1.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
            if (objOItem_Result.GUID == Globals.LState_Success.GUID)
            {
                if (objDBLevel_Config1.ObjectRels.Any())
                {

                    objORL_Ontology_To_OntolgyItems = objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingAttribute.GUID
                    }).ToList();

                    objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingClass.GUID
                    }));
                    objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingObject.GUID
                    }));
                    objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingRelationType.GUID
                    }));

                    objOItem_Result = objDBLevel_Config2.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
                    if (objOItem_Result.GUID == Globals.LState_Success.GUID)
                    {
                        if (!objDBLevel_Config2.ObjectRels.Any())
                        {
                            throw new Exception("Config-Error");
                        }
                    }
                    else
                    {
                        throw new Exception("Config-Error");
                    }
                }
                else
                {
                    throw new Exception("Config-Error");
                }

            }

        }

        public clsLocalConfig()
        {
            Globals = new Globals();
            set_DBConnection();
            get_Config();
        }

        public clsLocalConfig(Globals Globals)
        {
            this.Globals = Globals;
            set_DBConnection();
            get_Config();
        }

        private void set_DBConnection()
        {
            objDBLevel_Config1 = new OntologyModDBConnector(Globals);
            objDBLevel_Config2 = new OntologyModDBConnector(Globals);
            objImport = new XMLImportWorker(Globals);
        }

        private void get_Config()
        {
            try
            {
                get_Data_DevelopmentConfig();
                get_Config_AttributeTypes();
                get_Config_RelationTypes();
                get_Config_Classes();
                get_Config_Objects();
            }
            catch (Exception ex)
            {
                var objAssembly = Assembly.GetExecutingAssembly();
                AssemblyTitleAttribute[] objCustomAttributes = (AssemblyTitleAttribute[])objAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                var strTitle = "Unbekannt";
                if (objCustomAttributes.Length == 1)
                {
                    strTitle = objCustomAttributes.First().Title;
                }
                var objOItem_Result = objImport.ImportTemplates(objAssembly);
                if (objOItem_Result.GUID != Globals.LState_Error.GUID)
                {
                    get_Data_DevelopmentConfig();
                    get_Config_AttributeTypes();
                    get_Config_RelationTypes();
                    get_Config_Classes();
                    get_Config_Objects();
                }
                else
                {
                    throw new Exception("Config not importable");
                }
                
            }
        }

        private void get_Config_AttributeTypes()
        {
            var objOList_attribute_asc = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "attribute_asc".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                          select objRef).ToList();

            if (objOList_attribute_asc.Any())
            {
                OItem_attribute_asc = new clsOntologyItem()
                {
                    GUID = objOList_attribute_asc.First().ID_Other,
                    Name = objOList_attribute_asc.First().Name_Other,
                    GUID_Parent = objOList_attribute_asc.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_dbpostfix = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "attribute_dbpostfix".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                select objRef).ToList();

            if (objOList_attribute_dbpostfix.Any())
            {
                OItem_attribute_dbpostfix = new clsOntologyItem()
                {
                    GUID = objOList_attribute_dbpostfix.First().ID_Other,
                    Name = objOList_attribute_dbpostfix.First().Name_Other,
                    GUID_Parent = objOList_attribute_dbpostfix.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_invisible = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "attribute_invisible".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                select objRef).ToList();

            if (objOList_attribute_invisible.Any())
            {
                OItem_attribute_invisible = new clsOntologyItem()
                {
                    GUID = objOList_attribute_invisible.First().ID_Other,
                    Name = objOList_attribute_invisible.First().Name_Other,
                    GUID_Parent = objOList_attribute_invisible.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_is_null = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "attribute_is_null".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                              select objRef).ToList();

            if (objOList_attribute_is_null.Any())
            {
                OItem_attribute_is_null = new clsOntologyItem()
                {
                    GUID = objOList_attribute_is_null.First().ID_Other,
                    Name = objOList_attribute_is_null.First().Name_Other,
                    GUID_Parent = objOList_attribute_is_null.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_row_header = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "attribute_row_header".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                 select objRef).ToList();

            if (objOList_attribute_row_header.Any())
            {
                OItem_attribute_row_header = new clsOntologyItem()
                {
                    GUID = objOList_attribute_row_header.First().ID_Other,
                    Name = objOList_attribute_row_header.First().Name_Other,
                    GUID_Parent = objOList_attribute_row_header.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_standard = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "attribute_standard".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                               select objRef).ToList();

            if (objOList_attribute_standard.Any())
            {
                OItem_attribute_standard = new clsOntologyItem()
                {
                    GUID = objOList_attribute_standard.First().ID_Other,
                    Name = objOList_attribute_standard.First().Name_Other,
                    GUID_Parent = objOList_attribute_standard.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_value = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "attribute_value".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                            select objRef).ToList();

            if (objOList_attribute_value.Any())
            {
                OItem_attribute_value = new clsOntologyItem()
                {
                    GUID = objOList_attribute_value.First().ID_Other,
                    Name = objOList_attribute_value.First().Name_Other,
                    GUID_Parent = objOList_attribute_value.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_visible = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "attribute_visible".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                              select objRef).ToList();

            if (objOList_attribute_visible.Any())
            {
                OItem_attribute_visible = new clsOntologyItem()
                {
                    GUID = objOList_attribute_visible.First().ID_Other,
                    Name = objOList_attribute_visible.First().Name_Other,
                    GUID_Parent = objOList_attribute_visible.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_xml_text = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "attribute_xml_text".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                               select objRef).ToList();

            if (objOList_attribute_xml_text.Any())
            {
                OItem_attribute_xml_text = new clsOntologyItem()
                {
                    GUID = objOList_attribute_xml_text.First().ID_Other,
                    Name = objOList_attribute_xml_text.First().Name_Other,
                    GUID_Parent = objOList_attribute_xml_text.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attributetype_nextline = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "attributetype_nextline".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                   select objRef).ToList();

            if (objOList_attributetype_nextline.Any())
            {
                OItem_attributetype_nextline = new clsOntologyItem()
                {
                    GUID = objOList_attributetype_nextline.First().ID_Other,
                    Name = objOList_attributetype_nextline.First().Name_Other,
                    GUID_Parent = objOList_attributetype_nextline.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }


        }

        private void get_Config_RelationTypes()
        {
            var objOList_relationtype_belonging = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_belonging".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

            if (objOList_relationtype_belonging.Any())
            {
                OItem_relationtype_belonging = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belonging.First().ID_Other,
                    Name = objOList_relationtype_belonging.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belonging.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belonging_resources = (from objOItem in objDBLevel_Config1.ObjectRels
                                                             where objOItem.ID_Object == cstrID_Ontology
                                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                             where objRef.Name_Object.ToLower() == "relationtype_belonging_resources".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                             select objRef).ToList();

            if (objOList_relationtype_belonging_resources.Any())
            {
                OItem_relationtype_belonging_resources = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belonging_resources.First().ID_Other,
                    Name = objOList_relationtype_belonging_resources.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belonging_resources.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belonging_source = (from objOItem in objDBLevel_Config1.ObjectRels
                                                          where objOItem.ID_Object == cstrID_Ontology
                                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                          where objRef.Name_Object.ToLower() == "relationtype_belonging_source".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                          select objRef).ToList();

            if (objOList_relationtype_belonging_source.Any())
            {
                OItem_relationtype_belonging_source = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belonging_source.First().ID_Other,
                    Name = objOList_relationtype_belonging_source.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belonging_source.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belongsto = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_belongsto".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

            if (objOList_relationtype_belongsto.Any())
            {
                OItem_relationtype_belongsto = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belongsto.First().ID_Other,
                    Name = objOList_relationtype_belongsto.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belongsto.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_bold_tags = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_bold_tags".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

            if (objOList_relationtype_bold_tags.Any())
            {
                OItem_relationtype_bold_tags = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_bold_tags.First().ID_Other,
                    Name = objOList_relationtype_bold_tags.First().Name_Other,
                    GUID_Parent = objOList_relationtype_bold_tags.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_cell_config = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "relationtype_cell_config".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                     select objRef).ToList();

            if (objOList_relationtype_cell_config.Any())
            {
                OItem_relationtype_cell_config = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_cell_config.First().ID_Other,
                    Name = objOList_relationtype_cell_config.First().Name_Other,
                    GUID_Parent = objOList_relationtype_cell_config.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_cell_tags = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_cell_tags".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

            if (objOList_relationtype_cell_tags.Any())
            {
                OItem_relationtype_cell_tags = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_cell_tags.First().ID_Other,
                    Name = objOList_relationtype_cell_tags.First().Name_Other,
                    GUID_Parent = objOList_relationtype_cell_tags.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_connected_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                                      where objOItem.ID_Object == cstrID_Ontology
                                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                      where objRef.Name_Object.ToLower() == "relationtype_connected_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                      select objRef).ToList();

            if (objOList_relationtype_connected_by.Any())
            {
                OItem_relationtype_connected_by = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_connected_by.First().ID_Other,
                    Name = objOList_relationtype_connected_by.First().Name_Other,
                    GUID_Parent = objOList_relationtype_connected_by.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_contains = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "relationtype_contains".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                  select objRef).ToList();

            if (objOList_relationtype_contains.Any())
            {
                OItem_relationtype_contains = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_contains.First().ID_Other,
                    Name = objOList_relationtype_contains.First().Name_Other,
                    GUID_Parent = objOList_relationtype_contains.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_formatted_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                                      where objOItem.ID_Object == cstrID_Ontology
                                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                      where objRef.Name_Object.ToLower() == "relationtype_formatted_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                      select objRef).ToList();

            if (objOList_relationtype_formatted_by.Any())
            {
                OItem_relationtype_formatted_by = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_formatted_by.First().ID_Other,
                    Name = objOList_relationtype_formatted_by.First().Name_Other,
                    GUID_Parent = objOList_relationtype_formatted_by.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_header_tags = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "relationtype_header_tags".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                     select objRef).ToList();

            if (objOList_relationtype_header_tags.Any())
            {
                OItem_relationtype_header_tags = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_header_tags.First().ID_Other,
                    Name = objOList_relationtype_header_tags.First().Name_Other,
                    GUID_Parent = objOList_relationtype_header_tags.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_is = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "relationtype_is".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                            select objRef).ToList();

            if (objOList_relationtype_is.Any())
            {
                OItem_relationtype_is = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_is.First().ID_Other,
                    Name = objOList_relationtype_is.First().Name_Other,
                    GUID_Parent = objOList_relationtype_is.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_is_of_type = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_is_of_type".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

            if (objOList_relationtype_is_of_type.Any())
            {
                OItem_relationtype_is_of_type = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_is_of_type.First().ID_Other,
                    Name = objOList_relationtype_is_of_type.First().Name_Other,
                    GUID_Parent = objOList_relationtype_is_of_type.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_leads = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_leads".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

            if (objOList_relationtype_leads.Any())
            {
                OItem_relationtype_leads = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_leads.First().ID_Other,
                    Name = objOList_relationtype_leads.First().Name_Other,
                    GUID_Parent = objOList_relationtype_leads.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_located_in = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_located_in".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

            if (objOList_relationtype_located_in.Any())
            {
                OItem_relationtype_located_in = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_located_in.First().ID_Other,
                    Name = objOList_relationtype_located_in.First().Name_Other,
                    GUID_Parent = objOList_relationtype_located_in.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_row_config = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_row_config".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

            if (objOList_relationtype_row_config.Any())
            {
                OItem_relationtype_row_config = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_row_config.First().ID_Other,
                    Name = objOList_relationtype_row_config.First().Name_Other,
                    GUID_Parent = objOList_relationtype_row_config.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_row_tags = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "relationtype_row_tags".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                  select objRef).ToList();

            if (objOList_relationtype_row_tags.Any())
            {
                OItem_relationtype_row_tags = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_row_tags.First().ID_Other,
                    Name = objOList_relationtype_row_tags.First().Name_Other,
                    GUID_Parent = objOList_relationtype_row_tags.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_table_config = (from objOItem in objDBLevel_Config1.ObjectRels
                                                      where objOItem.ID_Object == cstrID_Ontology
                                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                      where objRef.Name_Object.ToLower() == "relationtype_table_config".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                      select objRef).ToList();

            if (objOList_relationtype_table_config.Any())
            {
                OItem_relationtype_table_config = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_table_config.First().ID_Other,
                    Name = objOList_relationtype_table_config.First().Name_Other,
                    GUID_Parent = objOList_relationtype_table_config.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_table_tags = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_table_tags".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

            if (objOList_relationtype_table_tags.Any())
            {
                OItem_relationtype_table_tags = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_table_tags.First().ID_Other,
                    Name = objOList_relationtype_table_tags.First().Name_Other,
                    GUID_Parent = objOList_relationtype_table_tags.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_type_field = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_type_field".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

            if (objOList_relationtype_type_field.Any())
            {
                OItem_relationtype_type_field = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_type_field.First().ID_Other,
                    Name = objOList_relationtype_type_field.First().Name_Other,
                    GUID_Parent = objOList_relationtype_type_field.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_value_type = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_value_type".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

            if (objOList_relationtype_value_type.Any())
            {
                OItem_relationtype_value_type = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_value_type.First().ID_Other,
                    Name = objOList_relationtype_value_type.First().Name_Other,
                    GUID_Parent = objOList_relationtype_value_type.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }


        }

        private void get_Config_Objects()
        {
            var objOList_object_baseconfig = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "object_baseconfig".ToLower() && objRef.Ontology == Globals.Type_Object
                                              select objRef).ToList();

            if (objOList_object_baseconfig.Any())
            {
                OItem_object_baseconfig = new clsOntologyItem()
                {
                    GUID = objOList_object_baseconfig.First().ID_Other,
                    Name = objOList_object_baseconfig.First().Name_Other,
                    GUID_Parent = objOList_object_baseconfig.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_bit = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "object_bit".ToLower() && objRef.Ontology == Globals.Type_Object
                                       select objRef).ToList();

            if (objOList_object_bit.Any())
            {
                OItem_object_bit = new clsOntologyItem()
                {
                    GUID = objOList_object_bit.First().ID_Other,
                    Name = objOList_object_bit.First().Name_Other,
                    GUID_Parent = objOList_object_bit.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_commandlinerun_module = (from objOItem in objDBLevel_Config1.ObjectRels
                                                         where objOItem.ID_Object == cstrID_Ontology
                                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                         where objRef.Name_Object.ToLower() == "object_commandlinerun_module".ToLower() && objRef.Ontology == Globals.Type_Object
                                                         select objRef).ToList();

            if (objOList_object_commandlinerun_module.Any())
            {
                OItem_object_commandlinerun_module = new clsOntologyItem()
                {
                    GUID = objOList_object_commandlinerun_module.First().ID_Other,
                    Name = objOList_object_commandlinerun_module.First().Name_Other,
                    GUID_Parent = objOList_object_commandlinerun_module.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_datetime = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "object_datetime".ToLower() && objRef.Ontology == Globals.Type_Object
                                            select objRef).ToList();

            if (objOList_object_datetime.Any())
            {
                OItem_object_datetime = new clsOntologyItem()
                {
                    GUID = objOList_object_datetime.First().ID_Other,
                    Name = objOList_object_datetime.First().Name_Other,
                    GUID_Parent = objOList_object_datetime.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_double = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "object_double".ToLower() && objRef.Ontology == Globals.Type_Object
                                          select objRef).ToList();

            if (objOList_object_double.Any())
            {
                OItem_object_double = new clsOntologyItem()
                {
                    GUID = objOList_object_double.First().ID_Other,
                    Name = objOList_object_double.First().Name_Other,
                    GUID_Parent = objOList_object_double.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_int = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "object_int".ToLower() && objRef.Ontology == Globals.Type_Object
                                       select objRef).ToList();

            if (objOList_object_int.Any())
            {
                OItem_object_int = new clsOntologyItem()
                {
                    GUID = objOList_object_int.First().ID_Other,
                    Name = objOList_object_int.First().Name_Other,
                    GUID_Parent = objOList_object_int.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_report_connection = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "object_report_connection".ToLower() && objRef.Ontology == Globals.Type_Object
                                                     select objRef).ToList();

            if (objOList_object_report_connection.Any())
            {
                OItem_object_report_connection = new clsOntologyItem()
                {
                    GUID = objOList_object_report_connection.First().ID_Other,
                    Name = objOList_object_report_connection.First().Name_Other,
                    GUID_Parent = objOList_object_report_connection.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_string = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "object_string".ToLower() && objRef.Ontology == Globals.Type_Object
                                          select objRef).ToList();

            if (objOList_object_string.Any())
            {
                OItem_object_string = new clsOntologyItem()
                {
                    GUID = objOList_object_string.First().ID_Other,
                    Name = objOList_object_string.First().Name_Other,
                    GUID_Parent = objOList_object_string.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_field_type_datetime = (from objOItem in objDBLevel_Config1.ObjectRels
                                                      where objOItem.ID_Object == cstrID_Ontology
                                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                      where objRef.Name_Object.ToLower() == "token_field_type_datetime".ToLower() && objRef.Ontology == Globals.Type_Object
                                                      select objRef).ToList();

            if (objOList_token_field_type_datetime.Any())
            {
                OItem_token_field_type_datetime = new clsOntologyItem()
                {
                    GUID = objOList_token_field_type_datetime.First().ID_Other,
                    Name = objOList_token_field_type_datetime.First().Name_Other,
                    GUID_Parent = objOList_token_field_type_datetime.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_field_type_guid = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "token_field_type_guid".ToLower() && objRef.Ontology == Globals.Type_Object
                                                  select objRef).ToList();

            if (objOList_token_field_type_guid.Any())
            {
                OItem_token_field_type_guid = new clsOntologyItem()
                {
                    GUID = objOList_token_field_type_guid.First().ID_Other,
                    Name = objOList_token_field_type_guid.First().Name_Other,
                    GUID_Parent = objOList_token_field_type_guid.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_field_type_text = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "token_field_type_text".ToLower() && objRef.Ontology == Globals.Type_Object
                                                  select objRef).ToList();

            if (objOList_token_field_type_text.Any())
            {
                OItem_token_field_type_text = new clsOntologyItem()
                {
                    GUID = objOList_token_field_type_text.First().ID_Other,
                    Name = objOList_token_field_type_text.First().Name_Other,
                    GUID_Parent = objOList_token_field_type_text.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_field_type_zahl = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "token_field_type_zahl".ToLower() && objRef.Ontology == Globals.Type_Object
                                                  select objRef).ToList();

            if (objOList_token_field_type_zahl.Any())
            {
                OItem_token_field_type_zahl = new clsOntologyItem()
                {
                    GUID = objOList_token_field_type_zahl.First().ID_Other,
                    Name = objOList_token_field_type_zahl.First().Name_Other,
                    GUID_Parent = objOList_token_field_type_zahl.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_ontology_relation_rule_child_token = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                     where objOItem.ID_Object == cstrID_Ontology
                                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                     where objRef.Name_Object.ToLower() == "token_ontology_relation_rule_child_token".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                     select objRef).ToList();

            if (objOList_token_ontology_relation_rule_child_token.Any())
            {
                OItem_token_ontology_relation_rule_child_token = new clsOntologyItem()
                {
                    GUID = objOList_token_ontology_relation_rule_child_token.First().ID_Other,
                    Name = objOList_token_ontology_relation_rule_child_token.First().Name_Other,
                    GUID_Parent = objOList_token_ontology_relation_rule_child_token.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_ontology_relation_rule_inner_join = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                    where objOItem.ID_Object == cstrID_Ontology
                                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                    where objRef.Name_Object.ToLower() == "token_ontology_relation_rule_inner_join".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                    select objRef).ToList();

            if (objOList_token_ontology_relation_rule_inner_join.Any())
            {
                OItem_token_ontology_relation_rule_inner_join = new clsOntologyItem()
                {
                    GUID = objOList_token_ontology_relation_rule_inner_join.First().ID_Other,
                    Name = objOList_token_ontology_relation_rule_inner_join.First().Name_Other,
                    GUID_Parent = objOList_token_ontology_relation_rule_inner_join.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_ontology_relation_rule_left_outer_join = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                         where objOItem.ID_Object == cstrID_Ontology
                                                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                         where objRef.Name_Object.ToLower() == "token_ontology_relation_rule_left_outer_join".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                         select objRef).ToList();

            if (objOList_token_ontology_relation_rule_left_outer_join.Any())
            {
                OItem_token_ontology_relation_rule_left_outer_join = new clsOntologyItem()
                {
                    GUID = objOList_token_ontology_relation_rule_left_outer_join.First().ID_Other,
                    Name = objOList_token_ontology_relation_rule_left_outer_join.First().Name_Other,
                    GUID_Parent = objOList_token_ontology_relation_rule_left_outer_join.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_ontology_relation_rule_name_of_type_parse = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                            where objOItem.ID_Object == cstrID_Ontology
                                                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                            where objRef.Name_Object.ToLower() == "token_ontology_relation_rule_name_of_type_parse".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                            select objRef).ToList();

            if (objOList_token_ontology_relation_rule_name_of_type_parse.Any())
            {
                OItem_token_ontology_relation_rule_name_of_type_parse = new clsOntologyItem()
                {
                    GUID = objOList_token_ontology_relation_rule_name_of_type_parse.First().ID_Other,
                    Name = objOList_token_ontology_relation_rule_name_of_type_parse.First().Name_Other,
                    GUID_Parent = objOList_token_ontology_relation_rule_name_of_type_parse.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_ontology_relation_rule_only_item = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                   where objOItem.ID_Object == cstrID_Ontology
                                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                   where objRef.Name_Object.ToLower() == "token_ontology_relation_rule_only_item".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                   select objRef).ToList();

            if (objOList_token_ontology_relation_rule_only_item.Any())
            {
                OItem_token_ontology_relation_rule_only_item = new clsOntologyItem()
                {
                    GUID = objOList_token_ontology_relation_rule_only_item.First().ID_Other,
                    Name = objOList_token_ontology_relation_rule_only_item.First().Name_Other,
                    GUID_Parent = objOList_token_ontology_relation_rule_only_item.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_ontology_relation_rule_parent_types = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                      where objOItem.ID_Object == cstrID_Ontology
                                                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                      where objRef.Name_Object.ToLower() == "token_ontology_relation_rule_parent_types".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                      select objRef).ToList();

            if (objOList_token_ontology_relation_rule_parent_types.Any())
            {
                OItem_token_ontology_relation_rule_parent_types = new clsOntologyItem()
                {
                    GUID = objOList_token_ontology_relation_rule_parent_types.First().ID_Other,
                    Name = objOList_token_ontology_relation_rule_parent_types.First().Name_Other,
                    GUID_Parent = objOList_token_ontology_relation_rule_parent_types.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_ontology_relation_rule_relation_break = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                        where objOItem.ID_Object == cstrID_Ontology
                                                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                        where objRef.Name_Object.ToLower() == "token_ontology_relation_rule_relation_break".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                        select objRef).ToList();

            if (objOList_token_ontology_relation_rule_relation_break.Any())
            {
                OItem_token_ontology_relation_rule_relation_break = new clsOntologyItem()
                {
                    GUID = objOList_token_ontology_relation_rule_relation_break.First().ID_Other,
                    Name = objOList_token_ontology_relation_rule_relation_break.First().Name_Other,
                    GUID_Parent = objOList_token_ontology_relation_rule_relation_break.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_ontology_relation_rule_right_outer_join = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                          where objOItem.ID_Object == cstrID_Ontology
                                                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                          where objRef.Name_Object.ToLower() == "token_ontology_relation_rule_right_outer_join".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                          select objRef).ToList();

            if (objOList_token_ontology_relation_rule_right_outer_join.Any())
            {
                OItem_token_ontology_relation_rule_right_outer_join = new clsOntologyItem()
                {
                    GUID = objOList_token_ontology_relation_rule_right_outer_join.First().ID_Other,
                    Name = objOList_token_ontology_relation_rule_right_outer_join.First().Name_Other,
                    GUID_Parent = objOList_token_ontology_relation_rule_right_outer_join.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_report_type_elasticview = (from objOItem in objDBLevel_Config1.ObjectRels
                                                          where objOItem.ID_Object == cstrID_Ontology
                                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                          where objRef.Name_Object.ToLower() == "token_report_type_elasticview".ToLower() && objRef.Ontology == Globals.Type_Object
                                                          select objRef).ToList();

            if (objOList_token_report_type_elasticview.Any())
            {
                OItem_token_report_type_elasticview = new clsOntologyItem()
                {
                    GUID = objOList_token_report_type_elasticview.First().ID_Other,
                    Name = objOList_token_report_type_elasticview.First().Name_Other,
                    GUID_Parent = objOList_token_report_type_elasticview.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_report_type_token_report = (from objOItem in objDBLevel_Config1.ObjectRels
                                                           where objOItem.ID_Object == cstrID_Ontology
                                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                           where objRef.Name_Object.ToLower() == "token_report_type_token_report".ToLower() && objRef.Ontology == Globals.Type_Object
                                                           select objRef).ToList();

            if (objOList_token_report_type_token_report.Any())
            {
                OItem_token_report_type_token_report = new clsOntologyItem()
                {
                    GUID = objOList_token_report_type_token_report.First().ID_Other,
                    Name = objOList_token_report_type_token_report.First().Name_Other,
                    GUID_Parent = objOList_token_report_type_token_report.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_report_type_view = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "token_report_type_view".ToLower() && objRef.Ontology == Globals.Type_Object
                                                   select objRef).ToList();

            if (objOList_token_report_type_view.Any())
            {
                OItem_token_report_type_view = new clsOntologyItem()
                {
                    GUID = objOList_token_report_type_view.First().ID_Other,
                    Name = objOList_token_report_type_view.First().Name_Other,
                    GUID_Parent = objOList_token_report_type_view.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_variable_author = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "token_variable_author".ToLower() && objRef.Ontology == Globals.Type_Object
                                                  select objRef).ToList();

            if (objOList_token_variable_author.Any())
            {
                OItem_token_variable_author = new clsOntologyItem()
                {
                    GUID = objOList_token_variable_author.First().ID_Other,
                    Name = objOList_token_variable_author.First().Name_Other,
                    GUID_Parent = objOList_token_variable_author.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_variable_cell_list = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "token_variable_cell_list".ToLower() && objRef.Ontology == Globals.Type_Object
                                                     select objRef).ToList();

            if (objOList_token_variable_cell_list.Any())
            {
                OItem_token_variable_cell_list = new clsOntologyItem()
                {
                    GUID = objOList_token_variable_cell_list.First().ID_Other,
                    Name = objOList_token_variable_cell_list.First().Name_Other,
                    GUID_Parent = objOList_token_variable_cell_list.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_variable_cell_name = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "token_variable_cell_name".ToLower() && objRef.Ontology == Globals.Type_Object
                                                     select objRef).ToList();

            if (objOList_token_variable_cell_name.Any())
            {
                OItem_token_variable_cell_name = new clsOntologyItem()
                {
                    GUID = objOList_token_variable_cell_name.First().ID_Other,
                    Name = objOList_token_variable_cell_name.First().Name_Other,
                    GUID_Parent = objOList_token_variable_cell_name.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_variable_cell_value = (from objOItem in objDBLevel_Config1.ObjectRels
                                                      where objOItem.ID_Object == cstrID_Ontology
                                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                      where objRef.Name_Object.ToLower() == "token_variable_cell_value".ToLower() && objRef.Ontology == Globals.Type_Object
                                                      select objRef).ToList();

            if (objOList_token_variable_cell_value.Any())
            {
                OItem_token_variable_cell_value = new clsOntologyItem()
                {
                    GUID = objOList_token_variable_cell_value.First().ID_Other,
                    Name = objOList_token_variable_cell_value.First().Name_Other,
                    GUID_Parent = objOList_token_variable_cell_value.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_variable_colcount = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "token_variable_colcount".ToLower() && objRef.Ontology == Globals.Type_Object
                                                    select objRef).ToList();

            if (objOList_token_variable_colcount.Any())
            {
                OItem_token_variable_colcount = new clsOntologyItem()
                {
                    GUID = objOList_token_variable_colcount.First().ID_Other,
                    Name = objOList_token_variable_colcount.First().Name_Other,
                    GUID_Parent = objOList_token_variable_colcount.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_variable_datetime_tz = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "token_variable_datetime_tz".ToLower() && objRef.Ontology == Globals.Type_Object
                                                       select objRef).ToList();

            if (objOList_token_variable_datetime_tz.Any())
            {
                OItem_token_variable_datetime_tz = new clsOntologyItem()
                {
                    GUID = objOList_token_variable_datetime_tz.First().ID_Other,
                    Name = objOList_token_variable_datetime_tz.First().Name_Other,
                    GUID_Parent = objOList_token_variable_datetime_tz.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_variable_id = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "token_variable_id".ToLower() && objRef.Ontology == Globals.Type_Object
                                              select objRef).ToList();

            if (objOList_token_variable_id.Any())
            {
                OItem_token_variable_id = new clsOntologyItem()
                {
                    GUID = objOList_token_variable_id.First().ID_Other,
                    Name = objOList_token_variable_id.First().Name_Other,
                    GUID_Parent = objOList_token_variable_id.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_variable_report = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "token_variable_report".ToLower() && objRef.Ontology == Globals.Type_Object
                                                  select objRef).ToList();

            if (objOList_token_variable_report.Any())
            {
                OItem_token_variable_report = new clsOntologyItem()
                {
                    GUID = objOList_token_variable_report.First().ID_Other,
                    Name = objOList_token_variable_report.First().Name_Other,
                    GUID_Parent = objOList_token_variable_report.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_variable_report_20 = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "token_variable_report_20".ToLower() && objRef.Ontology == Globals.Type_Object
                                                     select objRef).ToList();

            if (objOList_token_variable_report_20.Any())
            {
                OItem_token_variable_report_20 = new clsOntologyItem()
                {
                    GUID = objOList_token_variable_report_20.First().ID_Other,
                    Name = objOList_token_variable_report_20.First().Name_Other,
                    GUID_Parent = objOList_token_variable_report_20.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_variable_row_list = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "token_variable_row_list".ToLower() && objRef.Ontology == Globals.Type_Object
                                                    select objRef).ToList();

            if (objOList_token_variable_row_list.Any())
            {
                OItem_token_variable_row_list = new clsOntologyItem()
                {
                    GUID = objOList_token_variable_row_list.First().ID_Other,
                    Name = objOList_token_variable_row_list.First().Name_Other,
                    GUID_Parent = objOList_token_variable_row_list.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_variable_row_name = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "token_variable_row_name".ToLower() && objRef.Ontology == Globals.Type_Object
                                                    select objRef).ToList();

            if (objOList_token_variable_row_name.Any())
            {
                OItem_token_variable_row_name = new clsOntologyItem()
                {
                    GUID = objOList_token_variable_row_name.First().ID_Other,
                    Name = objOList_token_variable_row_name.First().Name_Other,
                    GUID_Parent = objOList_token_variable_row_name.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_token_variable_rowcount = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "token_variable_rowcount".ToLower() && objRef.Ontology == Globals.Type_Object
                                                    select objRef).ToList();

            if (objOList_token_variable_rowcount.Any())
            {
                OItem_token_variable_rowcount = new clsOntologyItem()
                {
                    GUID = objOList_token_variable_rowcount.First().ID_Other,
                    Name = objOList_token_variable_rowcount.First().Name_Other,
                    GUID_Parent = objOList_token_variable_rowcount.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }


        }

        private void get_Config_Classes()
        {
            var objOList_class_aggregate_type = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "class_aggregate_type".ToLower() && objRef.Ontology == Globals.Type_Class
                                                 select objRef).ToList();

            if (objOList_class_aggregate_type.Any())
            {
                OItem_class_aggregate_type = new clsOntologyItem()
                {
                    GUID = objOList_class_aggregate_type.First().ID_Other,
                    Name = objOList_class_aggregate_type.First().Name_Other,
                    GUID_Parent = objOList_class_aggregate_type.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_aggregate = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "class_aggregate".ToLower() && objRef.Ontology == Globals.Type_Class
                                            select objRef).ToList();

            if (objOList_class_aggregate.Any())
            {
                OItem_class_aggregate = new clsOntologyItem()
                {
                    GUID = objOList_class_aggregate.First().ID_Other,
                    Name = objOList_class_aggregate.First().Name_Other,
                    GUID_Parent = objOList_class_aggregate.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }


            var objOList_class_clipboardfilter = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "class_clipboardfilter".ToLower() && objRef.Ontology == Globals.Type_Class
                                                  select objRef).ToList();

            if (objOList_class_clipboardfilter.Any())
            {
                OItem_class_clipboardfilter = new clsOntologyItem()
                {
                    GUID = objOList_class_clipboardfilter.First().ID_Other,
                    Name = objOList_class_clipboardfilter.First().Name_Other,
                    GUID_Parent = objOList_class_clipboardfilter.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_clipboardfilter_tags = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "class_clipboardfilter_tags".ToLower() && objRef.Ontology == Globals.Type_Class
                                                       select objRef).ToList();

            if (objOList_class_clipboardfilter_tags.Any())
            {
                OItem_class_clipboardfilter_tags = new clsOntologyItem()
                {
                    GUID = objOList_class_clipboardfilter_tags.First().ID_Other,
                    Name = objOList_class_clipboardfilter_tags.First().Name_Other,
                    GUID_Parent = objOList_class_clipboardfilter_tags.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_datatypes = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "class_datatypes".ToLower() && objRef.Ontology == Globals.Type_Class
                                            select objRef).ToList();

            if (objOList_class_datatypes.Any())
            {
                OItem_class_datatypes = new clsOntologyItem()
                {
                    GUID = objOList_class_datatypes.First().ID_Other,
                    Name = objOList_class_datatypes.First().Name_Other,
                    GUID_Parent = objOList_class_datatypes.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_field = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "class_field".ToLower() && objRef.Ontology == Globals.Type_Class
                                        select objRef).ToList();

            if (objOList_class_field.Any())
            {
                OItem_class_field = new clsOntologyItem()
                {
                    GUID = objOList_class_field.First().ID_Other,
                    Name = objOList_class_field.First().Name_Other,
                    GUID_Parent = objOList_class_field.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_types__elastic_search_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                         where objOItem.ID_Object == cstrID_Ontology
                                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                         where objRef.Name_Object.ToLower() == "class_types__elastic_search_".ToLower() && objRef.Ontology == Globals.Type_Class
                                                         select objRef).ToList();

            if (objOList_class_types__elastic_search_.Any())
            {
                OItem_class_types__elastic_search_ = new clsOntologyItem()
                {
                    GUID = objOList_class_types__elastic_search_.First().ID_Other,
                    Name = objOList_class_types__elastic_search_.First().Name_Other,
                    GUID_Parent = objOList_class_types__elastic_search_.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_comparison_operators = (from objOItem in objDBLevel_Config1.ObjectRels
                                                      where objOItem.ID_Object == cstrID_Ontology
                                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                      where objRef.Name_Object.ToLower() == "type_comparison_operators".ToLower() && objRef.Ontology == Globals.Type_Class
                                                      select objRef).ToList();

            if (objOList_type_comparison_operators.Any())
            {
                OItem_type_comparison_operators = new clsOntologyItem()
                {
                    GUID = objOList_type_comparison_operators.First().ID_Other,
                    Name = objOList_type_comparison_operators.First().Name_Other,
                    GUID_Parent = objOList_type_comparison_operators.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_database = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "type_database".ToLower() && objRef.Ontology == Globals.Type_Class
                                          select objRef).ToList();

            if (objOList_type_database.Any())
            {
                OItem_type_database = new clsOntologyItem()
                {
                    GUID = objOList_type_database.First().ID_Other,
                    Name = objOList_type_database.First().Name_Other,
                    GUID_Parent = objOList_type_database.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_database_on_server = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "type_database_on_server".ToLower() && objRef.Ontology == Globals.Type_Class
                                                    select objRef).ToList();

            if (objOList_type_database_on_server.Any())
            {
                OItem_type_database_on_server = new clsOntologyItem()
                {
                    GUID = objOList_type_database_on_server.First().ID_Other,
                    Name = objOList_type_database_on_server.First().Name_Other,
                    GUID_Parent = objOList_type_database_on_server.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_datatypes__ms_sql_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "type_datatypes__ms_sql_".ToLower() && objRef.Ontology == Globals.Type_Class
                                                    select objRef).ToList();

            if (objOList_type_datatypes__ms_sql_.Any())
            {
                OItem_type_datatypes__ms_sql_ = new clsOntologyItem()
                {
                    GUID = objOList_type_datatypes__ms_sql_.First().ID_Other,
                    Name = objOList_type_datatypes__ms_sql_.First().Name_Other,
                    GUID_Parent = objOList_type_datatypes__ms_sql_.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_db_columns = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "type_db_columns".ToLower() && objRef.Ontology == Globals.Type_Class
                                            select objRef).ToList();

            if (objOList_type_db_columns.Any())
            {
                OItem_type_db_columns = new clsOntologyItem()
                {
                    GUID = objOList_type_db_columns.First().ID_Other,
                    Name = objOList_type_db_columns.First().Name_Other,
                    GUID_Parent = objOList_type_db_columns.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_db_procedure = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "type_db_procedure".ToLower() && objRef.Ontology == Globals.Type_Class
                                              select objRef).ToList();

            if (objOList_type_db_procedure.Any())
            {
                OItem_type_db_procedure = new clsOntologyItem()
                {
                    GUID = objOList_type_db_procedure.First().ID_Other,
                    Name = objOList_type_db_procedure.First().Name_Other,
                    GUID_Parent = objOList_type_db_procedure.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_db_views = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "type_db_views".ToLower() && objRef.Ontology == Globals.Type_Class
                                          select objRef).ToList();

            if (objOList_type_db_views.Any())
            {
                OItem_type_db_views = new clsOntologyItem()
                {
                    GUID = objOList_type_db_views.First().ID_Other,
                    Name = objOList_type_db_views.First().Name_Other,
                    GUID_Parent = objOList_type_db_views.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_field_format = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "type_field_format".ToLower() && objRef.Ontology == Globals.Type_Class
                                              select objRef).ToList();

            if (objOList_type_field_format.Any())
            {
                OItem_type_field_format = new clsOntologyItem()
                {
                    GUID = objOList_type_field_format.First().ID_Other,
                    Name = objOList_type_field_format.First().Name_Other,
                    GUID_Parent = objOList_type_field_format.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_field_type = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "type_field_type".ToLower() && objRef.Ontology == Globals.Type_Class
                                            select objRef).ToList();

            if (objOList_type_field_type.Any())
            {
                OItem_type_field_type = new clsOntologyItem()
                {
                    GUID = objOList_type_field_type.First().ID_Other,
                    Name = objOList_type_field_type.First().Name_Other,
                    GUID_Parent = objOList_type_field_type.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_file = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_file".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

            if (objOList_type_file.Any())
            {
                OItem_type_file = new clsOntologyItem()
                {
                    GUID = objOList_type_file.First().ID_Other,
                    Name = objOList_type_file.First().Name_Other,
                    GUID_Parent = objOList_type_file.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_indexes__elastic_search_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                          where objOItem.ID_Object == cstrID_Ontology
                                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                          where objRef.Name_Object.ToLower() == "type_indexes__elastic_search_".ToLower() && objRef.Ontology == Globals.Type_Class
                                                          select objRef).ToList();

            if (objOList_type_indexes__elastic_search_.Any())
            {
                OItem_type_indexes__elastic_search_ = new clsOntologyItem()
                {
                    GUID = objOList_type_indexes__elastic_search_.First().ID_Other,
                    Name = objOList_type_indexes__elastic_search_.First().Name_Other,
                    GUID_Parent = objOList_type_indexes__elastic_search_.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_logical_operators = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "type_logical_operators".ToLower() && objRef.Ontology == Globals.Type_Class
                                                   select objRef).ToList();

            if (objOList_type_logical_operators.Any())
            {
                OItem_type_logical_operators = new clsOntologyItem()
                {
                    GUID = objOList_type_logical_operators.First().ID_Other,
                    Name = objOList_type_logical_operators.First().Name_Other,
                    GUID_Parent = objOList_type_logical_operators.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_ontology_item = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "type_ontology_item".ToLower() && objRef.Ontology == Globals.Type_Class
                                               select objRef).ToList();

            if (objOList_type_ontology_item.Any())
            {
                OItem_type_ontology_item = new clsOntologyItem()
                {
                    GUID = objOList_type_ontology_item.First().ID_Other,
                    Name = objOList_type_ontology_item.First().Name_Other,
                    GUID_Parent = objOList_type_ontology_item.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_ontology_join = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "type_ontology_join".ToLower() && objRef.Ontology == Globals.Type_Class
                                               select objRef).ToList();

            if (objOList_type_ontology_join.Any())
            {
                OItem_type_ontology_join = new clsOntologyItem()
                {
                    GUID = objOList_type_ontology_join.First().ID_Other,
                    Name = objOList_type_ontology_join.First().Name_Other,
                    GUID_Parent = objOList_type_ontology_join.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_password = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "type_password".ToLower() && objRef.Ontology == Globals.Type_Class
                                          select objRef).ToList();

            if (objOList_type_password.Any())
            {
                OItem_type_password = new clsOntologyItem()
                {
                    GUID = objOList_type_password.First().ID_Other,
                    Name = objOList_type_password.First().Name_Other,
                    GUID_Parent = objOList_type_password.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_path = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_path".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

            if (objOList_type_path.Any())
            {
                OItem_type_path = new clsOntologyItem()
                {
                    GUID = objOList_type_path.First().ID_Other,
                    Name = objOList_type_path.First().Name_Other,
                    GUID_Parent = objOList_type_path.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_port = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_port".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

            if (objOList_type_port.Any())
            {
                OItem_type_port = new clsOntologyItem()
                {
                    GUID = objOList_type_port.First().ID_Other,
                    Name = objOList_type_port.First().Name_Other,
                    GUID_Parent = objOList_type_port.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_report_field = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "type_report_field".ToLower() && objRef.Ontology == Globals.Type_Class
                                              select objRef).ToList();

            if (objOList_type_report_field.Any())
            {
                OItem_type_report_field = new clsOntologyItem()
                {
                    GUID = objOList_type_report_field.First().ID_Other,
                    Name = objOList_type_report_field.First().Name_Other,
                    GUID_Parent = objOList_type_report_field.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_report_filter = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "type_report_filter".ToLower() && objRef.Ontology == Globals.Type_Class
                                               select objRef).ToList();

            if (objOList_type_report_filter.Any())
            {
                OItem_type_report_filter = new clsOntologyItem()
                {
                    GUID = objOList_type_report_filter.First().ID_Other,
                    Name = objOList_type_report_filter.First().Name_Other,
                    GUID_Parent = objOList_type_report_filter.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_report_sort = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "type_report_sort".ToLower() && objRef.Ontology == Globals.Type_Class
                                             select objRef).ToList();

            if (objOList_type_report_sort.Any())
            {
                OItem_type_report_sort = new clsOntologyItem()
                {
                    GUID = objOList_type_report_sort.First().ID_Other,
                    Name = objOList_type_report_sort.First().Name_Other,
                    GUID_Parent = objOList_type_report_sort.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_report_type = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "type_report_type".ToLower() && objRef.Ontology == Globals.Type_Class
                                             select objRef).ToList();

            if (objOList_type_report_type.Any())
            {
                OItem_type_report_type = new clsOntologyItem()
                {
                    GUID = objOList_type_report_type.First().ID_Other,
                    Name = objOList_type_report_type.First().Name_Other,
                    GUID_Parent = objOList_type_report_type.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_reports = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "type_reports".ToLower() && objRef.Ontology == Globals.Type_Class
                                         select objRef).ToList();

            if (objOList_type_reports.Any())
            {
                OItem_type_reports = new clsOntologyItem()
                {
                    GUID = objOList_type_reports.First().ID_Other,
                    Name = objOList_type_reports.First().Name_Other,
                    GUID_Parent = objOList_type_reports.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_server = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "type_server".ToLower() && objRef.Ontology == Globals.Type_Class
                                        select objRef).ToList();

            if (objOList_type_server.Any())
            {
                OItem_type_server = new clsOntologyItem()
                {
                    GUID = objOList_type_server.First().ID_Other,
                    Name = objOList_type_server.First().Name_Other,
                    GUID_Parent = objOList_type_server.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_server_port = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "type_server_port".ToLower() && objRef.Ontology == Globals.Type_Class
                                             select objRef).ToList();

            if (objOList_type_server_port.Any())
            {
                OItem_type_server_port = new clsOntologyItem()
                {
                    GUID = objOList_type_server_port.First().ID_Other,
                    Name = objOList_type_server_port.First().Name_Other,
                    GUID_Parent = objOList_type_server_port.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_url = (from objOItem in objDBLevel_Config1.ObjectRels
                                     where objOItem.ID_Object == cstrID_Ontology
                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                     where objRef.Name_Object.ToLower() == "type_url".ToLower() && objRef.Ontology == Globals.Type_Class
                                     select objRef).ToList();

            if (objOList_type_url.Any())
            {
                OItem_type_url = new clsOntologyItem()
                {
                    GUID = objOList_type_url.First().ID_Other,
                    Name = objOList_type_url.First().Name_Other,
                    GUID_Parent = objOList_type_url.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_user = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_user".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

            if (objOList_type_user.Any())
            {
                OItem_type_user = new clsOntologyItem()
                {
                    GUID = objOList_type_user.First().ID_Other,
                    Name = objOList_type_user.First().Name_Other,
                    GUID_Parent = objOList_type_user.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_variable = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "type_variable".ToLower() && objRef.Ontology == Globals.Type_Class
                                          select objRef).ToList();

            if (objOList_type_variable.Any())
            {
                OItem_type_variable = new clsOntologyItem()
                {
                    GUID = objOList_type_variable.First().ID_Other,
                    Name = objOList_type_variable.First().Name_Other,
                    GUID_Parent = objOList_type_variable.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_xml = (from objOItem in objDBLevel_Config1.ObjectRels
                                     where objOItem.ID_Object == cstrID_Ontology
                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                     where objRef.Name_Object.ToLower() == "type_xml".ToLower() && objRef.Ontology == Globals.Type_Class
                                     select objRef).ToList();

            if (objOList_type_xml.Any())
            {
                OItem_type_xml = new clsOntologyItem()
                {
                    GUID = objOList_type_xml.First().ID_Other,
                    Name = objOList_type_xml.First().Name_Other,
                    GUID_Parent = objOList_type_xml.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_xml_config = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "type_xml_config".ToLower() && objRef.Ontology == Globals.Type_Class
                                            select objRef).ToList();

            if (objOList_type_xml_config.Any())
            {
                OItem_type_xml_config = new clsOntologyItem()
                {
                    GUID = objOList_type_xml_config.First().ID_Other,
                    Name = objOList_type_xml_config.First().Name_Other,
                    GUID_Parent = objOList_type_xml_config.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }


        }
        public string IdLocalConfig
        {
            get
            {
                var attrib =
                      Assembly.GetExecutingAssembly()
                          .GetCustomAttributes(true)
                          .FirstOrDefault(objAttribute => objAttribute is GuidAttribute);
                if (attrib != null)
                {
                    return ((GuidAttribute)attrib).Value;
                }
                else
                {
                    return null;
                }
            }
        }
    }


}