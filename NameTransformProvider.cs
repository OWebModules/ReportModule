﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using ReportModule.Models;
using ReportModule.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ReportModule
{
    public class NameTransformProvider : INameTransform
    {
        private Globals globals;
        public bool IsReferenceCompatible => false;

        public bool IsAspNetProject { get; set; }

        public bool IsResponsible(string idClass)
        {
            return idClass == NameTransform.Config.LocalData.Class_NameTransform_Provider__Reports_.GUID;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> TransformNames(List<clsOntologyItem> items, bool encodeName = true, string idClass = null, IMessageOutput messageOutput = null)
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(async() =>
           {
               var result = new ResultItem<List<clsOntologyItem>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = items
               };

               if (!items.All(obj => obj.GUID_Parent == NameTransform.Config.LocalData.Class_NameTransform_Provider__Reports_.GUID))
               {
                   result.ResultState = globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "There are objects of not allowed classes contained!";
                   messageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               var elasticAgent = new ServiceAgentElasticNameTransform(globals);

               messageOutput?.OutputInfo("Get configuration-model...");

               var modelResult = await elasticAgent.GetNameTransformProviderModel(items);
               result.ResultState = modelResult.ResultState;

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   messageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               messageOutput?.OutputInfo("Have configuration-model.");

               var transferConfiguration = (from config in modelResult.Result.NameTransformConfigs
                                            join report in modelResult.Result.ConfigToReports on config.GUID equals report.ID_Object
                                            join rowTemplate in modelResult.Result.RowTemplate on report.ID_Object equals rowTemplate.ID_Object into rowTemplates
                                            from rowTemplate in rowTemplates.DefaultIfEmpty()
                                            join tableTemplate in modelResult.Result.TableTemplates on report.ID_Object equals tableTemplate.ID_Object into tableTemplates
                                            from tableTemplate in tableTemplates.DefaultIfEmpty()
                                            join style in modelResult.Result.Style on report.ID_Object equals style.ID_Object into styles
                                            from style in styles.DefaultIfEmpty()
                                            join reportFilter in modelResult.Result.ConfigToReportFilters on report.ID_Object equals reportFilter.ID_Object into reportFilters
                                            from reportFilter in reportFilters.DefaultIfEmpty()
                                            join reportSort in modelResult.Result.ConfigToReportSorts on report.ID_Object equals reportSort.ID_Object into reportSorts
                                            from reportSort in reportSorts.DefaultIfEmpty()
                                            select new
                                            {
                                                config,
                                                report = new clsOntologyItem
                                                {
                                                    GUID = report.ID_Other,
                                                    Name = report.Name_Other,
                                                    GUID_Parent = report.ID_Parent_Other,
                                                    Type = report.Ontology
                                                },
                                                rowTemplate,
                                                tableTemplate,
                                                style,
                                                reportFilter,
                                                reportSort
                                            }).ToList();


               var reportController = new ReportController(globals, IsAspNetProject);

               messageOutput?.OutputInfo("Get filter-items...");
               var getFilterItemsRequest = new GetFilterItemsRequest(FilterRequestType.GetFiltersByFilterOItems)
               {
                   FilterOItems = modelResult.Result.ConfigToReportFilters.Select(configToFilter => new clsOntologyItem
                   {
                       GUID = configToFilter.ID_Other,
                       Name = configToFilter.Name_Other,
                       GUID_Parent = configToFilter.ID_Parent_Other,
                       Type = configToFilter.Ontology
                   }).ToList()
               };
               getFilterItemsRequest.MessageOutput = messageOutput;

               var getFilterItemsResult = await reportController.GetFilterItems(getFilterItemsRequest);
               result.ResultState = getFilterItemsResult.ResultState;

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   messageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               messageOutput?.OutputInfo("Have filter-items.");

               messageOutput?.OutputInfo("Get Sort-items...");
               var getSortItemsRequest = new GetSortItemsRequest(modelResult.Result.ConfigToReportSorts.Select(configToSort => new clsOntologyItem
               {
                   GUID = configToSort.ID_Other,
                   Name = configToSort.Name_Other,
                   GUID_Parent = configToSort.ID_Parent_Other,
                   Type = configToSort.Ontology
               }).ToList());
               getSortItemsRequest.MessageOutput = messageOutput;

               var getSortItemsResult = await reportController.GetSortItems(getSortItemsRequest);
               result.ResultState = getSortItemsResult.ResultState;

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   messageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               messageOutput?.OutputInfo("Have Sort-items.");

               messageOutput?.OutputInfo("Get table...");

               foreach (var config in transferConfiguration)
               {
                   var reportResult = await reportController.GetReport(config.report);
                   string filter = null;
                   if (config.reportFilter != null)
                   {
                       var filterItem = getFilterItemsResult.Result.FirstOrDefault(filt => filt.FilterItem.GUID == config.reportFilter.ID_Other);
                       if (filterItem == null)
                       {
                           result.ResultState = globals.LState_Error.Clone();
                           result.ResultState.Additional1 = "Filter-Item cannot be found!";
                           messageOutput?.OutputError(result.ResultState.Additional1);
                           return result;
                       }

                       filter = filterItem.Value.Val_String;
                   }

                   string sort = null;
                   if (config.reportSort != null)
                   {
                       var sortItem = getSortItemsResult.Result.FirstOrDefault(filt => filt.SortItem.GUID == config.reportSort.ID_Other);
                       if (sortItem == null)
                       {
                           result.ResultState = globals.LState_Error.Clone();
                           result.ResultState.Additional1 = "Sort-Item cannot be found!";
                           messageOutput?.OutputError(result.ResultState.Additional1);
                           return result;
                       }

                       sort = sortItem.Value.Val_String;
                   }

                   var reportDataResult = await reportController.LoadData(reportResult.Result.Report, reportResult.Result.ReportFields, filter, sort);
                   result.ResultState = reportDataResult.ResultState;
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       messageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   var sbData = new StringBuilder();
                   var rowTemplate = config.rowTemplate?.Val_String ?? "";

                   messageOutput?.OutputInfo("Create table...");
                   foreach (var row in reportDataResult.Result)
                   {
                       var createLine = true;
                       if (!string.IsNullOrEmpty(rowTemplate))
                       {
                           createLine = false;
                       }
                       var line = rowTemplate;
                       foreach (var reportField in reportResult.Result.ReportFields)
                       {
                           if (row.ContainsKey(reportField.NameCol))
                           {
                                var value = HttpUtility.HtmlEncode(row[reportField.NameCol]?.ToString() ?? "");
                               if (createLine)
                               {
                                   line += $"<td>{value}</td>";
                               }
                               else
                               {
                                   line = line.Replace($"@{reportField.NameReportField}@", value);
                               }
                               
                           }
                       }
                       sbData.AppendLine(line);
                   }

                   var tableTemplate = "<table>@ROWS@</table>";
                   if (config.tableTemplate != null)
                   {
                       tableTemplate = config.tableTemplate?.Val_String ?? "";
                   }

                   var tableString = tableTemplate.Replace("@ROWS@", sbData.ToString());

                   if (encodeName)
                   {
                       config.config.Name = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(tableString));
                   }
                   else
                   {
                       config.config.Name = tableString;
                   }
               }

               messageOutput?.OutputInfo("Have table.");

               return result;
           });

            return taskResult;
        }

        public NameTransformProvider(Globals globals)
        {
            this.globals = globals;
        }
    }
}
