﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReportModule.Models;

namespace ReportModule.Factories
{
    public class AutoCompleteFactory
    {
        private Globals globals;
        public async Task<ResultItem<AutoCompleteItem>> GetAutoCompleteFilter(List<ReportFilterItem> filterItems, List<ReportField> reportFields, string query)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<AutoCompleteItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                result.Result = new AutoCompleteItem
                {
                    query = query,
                    suggestions = (from pattern in filterItems
                                   where pattern.Value.Val_String.ToLower().Contains(query.ToLower())
                                   select new AutoCompleteSubItem
                                   {
                                       value = pattern.Value.Val_String.StartsWith(pattern.FilterItem.Name) ?  pattern.Value.Val_String : $"{pattern.FilterItem.Name}: {pattern.Value.Val_String}",
                                       data = pattern.Value.ID_Object,
                                       filter = pattern.Value.Val_String
                                   }).ToList()
                };

                if (reportFields.Any())
                    result.Result.suggestions.InsertRange(0, reportFields.Where(repField => repField.NameCol.ToLower().Contains(query.ToLower())).Select(repField => new AutoCompleteSubItem
                    {
                        value = repField.NameReportField,
                        data = repField.IdReportField,
                        filter = repField.NameCol
                    }));

                return result;
            });

            return taskResult;
        }

        public AutoCompleteFactory(Globals globals)
        {
            this.globals = globals;
        }
    }
}
