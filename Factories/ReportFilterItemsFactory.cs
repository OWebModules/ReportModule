﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using ReportModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Factories
{
    public static class ReportFilterItemsFactory
    {
        public static async Task<ResultItem<List<ReportFilterItem>>> CreateReportFilterItems(ReportFilterItemModel model, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<List<ReportFilterItem>>>(() =>
            {
                var result = new ResultItem<List<ReportFilterItem>>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                result.Result = (from filterItem in model.FilterItems
                                 join value in model.FilterItemValues on filterItem.GUID equals value.ID_Object
                                 join report in model.FilterItemsToReports on filterItem.GUID equals report.ID_Object
                                 join standard in model.FilterItemStandard on filterItem.GUID equals standard.ID_Object into standards
                                 from standard in standards.DefaultIfEmpty()
                                 select new ReportFilterItem
                                 {
                                     FilterItem = filterItem,
                                     Value = value,
                                     FilterItemToReport = report,
                                     Standard = standard
                                 }).ToList();

                return result;
            });
            return taskResult;
        }
    }
}
