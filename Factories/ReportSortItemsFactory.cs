﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using ReportModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Factories
{
    public static class ReportSortItemsFactory
    {
        public static async Task<ResultItem<List<ReportSortItem>>> CreateReportSortItems(ReportSortItemModel model, Globals globals)
        {
            var taskResult = await Task.Run<ResultItem<List<ReportSortItem>>>(() =>
            {
                var result = new ResultItem<List<ReportSortItem>>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                result.Result = (from SortItem in model.SortItems
                                 join value in model.SortItemValues on SortItem.GUID equals value.ID_Object
                                 join report in model.SortItemsToReports on SortItem.GUID equals report.ID_Object
                                 select new ReportSortItem
                                 {
                                     SortItem = SortItem,
                                     Value = value,
                                     SortItemToReport = report
                                 }).ToList();

                return result;
            });
            return taskResult;
        }
    }
}
