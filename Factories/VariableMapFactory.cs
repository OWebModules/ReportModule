﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using ReportModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Factories
{
    public class VariableMapFactory
    {
        private Globals globals;
        public async Task<ResultItem<List<VariableToField>>> CreateVariableToFieldMaps(VariableToFieldModel model)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<List<VariableToField>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<VariableToField>()
                };

                result.Result = (from variableFieldMap in model.VariableToFieldMaps
                                 join variable in model.MapsToVariables on variableFieldMap.GUID equals variable.ID_Object
                                 join field in model.MapsToFields on variableFieldMap.GUID equals field.ID_Object
                                 select new VariableToField
                                 {
                                     IdVariableToFieldMap = variableFieldMap.GUID,
                                     NameVariableToFieldMap = variableFieldMap.Name,
                                     IdReportField = field.ID_Other,
                                     NameReportField = field.Name_Other,
                                     IdVariable = variable.ID_Other,
                                     NameVariable = variable.Name_Other
                                 }).ToList();

                return result;
            });

            return taskResult;
        }

        public VariableMapFactory(Globals globals)
        {
            this.globals = globals;
        }
    }
}
