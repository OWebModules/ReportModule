﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using ReportModule.Models;
using ReportModule.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ReportModule.Factories
{
    public class ReportViewFactory : NotifyPropertyChange
    {
        private Globals globals;

        public async Task<ResultItem<List<ReportField>>> CreateReportFieldList(ReportFieldResult reportFields)
        {
            var reportFieldItems = reportFields;
            var taskResult = await Task.Run<ResultItem<List<ReportField>>>(() =>
             {
                 var result = new ResultItem<List<ReportField>>
                 {
                     ResultState = globals.LState_Success.Clone()
                 };

                 var colsOfFields = (from fieldRel in reportFieldItems.ReportFieldsOfReport
                                     join colRel in reportFieldItems.ReportFieldsToColumns on fieldRel.ID_Object equals colRel.ID_Object
                                     select new
                                     {
                                         IdField = fieldRel.ID_Object,
                                         IdCol = colRel.ID_Other,
                                         NameCol = colRel.Name_Other
                                     });

                 var dbViewsOfCols = (from colRel in colsOfFields
                                      join dbViewOfColRel in reportFieldItems.ColumnToViews on colRel.IdCol equals dbViewOfColRel.ID_Object
                                      select new
                                      {
                                          IdField = colRel.IdField,
                                          IdCol = colRel.IdCol,
                                          IdView = dbViewOfColRel.ID_Other,
                                          NameView = dbViewOfColRel.Name_Other
                                      });

                 var dbViewsOfReport = (from fieldRel in reportFieldItems.ReportFieldsOfReport
                                        join viewRel in reportFieldItems.ReportToViews on fieldRel.ID_Other equals viewRel.ID_Object
                                        select new
                                        {
                                            IdReport = fieldRel.ID_Other,
                                            IdView = viewRel.ID_Other,
                                            NameView = viewRel.Name_Other
                                        }).GroupBy(view => new { IdReport = view.IdReport, IdView = view.IdView, NameView = view.NameView });

                 var dbOnServersOfReports = (from viewRel in dbViewsOfReport
                                             join dbOnServerRel in reportFieldItems.DatabaseOnServerToViews on viewRel.Key.IdView equals dbOnServerRel.ID_Other
                                             select new
                                             {
                                                 IdReport = viewRel.Key.IdReport,
                                                 IdView = viewRel.Key.IdView,
                                                 IdDBOnServer = dbOnServerRel.ID_Object,
                                                 NameDBOnServer = dbOnServerRel.Name_Object
                                             });

                 var serverOfReports = (from dbOnServerRel in dbOnServersOfReports
                                        join serverRel in reportFieldItems.DBOnServerToServers on dbOnServerRel.IdDBOnServer equals serverRel.ID_Object
                                        select new
                                        {
                                            IdField = dbOnServerRel.IdView,
                                            IdView = dbOnServerRel.IdView,
                                            IdDBOnServer = dbOnServerRel.IdDBOnServer,
                                            IdServer = serverRel.ID_Other,
                                            NameServer = serverRel.Name_Other
                                        });

                 var databaseOfReports = (from dbOnServerRel in dbOnServersOfReports
                                          join databaseRel in reportFieldItems.DBOnServerToDatabases on dbOnServerRel.IdDBOnServer equals databaseRel.ID_Object
                                          select new
                                          {
                                              IdField = dbOnServerRel.IdView,
                                              IdView = dbOnServerRel.IdView,
                                              IdDBOnServer = dbOnServerRel.IdDBOnServer,
                                              IdDatabase = databaseRel.ID_Other,
                                              NameDatabase = databaseRel.Name_Other
                                          });

                 var instancesOfReports = (from dbOnServerRel in dbOnServersOfReports
                                           join instanceRel in reportFieldItems.DBOnServerToInstance on dbOnServerRel.IdDBOnServer equals instanceRel.ID_Object
                                           select new
                                           {
                                               IdField = dbOnServerRel.IdView,
                                               IdView = dbOnServerRel.IdView,
                                               IdDBOnServer = dbOnServerRel.IdDBOnServer,
                                               IdInstance = instanceRel.ID_Other,
                                               NameInstance = instanceRel.Name_Other
                                           });

                 var fieldTypesOfFields = (from fieldRel in reportFieldItems.ReportFieldsOfReport
                                           join fieldType in reportFieldItems.FieldsToFieldTypes on fieldRel.ID_Object equals fieldType.ID_Object
                                           select new
                                           {
                                               IdField = fieldRel.ID_Object,
                                               IdFieldType = fieldType.ID_Other,
                                               NameFieldType = fieldType.Name_Other
                                           });

                 var fieldFormatsOfFields = (from fieldRel in reportFieldItems.ReportFieldsOfReport
                                             join fieldFormat in reportFieldItems.FieldsToFieldFormats on fieldRel.ID_Object equals fieldFormat.ID_Object
                                             select new
                                             {
                                                 IdField = fieldRel.ID_Object,
                                                 IdFieldFormat = fieldFormat.ID_Other,
                                                 NameFieldFormat = fieldFormat.Name_Other
                                             });

                 var leadFieldsOfFields = (from fieldRel in reportFieldItems.ReportFieldsOfReport
                                           join leadFieldRel in reportFieldItems.LeadFieldsToFields on fieldRel.ID_Object equals leadFieldRel.ID_Other
                                           select new
                                           {
                                               IdField = fieldRel.ID_Object,
                                               IdLeadField = leadFieldRel.ID_Object,
                                               NameLeadField = leadFieldRel.Name_Object
                                           });

                 var typeFieldsOfFields = (from fieldRel in reportFieldItems.ReportFieldsOfReport
                                           join typeFieldRel in reportFieldItems.TypeFieldToFields on fieldRel.ID_Object equals typeFieldRel.ID_Object
                                           join leadFieldRel in leadFieldsOfFields on fieldRel.ID_Object equals leadFieldRel.IdField
                                           where typeFieldRel.ID_RelationType == Config.LocalData.RelationType_Type_Field.GUID
                                             && fieldRel.ID_RelationType == Config.LocalData.RelationType_belongs_to.GUID
                                           select new
                                           {
                                               IdField = fieldRel.ID_Object,
                                               IdTypeField = typeFieldRel.ID_Other,
                                               NameTypeField = typeFieldRel.Name_Other
                                           });

                 var aggregates = (from aggregate in reportFieldItems.ReportToAggregates
                                   join aggregateField in reportFieldItems.AggregateFields on aggregate.ID_Object equals aggregateField.ID_Object
                                   join aggregateType in reportFieldItems.AggregateTypes on aggregateField.ID_Object equals aggregateType.ID_Object
                                   select new
                                   {
                                       IdField = aggregateField.ID_Other,
                                       IdAggregateType = aggregateType.ID_Other,
                                       AggregateType = aggregateType.Name_Other
                                   });

                 var templates = (from templateToField in reportFieldItems.Templates
                                  join templateToHTML in reportFieldItems.TemplateAttributes on templateToField.ID_Object equals templateToHTML.ID_Object
                                  select new
                                  {
                                      IdField = templateToField.ID_Other,
                                      IdTemplate = templateToField.ID_Object,
                                      IdHTMLAttribute = templateToHTML.ID_Attribute,
                                      Template = templateToHTML.Val_String
                                  });

                 result.Result = (from fieldRel in reportFieldItems.ReportFieldsOfReport
                                     join inVisibility in reportFieldItems.ReportFieldsToInvisible on fieldRel.ID_Object equals inVisibility.ID_Object
                                     join doNotEncode in reportFieldItems.ReportFieldsToNoEncode on fieldRel.ID_Object equals doNotEncode.ID_Object into doNotEncodes
                                     from doNotEncode in doNotEncodes.DefaultIfEmpty()
                                     join viewRel in dbViewsOfReport on fieldRel.ID_Other equals viewRel.Key.IdReport
                                     join colRel in colsOfFields on fieldRel.ID_Object equals colRel.IdField
                                     join colViewrel in dbViewsOfCols on new { IdCol = colRel.IdCol, IdView = viewRel.Key.IdView } equals new { IdCol = colViewrel.IdCol, IdView = colViewrel.IdView }
                                     join dbOnServerRel in dbOnServersOfReports on fieldRel.ID_Other equals dbOnServerRel.IdReport
                                     join databaseRel in databaseOfReports on dbOnServerRel.IdDBOnServer equals databaseRel.IdDBOnServer
                                     join instanceRel in instancesOfReports on dbOnServerRel.IdDBOnServer equals instanceRel.IdDBOnServer into instancesRel
                                     from instanceRel in instancesRel.DefaultIfEmpty()
                                     join serverRel in serverOfReports on dbOnServerRel.IdDBOnServer equals serverRel.IdDBOnServer
                                     join fieldType in fieldTypesOfFields on fieldRel.ID_Object equals fieldType.IdField
                                     join fieldFormat in fieldFormatsOfFields on fieldRel.ID_Object equals fieldFormat.IdField into fieldFormats
                                     from fieldFormat in fieldFormats.DefaultIfEmpty()
                                     join leadField in leadFieldsOfFields on fieldRel.ID_Object equals leadField.IdField into leadFields
                                     from leadField in leadFields.DefaultIfEmpty()
                                     join typeField in typeFieldsOfFields on fieldRel.ID_Object equals typeField.IdField into typeFields
                                     from typeField in typeFields.DefaultIfEmpty()
                                     join aggregate in aggregates on fieldRel.ID_Object equals aggregate.IdField into aggregates1
                                     from aggregate in aggregates1.DefaultIfEmpty()
                                     join template in templates on fieldRel.ID_Object equals template.IdField into templates1
                                     from template in templates1.DefaultIfEmpty()
                                     join width in reportFieldItems.ReportFieldsToWidths on fieldRel.ID_Object equals width.ID_Object into widths
                                     from width in widths.DefaultIfEmpty()
                                     select new ReportField
                                     {
                                         IdReport = fieldRel.ID_Other,
                                         NameReport = fieldRel.Name_Other,
                                         IdReportField = fieldRel.ID_Object,
                                         NameReportField = fieldRel.Name_Object,
                                         IdAttributeVisible = inVisibility.ID_Attribute,
                                         IsVisible = !inVisibility.Val_Bit.Value,
                                         DoNotEncode = doNotEncode?.Val_Bit.Value ?? false,
                                         IdCol = colRel.IdCol,
                                         NameCol = colRel.NameCol,
                                         IdDatabase = databaseRel.IdDatabase,
                                         NameDatabase = databaseRel.NameDatabase,
                                         IdInstance = instanceRel?.IdInstance,
                                         NameInstance = instanceRel?.NameInstance,
                                         IdFieldType = fieldType.IdFieldType,
                                         NameFieldType = fieldType.NameFieldType,
                                         IdDBOnServer = dbOnServerRel.IdDBOnServer,
                                         NameDBOnServer = dbOnServerRel.NameDBOnServer,
                                         IdDBView = colViewrel.IdView,
                                         NameDBView = colViewrel.NameView,
                                         IdFieldFormat = fieldFormat != null ? fieldFormat.IdFieldFormat : null,
                                         NameFieldFormat = fieldFormat != null ? fieldFormat.NameFieldFormat : null,
                                         IdLeadField = leadField != null ? leadField.IdLeadField : null,
                                         NameLeadField = leadField != null ? leadField.NameLeadField : null,
                                         IdRow = Guid.NewGuid().ToString(),
                                         IdServer = serverRel.IdServer,
                                         NameServer = serverRel.NameServer,
                                         IdTypeField = typeField != null ? typeField.IdTypeField : null,
                                         NameTypeField = typeField != null ? typeField.NameTypeField : null,
                                         IdView = viewRel.Key.IdView,
                                         NameView = viewRel.Key.NameView,
                                         OrderId = fieldRel.OrderID.Value,
                                         IdAggregateType = aggregate != null ? aggregate.IdAggregateType : null,
                                         AggregateType = aggregate != null ? aggregate.AggregateType : null,
                                         IdTemplate = template?.IdTemplate,
                                         IdAttributeTemplate = template?.IdHTMLAttribute,
                                         Template = template?.Template,
                                         IdWidth = width?.ID_Other,
                                         Width = width?.Name_Other
                                     }).ToList();

                 return result;
             });


            return taskResult;
        }

        public ReportViewFactory(Globals globals)
        {
            this.globals = globals;
        }

        public async Task<ResultItem<ReportItem>> GetReport(ReportFieldResult reportFields)
        {
            var taskResult = await Task.Run<ResultItem<ReportItem>>(async () =>
            {
                var result = new ResultItem<ReportItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var oItemReport = reportFields.Reports.FirstOrDefault();
                if (oItemReport != null && oItemReport.GUID_Parent != Config.LocalData.Class_Reports.GUID)
                {
                    var resultGetReport = await GetReport(oItemReport);
                    if (result.ResultState.GUID == globals.LState_Error.GUID || result.ResultState.GUID == globals.LState_Nothing.GUID)
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        return result;
                    }
                    oItemReport = resultGetReport.Result;
                }

                var reportsPre = from reportToView in reportFields.ReportToViews
                                 join reportType in reportFields.ReportToReportTypes on reportToView.ID_Object equals reportType.ID_Object
                                 join dbOnServerToView in reportFields.DatabaseOnServerToViews on reportToView.ID_Other equals dbOnServerToView.ID_Other
                                 join dbOnServerToDatabase in reportFields.DBOnServerToDatabases on dbOnServerToView.ID_Object equals dbOnServerToDatabase.ID_Object
                                 join dbOnServerToServer in reportFields.DBOnServerToServers on dbOnServerToView.ID_Object equals dbOnServerToServer.ID_Object
                                 join dbOnServerToInstance in reportFields.DBOnServerToInstance on dbOnServerToView.ID_Object equals dbOnServerToInstance.ID_Object into dbOnServerToInstances
                                 from dbOnServerToInstance in dbOnServerToInstances.DefaultIfEmpty()
                                 select new { reportToView, reportType, dbOnServerToView, dbOnServerToDatabase, dbOnServerToServer, dbOnServerToInstance };
                  var reports = (from reportItem in reportFields.Reports.Where(rep => rep.GUID == oItemReport.GUID)
                               join reportPre in reportsPre on reportItem.GUID equals reportPre.reportToView.ID_Object into reportPres
                               from reportPre in reportPres.DefaultIfEmpty()
                               select new ReportItem
                               {
                                   IdReport = reportItem.GUID,
                                   NameReport = reportItem.Name,
                                   IdReportType = reportPre != null ? reportPre.reportType?.ID_Other : null,
                                   NameReportType = reportPre != null ? reportPre.reportType?.Name_Other : null,
                                   IdDBViewOrEsType = reportPre != null ? reportPre.dbOnServerToView?.ID_Other : null,
                                   NameDBViewOrEsType = reportPre != null ? reportPre.dbOnServerToView?.Name_Other : null,
                                   IdDatabaseOnServer = reportPre != null ? reportPre.dbOnServerToView?.ID_Object : null,
                                   NameDatabaseOnServer = reportPre != null ? reportPre.dbOnServerToView?.Name_Object : null,
                                   IdServer = reportPre != null ? reportPre.dbOnServerToServer?.ID_Other : null,
                                   NameServer = reportPre != null ? reportPre.dbOnServerToServer?.Name_Other : null,
                                   IdDatabaseOrIndex = reportPre != null ? reportPre.dbOnServerToDatabase?.ID_Other : null,
                                   NameDatabaseOrIndex = reportPre != null ? reportPre.dbOnServerToDatabase?.Name_Other : null,
                                   IdInstance = reportPre != null ? reportPre.dbOnServerToInstance?.ID_Other : null,
                                   NameInstance = reportPre != null ? reportPre.dbOnServerToInstance?.Name_Other : null,
                                   IdRow = Guid.NewGuid().ToString()
                               }).ToList();

                result.Result = reports.FirstOrDefault();
                return result;
            });


            return taskResult;
        }

        private async Task<ResultItem<clsOntologyItem>> GetReport(clsOntologyItem oItemRef)
        {

            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchReportLeftRight = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = oItemRef.GUID,
                    ID_Parent_Other = Config.LocalData.Class_Reports.GUID
                }
            };

                var dbReaderLeftRight = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderLeftRight.GetDataObjectRel(searchReportLeftRight);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                if (dbReaderLeftRight.ObjectRels.Any())
                {
                    result.Result = dbReaderLeftRight.ObjectRels.Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = globals.Type_Object
                    }).First();

                    return result;
                }


                var searchReportRightLeft = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = oItemRef.GUID,
                    ID_Parent_Object = Config.LocalData.Class_Reports.GUID
                }
            };

                var dbReaderRightLeft = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderRightLeft.GetDataObjectRel(searchReportRightLeft);
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                if (!dbReaderRightLeft.ObjectRels.Any())
                {
                    result.ResultState = globals.LState_Nothing.Clone();
                    return result;
                };

                result.Result = dbReaderRightLeft.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Object,
                    Name = rel.Name_Object,
                    GUID_Parent = rel.ID_Parent_Object,
                    Type = globals.Type_Object
                }).First();

                return result;
            });

            return taskResult;
        }


    }

}
