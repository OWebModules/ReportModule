﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using OntoWebCore.Attributes;
using OntoWebCore.Converter;
using OntoWebCore.Models;
using ReportModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Factories
{
    public class ReportGridFactory
    {
        private Globals globals;

        public async Task<ResultItem<Dictionary<string, object>>> CreateGridConfig(List<ReportField> reportFields, string getDataUrl, List<AdditionalReportField> additionalFields)
        {
            var taskResult = await Task.Run<ResultItem<Dictionary<string, object>>>(() =>
            {
                var result = new ResultItem<Dictionary<string, object>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new Dictionary<string, object>()
                };

                result.Result.Add("groupable", true);
                
                result.Result.Add("autobind", false);
                result.Result.Add("height", "100%");
                result.Result.Add("scrollable", true);
                result.Result.Add("resizable", true);
                result.Result.Add("selectable", "multiple cell");
                result.Result.Add("allowCopy", true);
                result.Result.Add("columnMenu", true);
                result.Result.Add("editable", false);

                var sortableDict = new Dictionary<string, object>();
                sortableDict.Add("mode", "multiple");
                sortableDict.Add("allowUnsort", true);
                sortableDict.Add("showIndexes", true);
                result.Result.Add("sortable", sortableDict);

                var pageConfig = new Dictionary<string, object>();
                pageConfig.Add("refresh", true);

                var pageSizes = new List<object>
                {
                    5,10,50,100,1000
                };
                pageConfig.Add("pageSizes", pageSizes);
                pageConfig.Add("numeric", false);

                var messagesConfig = new Dictionary<string, object>();
                messagesConfig.Add("display", "Showing {0}-{1} from {2} data items");

                pageConfig.Add("messages", messagesConfig);

                result.Result.Add("pageable", pageConfig);

                var filterConfig = new Dictionary<string, object>();
                filterConfig.Add("extra", true);

                var operatorsConfig = new Dictionary<string, object>();
                var stringConfig = new Dictionary<string, object>();
                stringConfig.Add("contains", "Contains");
                stringConfig.Add("startswith", "Starts with");
                stringConfig.Add("eq", "Is equal to");
                stringConfig.Add("neq", "Is not equal to");
                stringConfig.Add("isnull", "Is null");
                stringConfig.Add("isnotnull", "Is not null");
                stringConfig.Add("isempty", "Is Empty");
                operatorsConfig.Add("string", stringConfig);


                var numberConfig = new Dictionary<string, object>();
                numberConfig.Add("eq", "Is equal to");
                numberConfig.Add("neq", "Is not equal to");
                numberConfig.Add("isnotnull", "Is not null");
                numberConfig.Add("isempty", "Is Empty");
                numberConfig.Add("gt", "Greater");
                numberConfig.Add("lt", "Smaller");
                operatorsConfig.Add("number", numberConfig);

                var dateConfig = new Dictionary<string, object>();
                dateConfig.Add("eq", "Is equal to");
                dateConfig.Add("neq", "Is not equal to");
                dateConfig.Add("isnotnull", "Is not null");
                dateConfig.Add("isempty", "Is Empty");
                dateConfig.Add("gt", "Later");
                dateConfig.Add("lt", "Former");
                operatorsConfig.Add("date", dateConfig);

                filterConfig.Add("operators", operatorsConfig);

                result.Result.Add("filterable", filterConfig);

                var dictSchema = new Dictionary<string, object>();
                var dictModel = new Dictionary<string, object>();
                var dictFields = new Dictionary<string, object>();

                dictSchema.Add("model", dictModel);
                dictModel.Add("fields", dictFields);

                var columnList = reportFields.Select(field =>
                {
                    var dictColumnAttribute = new Dictionary<string, object>();

                    dictColumnAttribute.Add(nameof(KendoColumnAttribute.field), field.NameCol);
                    dictColumnAttribute.Add(nameof(KendoColumnAttribute.hidden), !field.IsVisible);
                    dictColumnAttribute.Add(nameof(KendoColumnAttribute.title), field.NameReportField);
                    dictColumnAttribute.Add(nameof(KendoColumnAttribute.Order), (int)field.OrderId);
                    dictColumnAttribute.Add(nameof(KendoColumnAttribute.lockable), true);
                    dictColumnAttribute.Add(nameof(KendoColumnAttribute.locked), false);


                    var dictField = new Dictionary<string, object>();
                    dictFields.Add(field.NameCol, dictField);
                    dictField.Add("editable", false);

                    if (!string.IsNullOrEmpty(field.Template))
                    {
                        var dictFilterable = new Dictionary<string, object>();
                        if (field.NameFieldType == "Zahl")
                        {

                            dictField.Add("type", "number");
                        }
                        else if (field.NameFieldType == "Bit")
                        {
                            dictField.Add("type", "boolean");
                        }
                        else
                        {
                            dictFilterable.Add(nameof(KendoColumnAttribute.multi), true);
                            dictFilterable.Add(nameof(KendoColumnAttribute.search), true);
                            dictField.Add("type", "string");
                        }


                        dictColumnAttribute.Add(nameof(KendoColumnAttribute.filterable), dictFilterable);
                        dictColumnAttribute.Add(nameof(KendoColumnAttribute.template), field.Template);
                    }
                    else
                    {
                        if (field.NameFieldType == "DateTime")
                        {
                            var dictFilterable = new Dictionary<string, object>();
                            dictFilterable.Add("ui", "datetimepicker");
                            dictColumnAttribute.Add(nameof(KendoColumnAttribute.filterable), dictFilterable);
                            dictColumnAttribute.Add(nameof(KendoColumnAttribute.type), ColType.DateType);
                            dictColumnAttribute.Add(nameof(KendoColumnAttribute.template), $"#= ({field.NameCol} == null) ? '' : kendo.toString(kendo.parseDate({field.NameCol}, 'yyyy-MM-ddTHH:mm:ss'),'{field.NameFieldFormat}')#");
                            dictField.Add("type", "date");
                        }
                        else
                        {
                            var dictFilterable = new Dictionary<string, object>();
                            if (field.NameFieldType == "Zahl")
                            {

                                dictField.Add("type", "number");
                            }
                            else if (field.NameFieldType == "Bit")
                            {
                                dictField.Add("type", "boolean");
                            }
                            else
                            {
                                dictFilterable.Add(nameof(KendoColumnAttribute.multi), true);
                                dictFilterable.Add(nameof(KendoColumnAttribute.search), true);
                                dictField.Add("type", "string");
                            }


                            dictColumnAttribute.Add(nameof(KendoColumnAttribute.filterable), dictFilterable);
                            dictColumnAttribute.Add(nameof(KendoColumnAttribute.template), $"#= ({field.NameCol} == null) ? '' : {field.NameCol} #");

                        }
                    }

                    if (!string.IsNullOrEmpty(field.NameFieldFormat))
                    {
                        dictColumnAttribute.Add(nameof(KendoColumnAttribute.format), "{0:" + field.NameFieldFormat + "}");
                    }
                    if (!string.IsNullOrEmpty(field.AggregateType))
                    {
                        dictColumnAttribute.Add(nameof(KendoColumnAttribute.aggregates), new string[] { field.AggregateType });
                        if (!string.IsNullOrEmpty(field.NameFieldFormat))
                        {
                            dictColumnAttribute.Add(nameof(KendoColumnAttribute.footerTemplate), field.AggregateType + ": #=kendo.toString(" + field.AggregateType + ",'" + field.NameFieldFormat + "') #");
                        }
                        else
                        {
                            dictColumnAttribute.Add(nameof(KendoColumnAttribute.footerTemplate), field.AggregateType + ": #=" + field.AggregateType + "#");
                        }

                    }

                    if (!string.IsNullOrEmpty(field.Width))
                    {
                        dictColumnAttribute.Add(nameof(KendoColumnAttribute.width), field.Width);
                    }
                    else
                    {
                        dictColumnAttribute.Add(nameof(KendoColumnAttribute.width), "150px");
                    }
                    return dictColumnAttribute;

                }).OrderBy(field => field[nameof(KendoColumnAttribute.Order)]).ToList();

                if (additionalFields != null && additionalFields.Any())
                {
                    foreach (var additionalField in additionalFields)
                    {
                        var dictField = new Dictionary<string, object>();
                        var fieldName = additionalField.Name;
                        var fieldTitle = additionalField.Title;
                        dictFields.Add(fieldName, dictField);
                        dictField.Add("editable", false);
                        var dataType = additionalField.DataType;
                        var template = additionalField.Template;
                        var first = additionalField.First;
                        var hidden = additionalField.Hidden;
                        var width = additionalField.Width;
                        
                        var dictColumnAttribute = new Dictionary<string, object>();
                        dictColumnAttribute.Add(nameof(KendoColumnAttribute.field), fieldName);
                        dictColumnAttribute.Add(nameof(KendoColumnAttribute.hidden), hidden);
                        dictColumnAttribute.Add(nameof(KendoColumnAttribute.title), fieldTitle);
                        dictColumnAttribute.Add(nameof(KendoColumnAttribute.Order), 0);
                        dictColumnAttribute.Add(nameof(KendoColumnAttribute.width), string.IsNullOrEmpty(width) ? "150px" : width);
                        dictColumnAttribute.Add(nameof(KendoColumnAttribute.template), template);

                        if (additionalField.GroupCell)
                        {
                            var dictAttribute = new Dictionary<string, string>();
                            dictAttribute.Add("class", "k-group-cell");
                            dictColumnAttribute.Add(nameof(KendoColumnAttribute.attributes), dictAttribute);
                        }

                        if (dataType == "DateTime")
                        {
                            var dictFilterable = new Dictionary<string, object>();
                            dictFilterable.Add("ui", "datetimepicker");
                            dictColumnAttribute.Add(nameof(KendoColumnAttribute.filterable), dictFilterable);
                            dictColumnAttribute.Add(nameof(KendoColumnAttribute.type), ColType.DateType);
                            dictField.Add("type", "date");
                        }
                        else
                        {
                            var dictFilterable = new Dictionary<string, object>();
                            if (dataType == "Zahl")
                            {

                                dictField.Add("type", "number");
                            }
                            else if (dataType == "Bit")
                            {
                                dictField.Add("type", "boolean");
                            }
                            else
                            {
                                dictFilterable.Add(nameof(KendoColumnAttribute.multi), true);
                                dictFilterable.Add(nameof(KendoColumnAttribute.search), true);
                                dictField.Add("type", "string");
                            }

                            dictColumnAttribute.Add(nameof(KendoColumnAttribute.filterable), dictFilterable);
                        }


                        if (first)
                        {
                            columnList.Insert(0, dictColumnAttribute);
                        }
                        else
                        {
                            columnList.Add(dictColumnAttribute);
                        }
                    }
                }

                var dictRowNoDict = new Dictionary<string, object>();
                dictRowNoDict.Add(nameof(KendoColumnAttribute.hidden), false);
                dictRowNoDict.Add(nameof(KendoColumnAttribute.title), "#");
                dictRowNoDict.Add(nameof(KendoColumnAttribute.Order), -1);
                dictRowNoDict.Add(nameof(KendoColumnAttribute.lockable), true);
                dictRowNoDict.Add(nameof(KendoColumnAttribute.locked), false);
                dictRowNoDict.Add(nameof(KendoColumnAttribute.template), "#= ++reportRecord #");
                dictRowNoDict.Add(nameof(KendoColumnAttribute.width), 50);
                columnList.Insert(0, dictRowNoDict);
                result.Result.Add("columns", columnList);
                var dataSourceConfig = new Dictionary<string, object>();
                var transportConfig = new Dictionary<string, object>();
                var readConfig = new Dictionary<string, object>();
                readConfig.Add("url", getDataUrl);
                readConfig.Add("dataType", "json");
                transportConfig.Add("read", readConfig);
                dataSourceConfig.Add("transport", transportConfig);
                dataSourceConfig.Add("pageSize", 20);
                dataSourceConfig.Add("schema", dictSchema);

                var aggregates = reportFields.Where(reportField => !string.IsNullOrEmpty(reportField.AggregateType)).ToList();

                if (aggregates.Any())
                {
                    var dictAggregate = new List<Dictionary<string, object>>();

                    dataSourceConfig.Add("aggregate", dictAggregate);

                    aggregates.ForEach(aggregate =>
                    {
                        var dictSingleAggregate = new Dictionary<string, object>();

                        dictSingleAggregate.Add("field", aggregate.NameCol);
                        dictSingleAggregate.Add("aggregate", aggregate.AggregateType);
                        dictAggregate.Add(dictSingleAggregate);
                    });
                }

                result.Result.Add("dataSource", dataSourceConfig);

                return result;
            });

            return taskResult;
        }

        public ReportGridFactory(Globals globals)
        {
            this.globals = globals;
        }
    }
}
