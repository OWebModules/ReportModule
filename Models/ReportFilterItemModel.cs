﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class ReportFilterItemModel
    {
        public List<clsOntologyItem> FilterItems { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectAtt> FilterItemValues { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectAtt> FilterItemStandard { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> FilterItemsToReports { get; set; } = new List<clsObjectRel>();

    }
}
