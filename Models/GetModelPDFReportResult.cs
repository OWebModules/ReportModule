﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class GetModelPDFReportResult
    {
        public clsOntologyItem RootConfig { get; set; }
        public List<clsOntologyItem> Configs { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> ConfigsToPaths { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ConfigsToFilter { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> FilterValues { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectRel> ConfigsToReports { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ConfigsToSort { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ConfigsToFieldFormat { get; set; } = new List<clsObjectRel>();

        public List<clsObjectAtt> FieldFormatIsHtml { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectRel> FieldFormatToReportFields { get; set; } = new List<clsObjectRel>();
    }
}
