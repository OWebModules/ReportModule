﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class GetVariablesToFieldsRequest
    {
        public List<clsOntologyItem> VariablesToFieldsOItems { get; private set; } = new List<clsOntologyItem>();
        public IMessageOutput MessageOutput { get; set; }

        public GetVariablesToFieldsRequest(List<clsOntologyItem> variablesToFieldsOItems)
        {
            VariablesToFieldsOItems = variablesToFieldsOItems;
        }
    }
}
