﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class GetRowDiffResult
    {
        public ReportField Field1 { get; set; }
        public ReportField Field2 { get; set; }
    }
}
