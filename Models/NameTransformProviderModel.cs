﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class NameTransformProviderModel
    {
        public List<clsOntologyItem> NameTransformConfigs { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectAtt> TableTemplates { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectAtt> RowTemplate { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectAtt> Style { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectRel> ConfigToReports { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ConfigToReportFilters { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ConfigToReportSorts { get; set; } = new List<clsObjectRel>();
        
    }
}
