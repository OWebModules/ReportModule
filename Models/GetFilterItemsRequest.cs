﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public enum FilterRequestType
    {
        GetFiltersByFilterOItems = 0,
        GetFiltersByReport = 1
    }
    public class GetFilterItemsRequest
    {
        public List<clsOntologyItem> FilterOItems { get; set; } = new List<clsOntologyItem>();
        public clsOntologyItem ReportOItem { get; set; }
        public IMessageOutput MessageOutput { get; set; }

        public FilterRequestType RequestType { get; set; }

        public GetFilterItemsRequest(FilterRequestType requestType)
        {
            RequestType = requestType;
        }
    }
}
