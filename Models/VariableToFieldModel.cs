﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class VariableToFieldModel
    {
        public List<clsOntologyItem> VariableToFieldMaps { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> MapsToFields { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> MapsToVariables { get; set; } = new List<clsObjectRel>();
    }
}
