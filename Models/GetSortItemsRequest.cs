﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class GetSortItemsRequest
    {
        public bool RequestSortOItems { get; set; }
        public List<clsOntologyItem> SortOItems { get; private set; } = new List<clsOntologyItem>();
        public IMessageOutput MessageOutput { get; set; }

        public GetSortItemsRequest(List<clsOntologyItem> sortOItems)
        {
            SortOItems = sortOItems;
        }
    }
}
