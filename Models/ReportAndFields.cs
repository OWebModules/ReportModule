﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class ReportAndFields
    {
        public ReportItem Report { get; set; }
        public List<ReportField> ReportFields { get; set; } = new List<ReportField>();

        public List<ReportFilterItem> ReportFilters { get; set; } = new List<ReportFilterItem>();
    }
}
