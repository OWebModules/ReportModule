﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class SyncResult
    {
        public ReportItem Report { get; set; }
        public DateTime SyncStart { get; set; }
        public DateTime SyncEnd { get; set; }

        public override string ToString()
        {
            if (SyncEnd < SyncStart)
            {
                return "00:00:00";
            }
            var syncSpan = SyncEnd.Subtract(SyncStart);

            return $"{syncSpan.Hours.ToString("00")}:{syncSpan.Minutes.ToString("00")}:{syncSpan.Seconds.ToString("00")}";

        }
    }
}
