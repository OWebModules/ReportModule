﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportModule.Models
{
    public class ObjectForSendRaw
    {
        public List<GridCell> row { get; set; }
        public string columnNameSelected { get; set; }
    }

    public class GridCell
    {
        public string columnName { get; set; }
        public object cellValue { get; set; }
    }
}