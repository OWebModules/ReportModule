﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class ObjectsFromReportModel
    {
        public clsOntologyItem Config { get; set; }
        public clsOntologyItem Report { get; set; }
        public clsOntologyItem ReportFilter { get; set; }

        public List<clsOntologyItem> ReportFields { get; set; } = new List<clsOntologyItem>();

    }
}
