﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class RowManipulationItem
    {
        public TableUpdateItem TableUpdateItem { get; set; }
        public DataRow TableRow { get; set; }
        public DataRow TableRowOr { get; set; }
    }
}
