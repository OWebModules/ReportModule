﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class ReportSortItem
    {
        public clsOntologyItem SortItem { get; set; }

        public clsObjectAtt Value { get; set; }

        public clsObjectRel SortItemToReport { get; set; }
    }
}
