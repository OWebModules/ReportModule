﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class GetObjectsFromReportResult
    {
        public List<clsOntologyItem> ValidObjects { get; set; } = new List<clsOntologyItem>();
        public List<clsOntologyItem> InvlidObjects { get; set; } = new List<clsOntologyItem>();
    }
}
