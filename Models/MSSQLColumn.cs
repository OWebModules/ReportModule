﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class MSSQLColumn
    {
        public string ColumnType { get; set; }
        public string ColumnName { get; set; }
        public int Length { get; set; }
    }
}
