﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class ReportFilterItem
    {
        public clsOntologyItem FilterItem { get; set; }

        public clsObjectAtt Value { get; set; }

        public clsObjectRel FilterItemToReport { get; set; }

        public clsObjectAtt Standard { get; set; }
    }
}
