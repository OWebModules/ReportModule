﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class ReportSortItemModel
    {
        public List<clsOntologyItem> SortItems { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectAtt> SortItemValues { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> SortItemsToReports { get; set; } = new List<clsObjectRel>();

    }
}
