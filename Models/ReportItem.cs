﻿using OntologyAppDBConnector.Base;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class ReportItem : NotifyPropertyChange
    {
        private string idRow;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = true, IsVisible = false)]
        public string IdRow
        {
            get { return idRow; }
            set
            {
                if (idRow == value) return;

                idRow = value;

                RaisePropertyChanged(nameof(IdRow));

            }
        }

        private string idReport;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdReport
        {
            get { return idReport; }
            set
            {
                if (idReport == value) return;

                idReport = value;

                RaisePropertyChanged(nameof(IdReport));

            }
        }

        private string nameReport;
        [Json()]
		[DataViewColumn(Caption = "Report", CellType = CellType.String, DisplayOrder = 0, IsIdField = false, IsVisible = true)]
        public string NameReport
        {
            get { return nameReport; }
            set
            {
                if (nameReport == value) return;

                nameReport = value;

                RaisePropertyChanged(nameof(NameReport));

            }
        }

        private string idReportType;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdReportType
        {
            get { return idReportType; }
            set
            {
                if (idReportType == value) return;

                idReportType = value;

                RaisePropertyChanged(nameof(IdReportType));

            }
        }

        private string nameReportType;
        [Json()]
		[DataViewColumn(Caption = "Reporttype", CellType = CellType.String, DisplayOrder = 1, IsIdField = false, IsVisible = true)]
        public string NameReportType
        {
            get { return nameReportType; }
            set
            {
                if (nameReportType == value) return;

                nameReportType = value;

                RaisePropertyChanged(nameof(NameReportType));

            }
        }

        private string idDatabaseOnServer;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdDatabaseOnServer
        {
            get { return idDatabaseOnServer; }
            set
            {
                if (idDatabaseOnServer == value) return;

                idDatabaseOnServer = value;

                RaisePropertyChanged(nameof(IdDatabaseOnServer));

            }
        }

        private string nameDatabaseOnServer;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string NameDatabaseOnServer
        {
            get { return nameDatabaseOnServer; }
            set
            {
                if (nameDatabaseOnServer == value) return;

                nameDatabaseOnServer = value;

                RaisePropertyChanged(nameof(NameDatabaseOnServer));

            }
        }

        private string idDBViewOrEsType;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdDBViewOrEsType
        {
            get { return idDBViewOrEsType; }
            set
            {
                if (idDBViewOrEsType == value) return;

                idDBViewOrEsType = value;

                RaisePropertyChanged(nameof(IdDBViewOrEsType));

            }
        }

        private string nameDBViewOrEsType;
        [Json()]
		[DataViewColumn(Caption = "View", CellType = CellType.String, DisplayOrder = 2, IsIdField = false, IsVisible = true)]
        public string NameDBViewOrEsType
        {
            get { return nameDBViewOrEsType; }
            set
            {
                if (nameDBViewOrEsType == value) return;

                nameDBViewOrEsType = value;

                RaisePropertyChanged(nameof(NameDBViewOrEsType));

            }
        }

        private string idDatabaseOrIndex;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdDatabaseOrIndex
        {
            get { return idDatabaseOrIndex; }
            set
            {
                if (idDatabaseOrIndex == value) return;

                idDatabaseOrIndex = value;

                RaisePropertyChanged(nameof(IdDatabaseOrIndex));

            }
        }

        private string nameDatabaseOrIndex;
        [Json()]
		[DataViewColumn(Caption = "Database", CellType = CellType.String, DisplayOrder = 3, IsIdField = false, IsVisible = true)]
        public string NameDatabaseOrIndex
        {
            get { return nameDatabaseOrIndex; }
            set
            {
                if (nameDatabaseOrIndex == value) return;

                nameDatabaseOrIndex = value;

                RaisePropertyChanged(nameof(NameDatabaseOrIndex));

            }
        }

        private string idInstance;
        [Json()]
        [DataViewColumn(CellType = CellType.String, DisplayOrder = 7, IsIdField = false, IsVisible = false)]
        public string IdInstance
        {
            get { return IdInstance; }
            set
            {
                if (idInstance == value) return;

                idInstance = value;

                RaisePropertyChanged(nameof(IdInstance));

            }
        }

        private string nameInstance;
        [Json()]
        [DataViewColumn(Caption = "Instance", CellType = CellType.String, DisplayOrder = 7, IsIdField = false, IsVisible = true)]
        public string NameInstance
        {
            get { return nameInstance; }
            set
            {
                if (nameInstance == value) return;

                nameInstance = value;

                RaisePropertyChanged(nameof(NameInstance));
            }
        }

        private string idServer;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdServer
        {
            get { return idServer; }
            set
            {
                if (idServer == value) return;

                idServer = value;

                RaisePropertyChanged(nameof(IdServer));

            }
        }

        private string nameServer;
        [Json()]
		[DataViewColumn(Caption = "Server", CellType = CellType.String, DisplayOrder = 4, IsIdField = false, IsVisible = true)]
        public string NameServer
        {
            get { return nameServer; }
            set
            {
                if (nameServer == value) return;

                nameServer = value;

                RaisePropertyChanged(nameof(NameServer));

            }
        }

        private string idPort;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string IdPort
        {
            get { return idPort; }
            set
            {
                if (idPort == value) return;

                idPort = value;

                RaisePropertyChanged(nameof(IdPort));

            }
        }

        private string namePort;
        [Json()]
		[DataViewColumn(CellType = CellType.String, IsIdField = false, IsVisible = false)]
        public string NamePort
        {
            get { return namePort; }
            set
            {
                if (namePort == value) return;

                namePort = value;

                long tmpPort;
                if  (long.TryParse(namePort, out tmpPort))
                {
                    Port = tmpPort;
                }
                else
                {
                    Port = null;
                }

                RaisePropertyChanged(nameof(NamePort));

            }
        }

        private long? port;
        [Json()]
		[DataViewColumn(Caption = "Port", CellType = CellType.Integer, DisplayOrder = 5, IsIdField = false, IsVisible = true)]
        public long? Port
        {
            get { return port; }
            set
            {
                if (port == value) return;

                port = value;

                RaisePropertyChanged(nameof(Port));

            }
        }
    }
}
