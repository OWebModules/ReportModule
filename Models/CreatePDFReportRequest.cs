﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class CreatePDFReportRequest
    {
        public string IdConfig { get; private set; }

        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set; }

        public CreatePDFReportRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}
