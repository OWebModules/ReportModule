﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class MoveObjectsByReportResult
    {
        public clsOntologyItem LogItem { get; set; }
    }
}
