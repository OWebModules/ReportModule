﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class MoveObjectsByReportModel
    {
        public clsOntologyItem Config { get; set; }
        public clsOntologyItem Report { get; set; }
        public clsOntologyItem ReportFieldId { get; set; }
        public clsOntologyItem ReportFilter { get; set; }

        public clsOntologyItem DestinationClass { get; set; }

    }
}
