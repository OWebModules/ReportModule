﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public enum UpdateItemType
    {
        Attribute,
        Class,
        Relation,
        RelationOr
    }
    public class TableUpdateItem
    {
        public string Item1 { get; set; }
        public string Item2 { get; set; }
        public string Item3 { get; set; }
        public string Id1 { get; set; }
        public string Id2 { get; set; }
        public string Id3 { get; set; }
        public UpdateItemType UpdateItemType { get; set; }
        public DataTable Table1 { get; set; }
        public List<DataRow> DataRowsTable1 { get; set; } = new List<DataRow>();
        public DataTable Table2 { get; set; }
        public List<DataRow> DataRowsTable2 { get; set; } = new List<DataRow>();
    }
}
