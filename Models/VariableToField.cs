﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class VariableToField
    {
        public string IdVariableToFieldMap { get; set; }
        public string NameVariableToFieldMap { get; set; }
        public string IdCommandLineRunToReport { get; set; }
        public string NameCommandLineRunToReport { get; set; }
        public string IdFieldReplaceTextParser {get; set;}
        public string NameFieldReplaceTextParser { get; set; }
        public string IdReportField { get; set; }
        public string NameReportField { get; set; }

        public string IdVariable { get; set; }
        public string NameVariable { get; set; }

        public List<RemoveCharacters> RemoveCharacters { get; set; } = new List<RemoveCharacters>();

        public List<FieldReplace> FieldReplaces { get; set; } = new List<FieldReplace>();

    }

    public class RemoveCharacters
    {
        public string IdRemoveCharacters { get; set; }
        public string NameRemoveCharacters { get; set; }
        public long Count { get; set; }
        public long Offset { get; set; }
    }

    public class FieldReplace
    {
        public string IdFieldReplace { get; set; }
        public string NameFieldReplace { get; set; }
        public string Text { get; set; }
    }
}
