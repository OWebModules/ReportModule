﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class GetRowDiffRequest
    {
        public ReportItem ReportItem { get; private set; }
        public List<ReportField> ReportFields { get; private set; }
        public IMessageOutput MessageOutput { get; set; }


        public GetRowDiffRequest(ReportItem reportItem, List<ReportField> reportFields)
        {
            ReportItem = reportItem;
            ReportFields = reportFields;
        }
    }
}
