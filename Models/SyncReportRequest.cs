﻿using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportModule.Models
{
    public class SyncReportRequest
    {
        public ReportItem ReportItem { get; set; }
        public OntologyAppDBConnector.IMessageOutput MessageOutput { get; set; }

    }
}
